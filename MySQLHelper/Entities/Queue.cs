﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;

namespace MySQLHelper.Entities
{
    public class Queue
    {
        public BlockingCollection<Action> Items = new BlockingCollection<Action>();
        private Thread QueueThread = null;
        public Queue()
        {
           QueueThread = new Thread(() =>
            {
                foreach (var action in Items.GetConsumingEnumerable())
                {
                    try
                    {
                        action();
                    }
                    catch
                    {
                    }
                }
            });
           QueueThread.Start();
        }
        public void AddQueueItem(Action item)
        {
            Items.Add(item);
        }

        public void ReleaseTheResources()
        {
            QueueThread.Abort();
        }
    }
}
