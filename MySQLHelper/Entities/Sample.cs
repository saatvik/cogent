﻿using MySQLHelper.Global;
using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Reflection;
using System.Configuration;

namespace MySQLHelper.Entities
{
    public class Sample : Entity<Sample>
    {
        [Identity]
        public int SampleID { get; set; }
        public string SampleName { get; set; }
        public string ParameterName { get; set; }
        public string Status { get; set; }
        public int TriggerNumber { get; set; }
        public decimal ParameterValue { get; set; }
        public int WorkStationNo { get; set; }
        public string ProgramName { get; set; }
        public int IsError { get; set; }
        public decimal TolerenceHigh { get; set; }
        public decimal TolerenceLow { get; set; }

        public virtual void Add()
        {
            try
            {
                base.Add();
            }
            catch { }
        }
        public virtual void AddTriggerData(DataTable dtData)
        {
        }

        public virtual void Update()
        {
            try
            {
                base.Update();
            }
            catch { }
        }

        public virtual List<Sample> GetSamples(DateTime startTime, DateTime endDateTime, string programName)
        {
            List<Sample> samples = new List<Sample>();
            try
            {
                var sSQL = "SELECT * FROM Sample WHERE IsError = 0 AND ProgramName = '" + programName + "'";
                samples = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).Where(s => s.ModifiedOn >= startTime && s.ModifiedOn <= endDateTime).ToList();
            }
            catch { }
            return samples;
        }

        public virtual List<Sample> GetSamples(string sampleName)
        {
            List<Sample> samples = new List<Sample>();
            try
            {
                var sSQL = "SELECT * FROM Sample WHERE SampleName = '" + sampleName + "'";
                samples = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).ToList();
            }
            catch { }
            return samples;
        }

        public void UpdteSamplesAsError(string sampleName)
        {
            try
            {
                string sSQL = "SELECT * FROM Sample WHERE SampleName = '" + sampleName + "'";
                var data = ExecuteDataTable(sSQL);

                if (data.AsEnumerable().Any(dr => Convert.ToInt32(dr.Field<int>("IsError")) == 1))
                {
                    sSQL = "UPDATE Sample Set IsError = 1 WHERE SampleName = '" + sampleName + "'";
                    ExecuteNonQuery(sSQL);
                }
            }
            catch { }
        }

        public virtual Sample GetLastSample(string programName)
        {
            Sample sample = null;
            try
            {
                var sSQL = "SELECT* FROM Sample WHERE ProgramName = '" + programName + "' ORDER BY sampleid DESC";
                sample = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).FirstOrDefault();
            }
            catch { }
            return sample;
        }

        public List<Sample> GetSamples(DataTable dtData)
        {
            return dtData.AsEnumerable().Select(dr => Get(dr)).ToList();
        }

        //Delete all previously generated samples data
        public void ResetSQLData(string programName)
        {
            try
            {
                string sSQL = "DELETE FROM Sample WHERE ProgramName = '" + programName + "'";
                ExecuteNonQuery(sSQL);
            }
            catch { }
        }

        public virtual int GetSampleCount()
        {
            return 1;
        }

        public virtual void SetSampleCount()
        {
        }
    }

    public class RotationCameraData : Sample
    {
        public static int sampleCount = 1;

        public override int GetSampleCount()
        {
            return RotationCameraData.sampleCount;
        }

        public List<RotationCameraData> GetListOfObjects(DataTable dtData)
        {
            try
            {
                bool isErrorEntry = dtData.AsEnumerable().Any(dr => dr.Field<int>("IsError") == 2);
                if (isErrorEntry)
                {
                    foreach (DataRow dr in dtData.Rows)
                    {

                        dr["ParameterValue"] = 0;
                        dr["IsError"] = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
            }

            return dtData.AsEnumerable().Select(dr => GetSample(dr)).ToList();
        }

        public override void AddTriggerData(DataTable dtData)
        {
            try
            {
                if (dtData.Rows.Count != 0)
                    new SideCameraSamples().Delete(dtData.Rows[0].Field<int>("WorkStationNo"));
                bool isErrorEntry = dtData.AsEnumerable().Any(dr => dr.Field<int>("IsError") == 2);
                for (var i = 0; i < dtData.Rows.Count; i++)
                {
                    DataRow dr = dtData.Rows[i];
                    RotationCameraData triggerData = GetSample(dr);
                    if (isErrorEntry)
                    {
                        triggerData.ParameterValue = 0;
                        triggerData.IsError = 1;
                    }
                    triggerData.SampleName = triggerData.SampleName + "_" + sampleCount;
                    triggerData.Add();
                    new SideCameraSamples()
                    {
                        SampleName = triggerData.SampleName,
                        ParameterName = triggerData.ParameterName,
                        Status = triggerData.Status,
                        TriggerNumber = triggerData.TriggerNumber,
                        WorkStationNo = triggerData.WorkStationNo,
                        ParameterValue = triggerData.ParameterValue,
                        ProgramName = triggerData.ProgramName,
                        IsError = triggerData.IsError,
                        TolerenceHigh = triggerData.TolerenceHigh,
                        TolerenceLow = triggerData.TolerenceLow,
                        ModifiedBy = triggerData.ModifiedBy,
                        ModifiedOn = triggerData.ModifiedOn
                    }.Add();
                }
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
            }
        }

        public override void SetSampleCount()
        {
            RotationCameraData.sampleCount++;
        }

        public override Sample GetLastSample(string programName)
        {
            Sample sample = null;
            try
            {
                var sSQL = "SELECT* FROM RotationCameraData WHERE ProgramName = '" + programName + "' ORDER BY sampleid DESC";
                sample = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
            }
            return sample;
        }

        public RotationCameraData GetSample(DataRow row)
        {
            RotationCameraData obj = new RotationCameraData();
            PropertyInfo[] properties = this.GetType().GetProperties();
            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(true);
                string fieldName = property.Name;
                if (row.Table.Columns.Contains(fieldName))
                {
                    try
                    {
                        property.SetValue(obj, row[fieldName], null);
                    }
                    catch (Exception ex)
                    {
                        Log.HandleError(ex);
                    }
                }
            }
            return obj;
        }

        public new RotationCameraData GetRCSample(Sample s)
        {
            return new RotationCameraData()
            {
                SampleID = s.SampleID,
                SampleName = s.SampleName,
                ParameterName = s.ParameterName,
                Status = s.Status,
                TriggerNumber = s.TriggerNumber,
                ParameterValue = s.ParameterValue,
                WorkStationNo = s.WorkStationNo,
                ProgramName = s.ProgramName,
                IsError = s.IsError,
                TolerenceHigh = s.TolerenceHigh,
                TolerenceLow = s.TolerenceLow,
                ModifiedBy = s.ModifiedBy,
                ModifiedOn = s.ModifiedOn
            };
        }

        public new List<RotationCameraData> GetSamples(DateTime startTime, DateTime endDateTime, string programName)
        {
            List<RotationCameraData> samples = new List<RotationCameraData>();
            try
            {
                var sSQL = "SELECT * FROM RotationCameraData WHERE ProgramName = '" + programName + "' AND IsError = 0";
                TraceLog.LogTrace("SQL Command: " + sSQL);
                samples = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => GetRCSample(Get(dr))).Where(s => s.ModifiedOn >= startTime && s.ModifiedOn <= endDateTime).ToList();
                TraceLog.LogTrace("Result: " + samples.Count);
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
            }
            return samples;
        }

        public new List<RotationCameraData> GetSamples(string sampleName)
        {
            List<RotationCameraData> samples = new List<RotationCameraData>();
            try
            {
                var sSQL = "SELECT * FROM RotationCameraData WHERE SampleName = '" + sampleName + "'";
                samples = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => GetRCSample(Get(dr))).ToList();
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
            }
            return samples;
        }

        public new List<RotationCameraData> GetSamplesForChart(string programName)
        {
            List<RotationCameraData> samples = new List<RotationCameraData>();
            try
            {
                var lastSample = GetLastSample(programName);
                if (lastSample != null)
                {
                    var sampleData = lastSample.SampleName.Split('_');
                    var prevSample = sampleData[0] + "_" + (Convert.ToInt32(sampleData[1]) - 1);
                    var sSQL = "SELECT * FROM RotationCameraData WHERE SampleName = '" + prevSample + "' ORDER BY SampleID DESC LIMIT 9";
                    samples = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => GetRCSample(Get(dr))).ToList();
                }
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
            }
            return samples;
        }
    }

    public class IdCameraData : Sample
    {
        public static int sampleCount = 1;

        public override int GetSampleCount()
        {
            return IdCameraData.sampleCount;
        }

        public override void SetSampleCount()
        {
            IdCameraData.sampleCount++;
        }

        public override void Add()
        {
            base.Add();
        }

        public List<IdCameraData> GetListOfObjects(DataTable dtData)
        {
            try
            {
                bool isErrorEntry = dtData.AsEnumerable().Any(dr => dr.Field<int>("IsError") == 2);
                if (isErrorEntry)
                {
                    foreach (DataRow dr in dtData.Rows)
                    {

                        dr["ParameterValue"] = 0;
                        dr["IsError"] = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
            }

            return dtData.AsEnumerable().Select(dr => GetSample(dr)).ToList();
        }

        public IdCameraData GetSample(DataRow row)
        {
            IdCameraData obj = new IdCameraData();
            PropertyInfo[] properties = this.GetType().GetProperties();
            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(true);
                string fieldName = property.Name;
                if (row.Table.Columns.Contains(fieldName))
                {
                    try
                    {
                        property.SetValue(obj, row[fieldName], null);
                    }
                    catch (Exception ex)
                    {
                        Log.HandleError(ex);
                    }
                }
            }
            return obj;
        }

        public override void AddTriggerData(DataTable dtData)
        {
            try
            {
                if (dtData.Rows.Count != 0)
                    new TopCameraSamples().Delete(dtData.Rows[0].Field<int>("WorkStationNo"));
                bool isErrorEntry = dtData.AsEnumerable().Any(dr => dr.Field<int>("IsError") == 2);
                for (var i = 0; i < dtData.Rows.Count; i++)
                {
                    var dr = dtData.Rows[i];
                    IdCameraData triggerData = GetSample(dr);
                    if (isErrorEntry)
                    {
                        triggerData.ParameterValue = 0;
                        triggerData.IsError = 1;
                    }
                    triggerData.SampleName = triggerData.SampleName + "_" + sampleCount;
                    triggerData.Add();
                    new TopCameraSamples()
                    {
                        SampleName = triggerData.SampleName,
                        ParameterName = triggerData.ParameterName,
                        Status = triggerData.Status,
                        TriggerNumber = triggerData.TriggerNumber,
                        WorkStationNo = triggerData.WorkStationNo,
                        ParameterValue = triggerData.ParameterValue,
                        ProgramName = triggerData.ProgramName,
                        IsError = triggerData.IsError,
                        TolerenceHigh = triggerData.TolerenceHigh,
                        TolerenceLow = triggerData.TolerenceLow,
                        ModifiedBy = triggerData.ModifiedBy,
                        ModifiedOn = triggerData.ModifiedOn
                    }.Add();
                }
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
            }
        }

        public override void Update()
        {
            base.Update();
        }

        public new IdCameraData GetSample2(Sample s)
        {
            return new IdCameraData()
            {
                SampleID = s.SampleID,
                SampleName = s.SampleName,
                ParameterName = s.ParameterName,
                Status = s.Status,
                TriggerNumber = s.TriggerNumber,
                ParameterValue = s.ParameterValue,
                WorkStationNo = s.WorkStationNo,
                ProgramName = s.ProgramName,
                IsError = s.IsError,
                TolerenceHigh = s.TolerenceHigh,
                TolerenceLow = s.TolerenceLow,
                ModifiedBy = s.ModifiedBy,
                ModifiedOn = s.ModifiedOn
            };
        }

        public override Sample GetLastSample(string programName)
        {
            Sample sample = null;
            try
            {
                var sSQL = "SELECT* FROM IdCameraData WHERE ProgramName = '" + programName + "' ORDER BY sampleid DESC";
                sample = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
            }
            return sample;
        }

        public new List<IdCameraData> GetSamples(DateTime startTime, DateTime endDateTime, string programName)
        {
            List<IdCameraData> samples = new List<IdCameraData>();
            try
            {
                var sSQL = "SELECT * FROM IdCameraData WHERE ProgramName = '" + programName + "' AND IsError = 0";
                samples = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => GetSample2(Get(dr))).Where(s => s.ModifiedOn >= startTime && s.ModifiedOn <= endDateTime).ToList();
                var LVDTSamples = samples.Where(s => s.ParameterName.Equals("Vial Height")).ToList();
                var count = LVDTSamples.Count;
                while (count > 4)
                {
                    var entry = LVDTSamples[count - 1];
                    var prevEntry = LVDTSamples[count - 1 - 4];
                    entry.ParameterValue = prevEntry.ParameterValue;
                    count--;
                }

                for (var i = 0; i < samples.Count; i++)
                {
                    if (samples[i].ParameterName.Equals("Vial Height"))
                    {
                        var item = LVDTSamples.Find(s => s.SampleID == samples[i].SampleID);
                        samples[i].ParameterValue = item.ParameterValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
            }
            return samples;
        }

        public new List<IdCameraData> GetSamples(string sampleName)
        {
            List<IdCameraData> samples = new List<IdCameraData>();
            try
            {
                var sSQL = "SELECT * FROM IdCameraData WHERE SampleName = '" + sampleName + "'";
                samples = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => GetSample2(Get(dr))).ToList();
                List<IdCameraData> list = (from s in samples
                                           where s.ParameterName.Equals("Vial Height")
                                           select s).ToList<IdCameraData>();
                int j = list.Count;
                int num = Convert.ToInt32(ConfigurationManager.AppSettings["LVDTIndex"]);
                while (j > num)
                {
                    IdCameraData idCameraData = list[j - 1];
                    IdCameraData idCameraData2 = list[j - (num + 1)];
                    idCameraData.ParameterValue = idCameraData2.ParameterValue;
                    j--;
                }
                int i;
                for (i = 0; i < samples.Count; i++)
                {
                    if (samples[i].ParameterName.Equals("Vial Height"))
                    {
                        IdCameraData idCameraData3 = list.Find((IdCameraData s) => s.SampleID == samples[i].SampleID);
                        samples[i].ParameterValue = idCameraData3.ParameterValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
            }
            return samples;
        }

        public new List<IdCameraData> GetSamplesForChart(string programName)
        {
            List<IdCameraData> samples = new List<IdCameraData>();
            try
            {
                var lastSample = GetLastSample(programName);
                if (lastSample != null)
                {
                    var sampleData = lastSample.SampleName.Split('_');
                    var prevSample = sampleData[0] + "_" + (Convert.ToInt32(sampleData[1]) - 1);
                    var sSQL = "SELECT * FROM IdCameraData WHERE SampleName = '" + prevSample + "' ORDER BY SampleID DESC LIMIT 1";
                    samples = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => GetSample2(Get(dr))).ToList();
                }
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
            }
            return samples;
        }
    }

    public class ParameterValue : Entity<ParameterValue>
    {
        [Identity]
        public int ParameterValueID { get; set; }
        public int SampleID { get; set; }
        public decimal Value { get; set; }

        public void AddParameterValue()
        {
            base.Add();
        }

        public List<ParameterValue> GetParamValues(List<int> sampleIDs)
        {
            List<ParameterValue> paramValues = new List<ParameterValue>();
            try
            {
                var sSQL = "SELECT * FROM ParameterValue";
                paramValues = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).Where(s => sampleIDs.Contains(s.SampleID)).ToList();
            }
            catch { }
            return paramValues;
        }


    }

    public class SideCameraSamples : Entity<SideCameraSamples>
    {
        [Identity]
        public int SampleID { get; set; }
        public string SampleName { get; set; }
        public string ParameterName { get; set; }
        public string Status { get; set; }
        public int TriggerNumber { get; set; }
        public decimal ParameterValue { get; set; }
        public int WorkStationNo { get; set; }
        public string ProgramName { get; set; }
        public int IsError { get; set; }
        public decimal TolerenceHigh { get; set; }
        public decimal TolerenceLow { get; set; }

        public new void Add()
        {
            try
            {
                base.Add();
            }
            catch { }
        }

        public new void Delete(int workStationCount)
        {
            try
            {
                if (workStationCount == 15)
                {
                    var sSQL = "DELETE FROM SideCameraSamples";
                    ExecuteNonQuery(sSQL);
                }
            }
            catch { }
        }

        public List<SideCameraSamples> GetData(int workStationCount)
        {
            try
            {
                var sSQL = "SELECT * FROM SideCameraSamples WHERE WorkStationNo = " + workStationCount; 
                return ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).ToList();
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
                return new List<SideCameraSamples>();
            }
        }
    }

    public class TopCameraSamples : Entity<TopCameraSamples>
    {
        [Identity]
        public int SampleID { get; set; }
        public string SampleName { get; set; }
        public string ParameterName { get; set; }
        public string Status { get; set; }
        public int TriggerNumber { get; set; }
        public decimal ParameterValue { get; set; }
        public int WorkStationNo { get; set; }
        public string ProgramName { get; set; }
        public int IsError { get; set; }
        public decimal TolerenceHigh { get; set; }
        public decimal TolerenceLow { get; set; }

        public new void Add()
        {
            try
            {
                base.Add();
            }
            catch { }
        }

        public new void Delete(int workStationCount)
        {
            try
            {
                if (workStationCount == 15)
                {
                    var sSQL = "DELETE FROM TopCameraSamples";
                    ExecuteNonQuery(sSQL);
                }
            }
            catch { }
        }

        public List<TopCameraSamples> GetData(int workStationCount)
        {
            try
            {
                var sSQL = "SELECT * FROM TopCameraSamples WHERE WorkStationNo = " + workStationCount; 
                return ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).ToList();
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
                return new List<TopCameraSamples>();
            }
        }
    }

}
