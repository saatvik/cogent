﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySQLHelper.Global;
using System.Data;

namespace MySQLHelper.Entities
{
    public class VisionProParams : Entity<VisionProParams>
    {
        [Identity]
        public int VisionProParamID { get; set; }
        public string ParamName { get; set; }
        public decimal pTolerence { get; set; }
        public decimal nTolerence { get; set; }
        public decimal Nominal { get; set; }
        public int IncludeInReport { get; set; }

        public void Add()
        {
            try
            {
                base.Add();
            }
            catch { }
        }

        public void DeleteAll()
        {
            try
            {
                string sSQL = "DELETE FROM VisionProParams";
                ExecuteNonQuery(sSQL);
            }
            catch { }
        }

        public List<VisionProParams> GetAll()
        {
            List<VisionProParams> paramAttributes = new List<VisionProParams>();
            try
            {
                var sSQL = "SELECT * FROM VisionProParams";
                paramAttributes = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).ToList();
            }
            catch { }
            return paramAttributes;
        }
    }
}
