﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MySQLHelper.Global
{
    public class Log
    {
        private static string logFilePath = AppDomain.CurrentDomain.BaseDirectory + "\\Log.txt";
        static readonly object fileObject = new object();
        public static void LogDetails(string details)
        {
            try
            {
                var logEnabled = System.Configuration.ConfigurationManager.AppSettings["EnableTriggerLog"] == "1";
                if (logEnabled)
                    return;

                lock (fileObject)
                {
                    StreamWriter sw = null;
                    string logEntry = string.Empty;

                    logEntry = String.Format("{0,0}", details);
                    sw = new StreamWriter(logFilePath, true);
                    sw.WriteLine(logEntry);
                    sw.Flush();
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static void HandleError(Exception ex)
        {
            var logEnabled = System.Configuration.ConfigurationManager.AppSettings["EnableTriggerLog"] == "1";
            if (logEnabled)
                Log.LogDetails(DateTime.Now.ToString() + "         " + ex.Message + "\n START: \n" + ex.StackTrace.ToString() + "\nEND\n");
        }
    }

    public class TraceLog
    {
        private static string logFilePath = AppDomain.CurrentDomain.BaseDirectory + "\\Trace.txt";
        static readonly object fileTraceObject = new object();
        private static void LogDetails(string details)
        {
            try
            {
                lock (fileTraceObject)
                {
                    StreamWriter sw = null;
                    string logEntry = string.Empty;

                    logEntry = String.Format("{0,0}", details);
                    sw = new StreamWriter(logFilePath, true);
                    sw.WriteLine(logEntry);
                    sw.Flush();
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static void LogTrace(string message)
        {
            var traceEnabled = System.Configuration.ConfigurationManager.AppSettings["EnableTracing"] == "1";
            if (traceEnabled)
                TraceLog.LogDetails(message + Environment.NewLine);
        }
    }
}
