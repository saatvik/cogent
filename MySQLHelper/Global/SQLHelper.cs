﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using MySQLHelper.Global;

namespace MySQLHelper.Globals
{
    public abstract class SQLHelper
    {
        private string ConnectionString
        {
            get { return "Server=localhost;Database=reportData;Uid=root;Pwd=root"; }
        }

        public DataTable ExecuteDataTable(string sSQL)
        {
            TraceLog.LogTrace("SQL Command:" + sSQL);
            DataTable data = null;
            MySqlConnection connection = new MySqlConnection(ConnectionString);
            try
            {
                MySqlCommand command = new MySqlCommand(sSQL, connection);
                DataSet ds = new DataSet();
                MySqlDataAdapter da = new MySqlDataAdapter();
                da.SelectCommand = command;
                connection.Open();
                da.Fill(ds);
                if (ds.Tables.Count != 0)
                {
                    data = ds.Tables[0];
                    TraceLog.LogTrace("Results:" + data.Rows.Count);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return data;
        }

        public int ExecuteNonQuery(string sSQL)
        {
            TraceLog.LogTrace(sSQL);
            MySqlConnection connection = new MySqlConnection(ConnectionString);
            try
            {
                MySqlCommand command = new MySqlCommand(sSQL, connection);
                connection.Open();
                return command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
