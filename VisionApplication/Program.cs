using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Cognex.VisionPro;
using System.Diagnostics;

namespace VisionApplication
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler((o, s) =>
            {
            });
            if (!ValidateMacAddress())
            {
                var form = new VisionControl.AccessDenied();
                form.ShowDialog();
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());

                CogFrameGrabbers frameGrabbers = new CogFrameGrabbers();
                for (int i = 0; i < frameGrabbers.Count; i++)
                {
                    frameGrabbers[i].Disconnect(false);
                }
            }
        }

        static bool ValidateMacAddress()
        {
            bool IsMacValid = true;
            try
            {
                var validSerialNumbers = new List<string>() { "390654505", "1471332497", "otherMacID2" };
                if (IsMacValid)
                {
                    ProcessStartInfo procStartInfo = new ProcessStartInfo("cogtool", "--print");
                    procStartInfo.UseShellExecute = false;
                    procStartInfo.RedirectStandardOutput = true;
                    procStartInfo.CreateNoWindow = true;
                    procStartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                    var p = Process.Start(procStartInfo);
                    string s = p.StandardOutput.ReadToEnd();
                    var dongleSerialNumber = System.Text.RegularExpressions.Regex.Match(s.Substring(s.IndexOf("Serial Number:")), @"\d+").Value;
                    IsMacValid = validSerialNumbers.Contains(dongleSerialNumber);
                }
            }
            catch
            {
                IsMacValid = false;
            }
            return IsMacValid;
        }
    }
}