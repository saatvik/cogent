﻿using Modbus.Device;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO.Ports;
using System.Linq;
using System.Text;
using SAATVIK.HARDWARE.ETHERNET;

namespace SAATVIK.HARDWARE
{
    public static class Device
    {
        private static SerialPort _port = null;
        private static IModbusSerialMaster _master = null;

        private static SerialPort Port
        {
            get
            {
                if (_port == null)
                {
                    var comPort = "Com" + System.Configuration.ConfigurationManager.AppSettings["ComPort"];
                    _port = new SerialPort(comPort, 9600, Parity.Even, 7, StopBits.One);
                }
                return _port;
            }
        }

        public static IModbusSerialMaster Master
        {
            get
            {
                if (_master == null)
                {
                    _master = ModbusSerialMaster.CreateAscii(Port);
                }
                return _master;
            }
        }

        public static void CloseComPort()
        {
            if (Master != null)
                Master.Dispose();

            if (Port != null)
                Port.Close();
        }

        static readonly object portObject = new object();
        public static ushort[] ReadHoldingRegisters(ushort startAddress, ushort numRegisters, byte deviceID)
        {
            lock (portObject)
            {
                ushort[] registerValues = new ushort[] { 0 };
                //if (!Port.IsOpen)
                //    Port.Open();

                //try
                //{
                //    registerValues = Master.ReadHoldingRegisters(deviceID, startAddress, numRegisters);
                //}
                //catch { }

                //return registerValues;

                byte[] values = new byte[5];
                new Master().ReadHoldingRegister(3, 1, startAddress, numRegisters, ref values);
                return values == null ? null : new ushort[] { values[0], values[1]  };
            }
        }

        public static ushort ReadHoldingRegisters(ushort startAddress, ushort numRegisters)
        {
            lock (SAATVIK.HARDWARE.Device.portObject)
            {
                byte[] local_0 = new byte[5];
                new SAATVIK.HARDWARE.ETHERNET.Master().ReadHoldingRegister((ushort)3, (byte)0, startAddress, numRegisters, ref local_0);
                bool[] local_1 = new bool[1];
                int[] local_2 = new int[1];
                if (local_0.Length >= 2)
                {
                    local_2 = new int[local_0.Length / 2];
                    int local_3 = 0;
                    while (local_3 < local_0.Length)
                    {
                        local_2[local_3 / 2] = (int)local_0[local_3] * 256 + (int)local_0[local_3 + 1];
                        local_3 += 2;
                    }
                }
                return Convert.ToUInt16(local_2[0]);
            }
        }

        public static bool ReadCoils(ushort startAddress, byte deviceID)
        {
            bool coilStatus = false;
            if (!Port.IsOpen)
                Port.Open();

            try
            {
                bool[] inputValues = Master.ReadCoils(deviceID, startAddress, 1);
                coilStatus = inputValues[0];
            }
            catch
            {
            }
            return coilStatus;
        }

        public static void WriteHoldingRegisters(ushort startAddress, ushort[] data, byte deviceID)
        {
            try
            {
                if (!Port.IsOpen)
                    Port.Open();

                Master.WriteMultipleRegisters(deviceID, (ushort)(startAddress), data);
            }
            catch
            {

            }
        }

        public static void WriteCoils(ushort startAddress, bool[] registers, byte deviceID)
        {
            try
            {
                //if (!Port.IsOpen)
                //    Port.Open();

                //Master.WriteMultipleCoils(deviceID, (ushort)(startAddress), registers);

                new Master().WriteSingleCoils(5, 1, (ushort)(startAddress), registers[0]);
            }
            catch
            {
            }
        }

        public static void WriteCoilAsyncs(ushort startAddress, bool[] registers, byte deviceID)
        {
            try
            {
                if (!Port.IsOpen)
                    Port.Open();

                Master.WriteMultipleCoilsAsync(deviceID, (ushort)(startAddress), registers);
            }
            catch
            {
            }
        }
    }

}
