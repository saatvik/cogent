using System.Windows.Forms;

namespace Saatvik
{
    public partial class VisionControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                if (mJM != null)
                    mJM.Shutdown();
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        // cognex.wizard.initializecomponent
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisionControl));
            System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation verticalLineAnnotation1 = new System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation verticalLineAnnotation2 = new System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation verticalLineAnnotation3 = new System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title3 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation verticalLineAnnotation4 = new System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title4 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation verticalLineAnnotation5 = new System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title5 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation verticalLineAnnotation6 = new System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title6 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation verticalLineAnnotation7 = new System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title7 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation verticalLineAnnotation8 = new System.Windows.Forms.DataVisualization.Charting.VerticalLineAnnotation();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title8 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.label_controlErrorMessage = new System.Windows.Forms.Label();
            this.btnRun = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_ProgramFile = new System.Windows.Forms.PictureBox();
            this.tabControl_ProductionParams = new System.Windows.Forms.TabControl();
            this.tabPage_JobN_JobStatistics = new System.Windows.Forms.TabPage();
            this.label37 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.textBoxICRejected = new System.Windows.Forms.TextBox();
            this.textBoxICYield = new System.Windows.Forms.TextBox();
            this.textBoxICResetRejected = new System.Windows.Forms.TextBox();
            this.textBoxICResetYield = new System.Windows.Forms.TextBox();
            this.buttonICReset = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.textBoxRCYield = new System.Windows.Forms.TextBox();
            this.textBoxRCResetYield = new System.Windows.Forms.TextBox();
            this.textBoxRCRejected = new System.Windows.Forms.TextBox();
            this.textBoxRCReset_Rejected = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.buttonRCYieldReset = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxTotalYield = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxResetUtilisation = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxTotalUtilisation = new System.Windows.Forms.TextBox();
            this.buttonUtilisationDataReset = new System.Windows.Forms.Button();
            this.textBoxProducedReset = new System.Windows.Forms.TextBox();
            this.textBoxProducedTotal = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxIndexedReset = new System.Windows.Forms.TextBox();
            this.textBoxIndexedTotal = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLVDTReset = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.textBoxTotalResetYield = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tabPage_Configuration = new System.Windows.Forms.TabPage();
            this.buttonProductionParams = new System.Windows.Forms.Button();
            this.buttonSetDimensionParams = new System.Windows.Forms.Button();
            this.button_Configuration = new System.Windows.Forms.Button();
            this.comboBox_Login = new System.Windows.Forms.ComboBox();
            this.label_Login = new System.Windows.Forms.Label();
            this.tabReport = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.btn_setResultsPath = new System.Windows.Forms.Button();
            this.txt_results_path = new System.Windows.Forms.TextBox();
            this.buttonGenerateReport = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox_Vpp_Files = new System.Windows.Forms.ComboBox();
            this.btn_loadvppfile = new System.Windows.Forms.Button();
            this.label_Online = new System.Windows.Forms.Label();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.btnRunCont = new System.Windows.Forms.Button();
            this.labelAppVersion = new System.Windows.Forms.Label();
            this.applicationErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.panelCameraDisplay = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cogRecordsDisplay1 = new Cognex.VisionPro.CogRecordsDisplay();
            this.labelCam2Status = new System.Windows.Forms.Label();
            this.labelCam1Status = new System.Windows.Forms.Label();
            this.cogRecordsDisplay2 = new Cognex.VisionPro.CogRecordsDisplay();
            this.fldrBrowser_ResultsPath = new System.Windows.Forms.FolderBrowserDialog();
            this.timerWorkStation = new System.Windows.Forms.Timer(this.components);
            this.panelCharts = new System.Windows.Forms.Panel();
            this.tableLayoutPanelChart8 = new System.Windows.Forms.TableLayoutPanel();
            this.chart8 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxVal_8 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelChart7 = new System.Windows.Forms.TableLayoutPanel();
            this.chart7 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxVal_7 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelChart6 = new System.Windows.Forms.TableLayoutPanel();
            this.chart6 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxVal_6 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelChart5 = new System.Windows.Forms.TableLayoutPanel();
            this.chart5 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxVal_5 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelChart4 = new System.Windows.Forms.TableLayoutPanel();
            this.chart4 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxVal_4 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelChart3 = new System.Windows.Forms.TableLayoutPanel();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxVal_3 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelChart2 = new System.Windows.Forms.TableLayoutPanel();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxVal_2 = new System.Windows.Forms.TextBox();
            this.panelChart1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanelChart1 = new System.Windows.Forms.TableLayoutPanel();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxVal_1 = new System.Windows.Forms.TextBox();
            this.panelChartParentContainer = new System.Windows.Forms.Panel();
            this.cogToolPropertyProvider0 = new Cognex.VisionPro.CogToolPropertyProvider();
            this.cogToolPropertyProvider1 = new Cognex.VisionPro.CogToolPropertyProvider();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_ProgramFile)).BeginInit();
            this.tabControl_ProductionParams.SuspendLayout();
            this.tabPage_JobN_JobStatistics.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage_Configuration.SuspendLayout();
            this.tabReport.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.applicationErrorProvider)).BeginInit();
            this.panelCameraDisplay.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panelCharts.SuspendLayout();
            this.tableLayoutPanelChart8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart8)).BeginInit();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanelChart7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart7)).BeginInit();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanelChart6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart6)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanelChart5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanelChart4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).BeginInit();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanelChart3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanelChart2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.panelChart1.SuspendLayout();
            this.tableLayoutPanelChart1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panelChartParentContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_controlErrorMessage
            // 
            this.label_controlErrorMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label_controlErrorMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_controlErrorMessage.Location = new System.Drawing.Point(3, 213);
            this.label_controlErrorMessage.Name = "label_controlErrorMessage";
            this.label_controlErrorMessage.Size = new System.Drawing.Size(294, 12);
            this.label_controlErrorMessage.TabIndex = 30;
            this.label_controlErrorMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_controlErrorMessage.Click += new System.EventHandler(this.label_controlErrorMessage_Click);
            // 
            // btnRun
            // 
            this.btnRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(157, 3);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(134, 34);
            this.btnRun.TabIndex = 43;
            this.btnRun.Text = "Manual";
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.tableLayoutPanel10);
            this.panel3.Location = new System.Drawing.Point(613, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(300, 599);
            this.panel3.TabIndex = 37;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.lbl_ProgramFile, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.tabControl_ProductionParams, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.panel7, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.label_controlErrorMessage, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel11, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.labelAppVersion, 0, 5);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 6;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.98411F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.86821F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.91214F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.057141F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 58.38295F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.795453F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(300, 599);
            this.tableLayoutPanel10.TabIndex = 57;
            // 
            // lbl_ProgramFile
            // 
            this.lbl_ProgramFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_ProgramFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lbl_ProgramFile.Image = ((System.Drawing.Image)(resources.GetObject("lbl_ProgramFile.Image")));
            this.lbl_ProgramFile.Location = new System.Drawing.Point(3, 3);
            this.lbl_ProgramFile.Name = "lbl_ProgramFile";
            this.lbl_ProgramFile.Size = new System.Drawing.Size(294, 80);
            this.lbl_ProgramFile.TabIndex = 52;
            this.lbl_ProgramFile.TabStop = false;
            // 
            // tabControl_ProductionParams
            // 
            this.tabControl_ProductionParams.Controls.Add(this.tabPage_JobN_JobStatistics);
            this.tabControl_ProductionParams.Controls.Add(this.tabPage_Configuration);
            this.tabControl_ProductionParams.Controls.Add(this.tabReport);
            this.tabControl_ProductionParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl_ProductionParams.Location = new System.Drawing.Point(3, 228);
            this.tabControl_ProductionParams.Name = "tabControl_ProductionParams";
            this.tabControl_ProductionParams.SelectedIndex = 0;
            this.tabControl_ProductionParams.Size = new System.Drawing.Size(294, 343);
            this.tabControl_ProductionParams.TabIndex = 56;
            this.tabControl_ProductionParams.Tag = "";
            // 
            // tabPage_JobN_JobStatistics
            // 
            this.tabPage_JobN_JobStatistics.AutoScroll = true;
            this.tabPage_JobN_JobStatistics.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tabPage_JobN_JobStatistics.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage_JobN_JobStatistics.Controls.Add(this.label37);
            this.tabPage_JobN_JobStatistics.Controls.Add(this.label33);
            this.tabPage_JobN_JobStatistics.Controls.Add(this.groupBox3);
            this.tabPage_JobN_JobStatistics.Controls.Add(this.label31);
            this.tabPage_JobN_JobStatistics.Controls.Add(this.label32);
            this.tabPage_JobN_JobStatistics.Controls.Add(this.groupBox2);
            this.tabPage_JobN_JobStatistics.Controls.Add(this.textBoxTotalYield);
            this.tabPage_JobN_JobStatistics.Controls.Add(this.groupBox1);
            this.tabPage_JobN_JobStatistics.Controls.Add(this.btnLVDTReset);
            this.tabPage_JobN_JobStatistics.Controls.Add(this.label23);
            this.tabPage_JobN_JobStatistics.Controls.Add(this.textBoxTotalResetYield);
            this.tabPage_JobN_JobStatistics.Controls.Add(this.label24);
            this.tabPage_JobN_JobStatistics.Location = new System.Drawing.Point(4, 22);
            this.tabPage_JobN_JobStatistics.Name = "tabPage_JobN_JobStatistics";
            this.tabPage_JobN_JobStatistics.Size = new System.Drawing.Size(286, 317);
            this.tabPage_JobN_JobStatistics.TabIndex = 0;
            this.tabPage_JobN_JobStatistics.Text = "Job Statistics";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(201, 427);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(41, 13);
            this.label37.TabIndex = 26;
            this.label37.Text = "label37";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(180, 452);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(15, 13);
            this.label33.TabIndex = 24;
            this.label33.Text = "%";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.textBoxICRejected);
            this.groupBox3.Controls.Add(this.textBoxICYield);
            this.groupBox3.Controls.Add(this.textBoxICResetRejected);
            this.groupBox3.Controls.Add(this.textBoxICResetYield);
            this.groupBox3.Controls.Add(this.buttonICReset);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox3.Location = new System.Drawing.Point(4, 300);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(260, 119);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "ID Camera Yield";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(180, 63);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(15, 13);
            this.label30.TabIndex = 24;
            this.label30.Text = "%";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 36);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(50, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Rejected";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(110, 63);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(15, 13);
            this.label29.TabIndex = 25;
            this.label29.Text = "%";
            // 
            // textBoxICRejected
            // 
            this.textBoxICRejected.Location = new System.Drawing.Point(66, 32);
            this.textBoxICRejected.Name = "textBoxICRejected";
            this.textBoxICRejected.Size = new System.Drawing.Size(45, 20);
            this.textBoxICRejected.TabIndex = 1;
            this.textBoxICRejected.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxICYield
            // 
            this.textBoxICYield.Location = new System.Drawing.Point(66, 60);
            this.textBoxICYield.Name = "textBoxICYield";
            this.textBoxICYield.Size = new System.Drawing.Size(45, 20);
            this.textBoxICYield.TabIndex = 1;
            this.textBoxICYield.Text = "0";
            this.textBoxICYield.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxICResetRejected
            // 
            this.textBoxICResetRejected.Location = new System.Drawing.Point(132, 32);
            this.textBoxICResetRejected.Name = "textBoxICResetRejected";
            this.textBoxICResetRejected.Size = new System.Drawing.Size(45, 20);
            this.textBoxICResetRejected.TabIndex = 1;
            this.textBoxICResetRejected.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxICResetYield
            // 
            this.textBoxICResetYield.Location = new System.Drawing.Point(132, 60);
            this.textBoxICResetYield.Name = "textBoxICResetYield";
            this.textBoxICResetYield.Size = new System.Drawing.Size(45, 20);
            this.textBoxICResetYield.TabIndex = 1;
            this.textBoxICResetYield.Text = "0";
            this.textBoxICResetYield.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonICReset
            // 
            this.buttonICReset.Location = new System.Drawing.Point(194, 60);
            this.buttonICReset.Name = "buttonICReset";
            this.buttonICReset.Size = new System.Drawing.Size(59, 23);
            this.buttonICReset.TabIndex = 7;
            this.buttonICReset.Text = "Reset";
            this.buttonICReset.UseVisualStyleBackColor = true;
            this.buttonICReset.Click += new System.EventHandler(this.buttonICReset_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(31, 63);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(30, 13);
            this.label21.TabIndex = 9;
            this.label21.Text = "Yield";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(68, 16);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(31, 13);
            this.label25.TabIndex = 2;
            this.label25.Text = "Total";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(136, 16);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 13);
            this.label26.TabIndex = 3;
            this.label26.Text = "Reset";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(1, 451);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(68, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "Total Yield";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(114, 451);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(15, 13);
            this.label32.TabIndex = 25;
            this.label32.Text = "%";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.textBoxRCYield);
            this.groupBox2.Controls.Add(this.textBoxRCResetYield);
            this.groupBox2.Controls.Add(this.textBoxRCRejected);
            this.groupBox2.Controls.Add(this.textBoxRCReset_Rejected);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.buttonRCYieldReset);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox2.Location = new System.Drawing.Point(7, 183);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(257, 111);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Rotation Camera Yield";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(180, 60);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(15, 13);
            this.label28.TabIndex = 24;
            this.label28.Text = "%";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(11, 36);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(50, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Rejected";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(110, 61);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(15, 13);
            this.label27.TabIndex = 25;
            this.label27.Text = "%";
            // 
            // textBoxRCYield
            // 
            this.textBoxRCYield.Location = new System.Drawing.Point(66, 58);
            this.textBoxRCYield.Name = "textBoxRCYield";
            this.textBoxRCYield.Size = new System.Drawing.Size(45, 20);
            this.textBoxRCYield.TabIndex = 1;
            this.textBoxRCYield.Text = "0";
            this.textBoxRCYield.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxRCResetYield
            // 
            this.textBoxRCResetYield.Location = new System.Drawing.Point(132, 58);
            this.textBoxRCResetYield.Name = "textBoxRCResetYield";
            this.textBoxRCResetYield.Size = new System.Drawing.Size(45, 20);
            this.textBoxRCResetYield.TabIndex = 1;
            this.textBoxRCResetYield.Text = "0";
            this.textBoxRCResetYield.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxRCRejected
            // 
            this.textBoxRCRejected.Location = new System.Drawing.Point(66, 32);
            this.textBoxRCRejected.Name = "textBoxRCRejected";
            this.textBoxRCRejected.Size = new System.Drawing.Size(45, 20);
            this.textBoxRCRejected.TabIndex = 1;
            this.textBoxRCRejected.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxRCReset_Rejected
            // 
            this.textBoxRCReset_Rejected.Location = new System.Drawing.Point(132, 32);
            this.textBoxRCReset_Rejected.Name = "textBoxRCReset_Rejected";
            this.textBoxRCReset_Rejected.Size = new System.Drawing.Size(45, 20);
            this.textBoxRCReset_Rejected.TabIndex = 1;
            this.textBoxRCReset_Rejected.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(31, 61);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(30, 13);
            this.label20.TabIndex = 9;
            this.label20.Text = "Yield";
            // 
            // buttonRCYieldReset
            // 
            this.buttonRCYieldReset.Location = new System.Drawing.Point(194, 58);
            this.buttonRCYieldReset.Name = "buttonRCYieldReset";
            this.buttonRCYieldReset.Size = new System.Drawing.Size(57, 23);
            this.buttonRCYieldReset.TabIndex = 7;
            this.buttonRCYieldReset.Text = "Reset";
            this.buttonRCYieldReset.UseVisualStyleBackColor = true;
            this.buttonRCYieldReset.Click += new System.EventHandler(this.buttonRCYieldReset_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(68, 16);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Total";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(136, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 13);
            this.label19.TabIndex = 3;
            this.label19.Text = "Reset";
            // 
            // textBoxTotalYield
            // 
            this.textBoxTotalYield.Location = new System.Drawing.Point(69, 448);
            this.textBoxTotalYield.Name = "textBoxTotalYield";
            this.textBoxTotalYield.Size = new System.Drawing.Size(45, 20);
            this.textBoxTotalYield.TabIndex = 1;
            this.textBoxTotalYield.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.textBoxResetUtilisation);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.textBoxTotalUtilisation);
            this.groupBox1.Controls.Add(this.buttonUtilisationDataReset);
            this.groupBox1.Controls.Add(this.textBoxProducedReset);
            this.groupBox1.Controls.Add(this.textBoxProducedTotal);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.textBoxIndexedReset);
            this.groupBox1.Controls.Add(this.textBoxIndexedTotal);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Location = new System.Drawing.Point(7, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(257, 160);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Utilisation Data";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(186, 105);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(15, 13);
            this.label16.TabIndex = 24;
            this.label16.Text = "%";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(116, 106);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(15, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "%";
            // 
            // textBoxResetUtilisation
            // 
            this.textBoxResetUtilisation.Location = new System.Drawing.Point(140, 103);
            this.textBoxResetUtilisation.Name = "textBoxResetUtilisation";
            this.textBoxResetUtilisation.Size = new System.Drawing.Size(45, 20);
            this.textBoxResetUtilisation.TabIndex = 23;
            this.textBoxResetUtilisation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(20, 108);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "Utilisation";
            // 
            // textBoxTotalUtilisation
            // 
            this.textBoxTotalUtilisation.Location = new System.Drawing.Point(74, 103);
            this.textBoxTotalUtilisation.Name = "textBoxTotalUtilisation";
            this.textBoxTotalUtilisation.Size = new System.Drawing.Size(45, 20);
            this.textBoxTotalUtilisation.TabIndex = 21;
            this.textBoxTotalUtilisation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonUtilisationDataReset
            // 
            this.buttonUtilisationDataReset.Location = new System.Drawing.Point(171, 126);
            this.buttonUtilisationDataReset.Name = "buttonUtilisationDataReset";
            this.buttonUtilisationDataReset.Size = new System.Drawing.Size(75, 23);
            this.buttonUtilisationDataReset.TabIndex = 20;
            this.buttonUtilisationDataReset.Text = "Reset";
            this.buttonUtilisationDataReset.UseVisualStyleBackColor = true;
            this.buttonUtilisationDataReset.Click += new System.EventHandler(this.buttonUtilisationDataReset_Click_1);
            // 
            // textBoxProducedReset
            // 
            this.textBoxProducedReset.Location = new System.Drawing.Point(140, 74);
            this.textBoxProducedReset.Name = "textBoxProducedReset";
            this.textBoxProducedReset.Size = new System.Drawing.Size(45, 20);
            this.textBoxProducedReset.TabIndex = 19;
            this.textBoxProducedReset.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxProducedTotal
            // 
            this.textBoxProducedTotal.Location = new System.Drawing.Point(74, 74);
            this.textBoxProducedTotal.Name = "textBoxProducedTotal";
            this.textBoxProducedTotal.Size = new System.Drawing.Size(45, 20);
            this.textBoxProducedTotal.TabIndex = 18;
            this.textBoxProducedTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(19, 78);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 13);
            this.label13.TabIndex = 17;
            this.label13.Text = "Produced";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(144, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Reset";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(76, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Total";
            // 
            // textBoxIndexedReset
            // 
            this.textBoxIndexedReset.Location = new System.Drawing.Point(140, 44);
            this.textBoxIndexedReset.Name = "textBoxIndexedReset";
            this.textBoxIndexedReset.Size = new System.Drawing.Size(45, 20);
            this.textBoxIndexedReset.TabIndex = 13;
            this.textBoxIndexedReset.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxIndexedTotal
            // 
            this.textBoxIndexedTotal.BackColor = System.Drawing.Color.White;
            this.textBoxIndexedTotal.Location = new System.Drawing.Point(74, 44);
            this.textBoxIndexedTotal.Name = "textBoxIndexedTotal";
            this.textBoxIndexedTotal.Size = new System.Drawing.Size(45, 20);
            this.textBoxIndexedTotal.TabIndex = 14;
            this.textBoxIndexedTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Indexed";
            // 
            // btnLVDTReset
            // 
            this.btnLVDTReset.Location = new System.Drawing.Point(194, 447);
            this.btnLVDTReset.Name = "btnLVDTReset";
            this.btnLVDTReset.Size = new System.Drawing.Size(59, 23);
            this.btnLVDTReset.TabIndex = 7;
            this.btnLVDTReset.Text = "Reset";
            this.btnLVDTReset.UseVisualStyleBackColor = true;
            this.btnLVDTReset.Click += new System.EventHandler(this.btnLVDTReset_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(139, 432);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 13);
            this.label23.TabIndex = 3;
            this.label23.Text = "Reset";
            // 
            // textBoxTotalResetYield
            // 
            this.textBoxTotalResetYield.Location = new System.Drawing.Point(135, 448);
            this.textBoxTotalResetYield.Name = "textBoxTotalResetYield";
            this.textBoxTotalResetYield.Size = new System.Drawing.Size(45, 20);
            this.textBoxTotalResetYield.TabIndex = 1;
            this.textBoxTotalResetYield.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(71, 432);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(31, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Total";
            // 
            // tabPage_Configuration
            // 
            this.tabPage_Configuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tabPage_Configuration.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage_Configuration.Controls.Add(this.buttonProductionParams);
            this.tabPage_Configuration.Controls.Add(this.buttonSetDimensionParams);
            this.tabPage_Configuration.Controls.Add(this.button_Configuration);
            this.tabPage_Configuration.Controls.Add(this.comboBox_Login);
            this.tabPage_Configuration.Controls.Add(this.label_Login);
            this.tabPage_Configuration.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Configuration.Name = "tabPage_Configuration";
            this.tabPage_Configuration.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Configuration.Size = new System.Drawing.Size(286, 317);
            this.tabPage_Configuration.TabIndex = 1;
            this.tabPage_Configuration.Text = "Configuration";
            // 
            // buttonProductionParams
            // 
            this.buttonProductionParams.Location = new System.Drawing.Point(102, 157);
            this.buttonProductionParams.Name = "buttonProductionParams";
            this.buttonProductionParams.Size = new System.Drawing.Size(179, 36);
            this.buttonProductionParams.TabIndex = 50;
            this.buttonProductionParams.Text = "Production Params";
            this.buttonProductionParams.UseVisualStyleBackColor = true;
            this.buttonProductionParams.Click += new System.EventHandler(this.buttonProductionParams_Click);
            // 
            // buttonSetDimensionParams
            // 
            this.buttonSetDimensionParams.Location = new System.Drawing.Point(102, 110);
            this.buttonSetDimensionParams.Name = "buttonSetDimensionParams";
            this.buttonSetDimensionParams.Size = new System.Drawing.Size(179, 36);
            this.buttonSetDimensionParams.TabIndex = 50;
            this.buttonSetDimensionParams.Text = "Tolerance";
            this.buttonSetDimensionParams.UseVisualStyleBackColor = true;
            this.buttonSetDimensionParams.Click += new System.EventHandler(this.buttonSetDimensionParams_Click);
            // 
            // button_Configuration
            // 
            this.button_Configuration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Configuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Configuration.Location = new System.Drawing.Point(100, 55);
            this.button_Configuration.Name = "button_Configuration";
            this.button_Configuration.Size = new System.Drawing.Size(182, 37);
            this.button_Configuration.TabIndex = 45;
            this.button_Configuration.Text = "System Configuration";
            this.button_Configuration.Click += new System.EventHandler(this.button_Configuration_Click_1);
            // 
            // comboBox_Login
            // 
            this.comboBox_Login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_Login.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_Login.Location = new System.Drawing.Point(100, 25);
            this.comboBox_Login.Name = "comboBox_Login";
            this.comboBox_Login.Size = new System.Drawing.Size(181, 24);
            this.comboBox_Login.TabIndex = 41;
            this.comboBox_Login.SelectedIndexChanged += new System.EventHandler(this.comboBox_Login_SelectedIndexChanged);
            // 
            // label_Login
            // 
            this.label_Login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Login.Location = new System.Drawing.Point(-2, 28);
            this.label_Login.Name = "label_Login";
            this.label_Login.Size = new System.Drawing.Size(103, 21);
            this.label_Login.TabIndex = 49;
            this.label_Login.Text = "Current login:";
            // 
            // tabReport
            // 
            this.tabReport.Controls.Add(this.panel8);
            this.tabReport.Controls.Add(this.buttonGenerateReport);
            this.tabReport.Location = new System.Drawing.Point(4, 22);
            this.tabReport.Name = "tabReport";
            this.tabReport.Padding = new System.Windows.Forms.Padding(3);
            this.tabReport.Size = new System.Drawing.Size(286, 317);
            this.tabReport.TabIndex = 2;
            this.tabReport.Text = "Report";
            this.tabReport.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.Controls.Add(this.label34);
            this.panel8.Controls.Add(this.btn_setResultsPath);
            this.panel8.Controls.Add(this.txt_results_path);
            this.panel8.Location = new System.Drawing.Point(3, 22);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(280, 55);
            this.panel8.TabIndex = 65;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(-1, 6);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(121, 13);
            this.label34.TabIndex = 57;
            this.label34.Text = "Result Destination Path:";
            // 
            // btn_setResultsPath
            // 
            this.btn_setResultsPath.Location = new System.Drawing.Point(164, 4);
            this.btn_setResultsPath.Name = "btn_setResultsPath";
            this.btn_setResultsPath.Size = new System.Drawing.Size(113, 38);
            this.btn_setResultsPath.TabIndex = 1;
            this.btn_setResultsPath.Text = "Select Results Path";
            this.btn_setResultsPath.UseVisualStyleBackColor = true;
            this.btn_setResultsPath.Click += new System.EventHandler(this.btn_setResultsPath_Click);
            // 
            // txt_results_path
            // 
            this.txt_results_path.Enabled = false;
            this.txt_results_path.Location = new System.Drawing.Point(2, 22);
            this.txt_results_path.Name = "txt_results_path";
            this.txt_results_path.Size = new System.Drawing.Size(155, 20);
            this.txt_results_path.TabIndex = 0;
            // 
            // buttonGenerateReport
            // 
            this.buttonGenerateReport.Location = new System.Drawing.Point(64, 127);
            this.buttonGenerateReport.Name = "buttonGenerateReport";
            this.buttonGenerateReport.Size = new System.Drawing.Size(158, 33);
            this.buttonGenerateReport.TabIndex = 0;
            this.buttonGenerateReport.Text = "Generate Report";
            this.buttonGenerateReport.UseVisualStyleBackColor = true;
            this.buttonGenerateReport.Click += new System.EventHandler(this.buttonGenerateReport_Click);
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.comboBox_Vpp_Files);
            this.panel7.Controls.Add(this.btn_loadvppfile);
            this.panel7.Controls.Add(this.label_Online);
            this.applicationErrorProvider.SetIconAlignment(this.panel7, System.Windows.Forms.ErrorIconAlignment.TopRight);
            this.panel7.Location = new System.Drawing.Point(3, 98);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(294, 62);
            this.panel7.TabIndex = 55;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 55;
            this.label2.Text = "Select Program File:";
            // 
            // comboBox_Vpp_Files
            // 
            this.comboBox_Vpp_Files.FormattingEnabled = true;
            this.comboBox_Vpp_Files.Location = new System.Drawing.Point(3, 16);
            this.comboBox_Vpp_Files.Name = "comboBox_Vpp_Files";
            this.comboBox_Vpp_Files.Size = new System.Drawing.Size(188, 21);
            this.comboBox_Vpp_Files.TabIndex = 53;
            // 
            // btn_loadvppfile
            // 
            this.btn_loadvppfile.Location = new System.Drawing.Point(197, 3);
            this.btn_loadvppfile.Name = "btn_loadvppfile";
            this.btn_loadvppfile.Size = new System.Drawing.Size(97, 37);
            this.btn_loadvppfile.TabIndex = 54;
            this.btn_loadvppfile.Text = "Set Program";
            this.btn_loadvppfile.UseVisualStyleBackColor = true;
            this.btn_loadvppfile.Click += new System.EventHandler(this.btn_loadvppfile_Click);
            // 
            // label_Online
            // 
            this.label_Online.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Online.AutoSize = true;
            this.label_Online.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Online.Location = new System.Drawing.Point(2, 40);
            this.label_Online.Name = "label_Online";
            this.label_Online.Size = new System.Drawing.Size(119, 15);
            this.label_Online.TabIndex = 51;
            this.label_Online.Text = "System online status";
            this.label_Online.Click += new System.EventHandler(this.label_Online_Click);
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Controls.Add(this.btnRunCont, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.btnRun, 1, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 169);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(294, 41);
            this.tableLayoutPanel11.TabIndex = 56;
            // 
            // btnRunCont
            // 
            this.btnRunCont.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRunCont.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRunCont.Location = new System.Drawing.Point(11, 3);
            this.btnRunCont.Name = "btnRunCont";
            this.btnRunCont.Size = new System.Drawing.Size(133, 34);
            this.btnRunCont.TabIndex = 42;
            this.btnRunCont.Text = "Autorun";
            this.btnRunCont.Click += new System.EventHandler(this.btnRunCont_Click);
            // 
            // labelAppVersion
            // 
            this.labelAppVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAppVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.labelAppVersion.Location = new System.Drawing.Point(3, 574);
            this.labelAppVersion.Name = "labelAppVersion";
            this.labelAppVersion.Size = new System.Drawing.Size(294, 25);
            this.labelAppVersion.TabIndex = 57;
            this.labelAppVersion.Text = "1.0.0.3";
            this.labelAppVersion.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // applicationErrorProvider
            // 
            this.applicationErrorProvider.ContainerControl = this;
            // 
            // panelCameraDisplay
            // 
            this.panelCameraDisplay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelCameraDisplay.Controls.Add(this.panel2);
            this.panelCameraDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCameraDisplay.Location = new System.Drawing.Point(0, 0);
            this.panelCameraDisplay.Name = "panelCameraDisplay";
            this.panelCameraDisplay.Size = new System.Drawing.Size(913, 612);
            this.panelCameraDisplay.TabIndex = 36;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.tableLayoutPanel2);
            this.panel2.Location = new System.Drawing.Point(13, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(597, 284);
            this.panel2.TabIndex = 57;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.cogRecordsDisplay1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.labelCam2Status, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelCam1Status, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.cogRecordsDisplay2, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.3574F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.6426F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(597, 284);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // cogRecordsDisplay1
            // 
            this.cogRecordsDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cogRecordsDisplay1.Location = new System.Drawing.Point(3, 40);
            this.cogRecordsDisplay1.Name = "cogRecordsDisplay1";
            this.cogRecordsDisplay1.SelectedRecordKey = null;
            this.cogRecordsDisplay1.ShowRecordsDropDown = true;
            this.cogRecordsDisplay1.Size = new System.Drawing.Size(292, 241);
            this.cogRecordsDisplay1.Subject = null;
            this.cogRecordsDisplay1.TabIndex = 30;
            // 
            // labelCam2Status
            // 
            this.labelCam2Status.BackColor = System.Drawing.SystemColors.Control;
            this.labelCam2Status.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelCam2Status.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCam2Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCam2Status.Location = new System.Drawing.Point(301, 3);
            this.labelCam2Status.Margin = new System.Windows.Forms.Padding(3);
            this.labelCam2Status.Name = "labelCam2Status";
            this.labelCam2Status.Size = new System.Drawing.Size(293, 31);
            this.labelCam2Status.TabIndex = 58;
            this.labelCam2Status.Text = "Reject";
            this.labelCam2Status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCam1Status
            // 
            this.labelCam1Status.BackColor = System.Drawing.SystemColors.Control;
            this.labelCam1Status.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelCam1Status.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCam1Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCam1Status.Location = new System.Drawing.Point(3, 3);
            this.labelCam1Status.Margin = new System.Windows.Forms.Padding(3);
            this.labelCam1Status.Name = "labelCam1Status";
            this.labelCam1Status.Size = new System.Drawing.Size(292, 31);
            this.labelCam1Status.TabIndex = 27;
            this.labelCam1Status.Text = "Accept";
            this.labelCam1Status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cogRecordsDisplay2
            // 
            this.cogRecordsDisplay2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cogRecordsDisplay2.Location = new System.Drawing.Point(301, 40);
            this.cogRecordsDisplay2.Name = "cogRecordsDisplay2";
            this.cogRecordsDisplay2.SelectedRecordKey = null;
            this.cogRecordsDisplay2.ShowRecordsDropDown = true;
            this.cogRecordsDisplay2.Size = new System.Drawing.Size(293, 241);
            this.cogRecordsDisplay2.Subject = null;
            this.cogRecordsDisplay2.TabIndex = 30;
            // 
            // timerWorkStation
            // 
            this.timerWorkStation.Interval = 5;
            this.timerWorkStation.Tick += new System.EventHandler(this.timerWorkStation_Tick);
            // 
            // panelCharts
            // 
            this.panelCharts.AutoScroll = true;
            this.panelCharts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelCharts.Controls.Add(this.tableLayoutPanelChart8);
            this.panelCharts.Controls.Add(this.tableLayoutPanelChart7);
            this.panelCharts.Controls.Add(this.tableLayoutPanelChart6);
            this.panelCharts.Controls.Add(this.tableLayoutPanelChart5);
            this.panelCharts.Controls.Add(this.tableLayoutPanelChart4);
            this.panelCharts.Controls.Add(this.tableLayoutPanelChart3);
            this.panelCharts.Controls.Add(this.tableLayoutPanelChart2);
            this.panelCharts.Controls.Add(this.panelChart1);
            this.panelCharts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCharts.Location = new System.Drawing.Point(0, 0);
            this.panelCharts.Name = "panelCharts";
            this.panelCharts.Size = new System.Drawing.Size(598, 298);
            this.panelCharts.TabIndex = 38;
            // 
            // tableLayoutPanelChart8
            // 
            this.tableLayoutPanelChart8.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanelChart8.ColumnCount = 1;
            this.tableLayoutPanelChart8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelChart8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelChart8.Controls.Add(this.chart8, 0, 0);
            this.tableLayoutPanelChart8.Controls.Add(this.tableLayoutPanel9, 0, 1);
            this.tableLayoutPanelChart8.Location = new System.Drawing.Point(389, 399);
            this.tableLayoutPanelChart8.Name = "tableLayoutPanelChart8";
            this.tableLayoutPanelChart8.RowCount = 2;
            this.tableLayoutPanelChart8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 77F));
            this.tableLayoutPanelChart8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.tableLayoutPanelChart8.Size = new System.Drawing.Size(279, 121);
            this.tableLayoutPanelChart8.TabIndex = 0;
            // 
            // chart8
            // 
            verticalLineAnnotation1.AxisXName = "ChartArea1\\rX";
            verticalLineAnnotation1.Height = 1D;
            verticalLineAnnotation1.IsSizeAlwaysRelative = false;
            verticalLineAnnotation1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            verticalLineAnnotation1.Name = "VerticalLineAnnotation1";
            verticalLineAnnotation1.X = 2D;
            verticalLineAnnotation1.Y = 16.5D;
            verticalLineAnnotation1.YAxisName = "ChartArea1\\rY";
            this.chart8.Annotations.Add(verticalLineAnnotation1);
            this.chart8.BackColor = System.Drawing.Color.Black;
            chartArea1.AxisX.IsLabelAutoFit = false;
            chartArea1.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisX.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea1.AxisX.LineColor = System.Drawing.Color.DimGray;
            chartArea1.AxisX.LineWidth = 2;
            chartArea1.AxisX.MajorGrid.Enabled = false;
            chartArea1.AxisX.Maximum = 15D;
            chartArea1.AxisX.Minimum = 0D;
            chartArea1.AxisY.Interval = 0.5D;
            chartArea1.AxisY.IsLabelAutoFit = false;
            chartArea1.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisY.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea1.AxisY.LineColor = System.Drawing.Color.DimGray;
            chartArea1.AxisY.LineWidth = 2;
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray;
            chartArea1.AxisY.MajorGrid.LineWidth = 2;
            chartArea1.AxisY.Maximum = 17.5D;
            chartArea1.AxisY.Minimum = 16.5D;
            chartArea1.BackColor = System.Drawing.Color.Black;
            chartArea1.Name = "ChartArea1";
            this.chart8.ChartAreas.Add(chartArea1);
            this.chart8.Location = new System.Drawing.Point(3, 3);
            this.chart8.Name = "chart8";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Name = "Series1";
            this.chart8.Series.Add(series1);
            this.chart8.Size = new System.Drawing.Size(273, 87);
            this.chart8.TabIndex = 0;
            this.chart8.Text = "chart1";
            title1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            title1.Name = "Title1";
            title1.Text = "Chart Title";
            this.chart8.Titles.Add(title1);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tableLayoutPanel9.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.21978F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.78022F));
            this.tableLayoutPanel9.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.textBoxVal_8, 1, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 96);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(273, 22);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(4, 1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(210, 20);
            this.label10.TabIndex = 1;
            this.label10.Text = "Current Value";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVal_8
            // 
            this.textBoxVal_8.BackColor = System.Drawing.Color.White;
            this.textBoxVal_8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxVal_8.Location = new System.Drawing.Point(219, 2);
            this.textBoxVal_8.Margin = new System.Windows.Forms.Padding(1);
            this.textBoxVal_8.Name = "textBoxVal_8";
            this.textBoxVal_8.Size = new System.Drawing.Size(52, 20);
            this.textBoxVal_8.TabIndex = 2;
            // 
            // tableLayoutPanelChart7
            // 
            this.tableLayoutPanelChart7.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanelChart7.ColumnCount = 1;
            this.tableLayoutPanelChart7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelChart7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelChart7.Controls.Add(this.chart7, 0, 0);
            this.tableLayoutPanelChart7.Controls.Add(this.tableLayoutPanel8, 0, 1);
            this.tableLayoutPanelChart7.Location = new System.Drawing.Point(36, 399);
            this.tableLayoutPanelChart7.Name = "tableLayoutPanelChart7";
            this.tableLayoutPanelChart7.RowCount = 2;
            this.tableLayoutPanelChart7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 77F));
            this.tableLayoutPanelChart7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.tableLayoutPanelChart7.Size = new System.Drawing.Size(279, 121);
            this.tableLayoutPanelChart7.TabIndex = 0;
            // 
            // chart7
            // 
            verticalLineAnnotation2.AxisXName = "ChartArea1\\rX";
            verticalLineAnnotation2.Height = 1D;
            verticalLineAnnotation2.IsSizeAlwaysRelative = false;
            verticalLineAnnotation2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            verticalLineAnnotation2.Name = "VerticalLineAnnotation1";
            verticalLineAnnotation2.X = 2D;
            verticalLineAnnotation2.Y = 16.5D;
            verticalLineAnnotation2.YAxisName = "ChartArea1\\rY";
            this.chart7.Annotations.Add(verticalLineAnnotation2);
            this.chart7.BackColor = System.Drawing.Color.Black;
            chartArea2.AxisX.IsLabelAutoFit = false;
            chartArea2.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea2.AxisX.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea2.AxisX.LineColor = System.Drawing.Color.DimGray;
            chartArea2.AxisX.LineWidth = 2;
            chartArea2.AxisX.MajorGrid.Enabled = false;
            chartArea2.AxisX.Maximum = 15D;
            chartArea2.AxisX.Minimum = 0D;
            chartArea2.AxisY.Interval = 0.5D;
            chartArea2.AxisY.IsLabelAutoFit = false;
            chartArea2.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea2.AxisY.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea2.AxisY.LineColor = System.Drawing.Color.DimGray;
            chartArea2.AxisY.LineWidth = 2;
            chartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray;
            chartArea2.AxisY.MajorGrid.LineWidth = 2;
            chartArea2.AxisY.Maximum = 17.5D;
            chartArea2.AxisY.Minimum = 16.5D;
            chartArea2.BackColor = System.Drawing.Color.Black;
            chartArea2.Name = "ChartArea1";
            this.chart7.ChartAreas.Add(chartArea2);
            this.chart7.Location = new System.Drawing.Point(3, 3);
            this.chart7.Name = "chart7";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Name = "Series1";
            this.chart7.Series.Add(series2);
            this.chart7.Size = new System.Drawing.Size(273, 87);
            this.chart7.TabIndex = 0;
            this.chart7.Text = "chart1";
            title2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            title2.Name = "Title1";
            title2.Text = "Chart Title";
            this.chart7.Titles.Add(title2);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tableLayoutPanel8.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.21978F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.78022F));
            this.tableLayoutPanel8.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.textBoxVal_7, 1, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 96);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(273, 22);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(4, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(210, 20);
            this.label9.TabIndex = 1;
            this.label9.Text = "Current Value";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVal_7
            // 
            this.textBoxVal_7.BackColor = System.Drawing.Color.White;
            this.textBoxVal_7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxVal_7.Location = new System.Drawing.Point(219, 2);
            this.textBoxVal_7.Margin = new System.Windows.Forms.Padding(1);
            this.textBoxVal_7.Name = "textBoxVal_7";
            this.textBoxVal_7.Size = new System.Drawing.Size(52, 20);
            this.textBoxVal_7.TabIndex = 2;
            // 
            // tableLayoutPanelChart6
            // 
            this.tableLayoutPanelChart6.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanelChart6.ColumnCount = 1;
            this.tableLayoutPanelChart6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelChart6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelChart6.Controls.Add(this.chart6, 0, 0);
            this.tableLayoutPanelChart6.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanelChart6.Location = new System.Drawing.Point(389, 272);
            this.tableLayoutPanelChart6.Name = "tableLayoutPanelChart6";
            this.tableLayoutPanelChart6.RowCount = 2;
            this.tableLayoutPanelChart6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 77F));
            this.tableLayoutPanelChart6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.tableLayoutPanelChart6.Size = new System.Drawing.Size(279, 121);
            this.tableLayoutPanelChart6.TabIndex = 0;
            // 
            // chart6
            // 
            verticalLineAnnotation3.AxisXName = "ChartArea1\\rX";
            verticalLineAnnotation3.Height = 1D;
            verticalLineAnnotation3.IsSizeAlwaysRelative = false;
            verticalLineAnnotation3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            verticalLineAnnotation3.Name = "VerticalLineAnnotation1";
            verticalLineAnnotation3.X = 2D;
            verticalLineAnnotation3.Y = 16.5D;
            verticalLineAnnotation3.YAxisName = "ChartArea1\\rY";
            this.chart6.Annotations.Add(verticalLineAnnotation3);
            this.chart6.BackColor = System.Drawing.Color.Black;
            chartArea3.AxisX.IsLabelAutoFit = false;
            chartArea3.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea3.AxisX.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea3.AxisX.LineColor = System.Drawing.Color.DimGray;
            chartArea3.AxisX.LineWidth = 2;
            chartArea3.AxisX.MajorGrid.Enabled = false;
            chartArea3.AxisX.Maximum = 15D;
            chartArea3.AxisX.Minimum = 0D;
            chartArea3.AxisY.Interval = 0.5D;
            chartArea3.AxisY.IsLabelAutoFit = false;
            chartArea3.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea3.AxisY.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea3.AxisY.LineColor = System.Drawing.Color.DimGray;
            chartArea3.AxisY.LineWidth = 2;
            chartArea3.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray;
            chartArea3.AxisY.MajorGrid.LineWidth = 2;
            chartArea3.AxisY.Maximum = 17.5D;
            chartArea3.AxisY.Minimum = 16.5D;
            chartArea3.BackColor = System.Drawing.Color.Black;
            chartArea3.Name = "ChartArea1";
            this.chart6.ChartAreas.Add(chartArea3);
            this.chart6.Location = new System.Drawing.Point(3, 3);
            this.chart6.Name = "chart6";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Name = "Series1";
            this.chart6.Series.Add(series3);
            this.chart6.Size = new System.Drawing.Size(273, 87);
            this.chart6.TabIndex = 0;
            this.chart6.Text = "chart1";
            title3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            title3.Name = "Title1";
            title3.Text = "Chart Title";
            this.chart6.Titles.Add(title3);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tableLayoutPanel6.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.21978F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.78022F));
            this.tableLayoutPanel6.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.textBoxVal_6, 1, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 96);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(273, 22);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(4, 1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(210, 20);
            this.label8.TabIndex = 1;
            this.label8.Text = "Current Value";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVal_6
            // 
            this.textBoxVal_6.BackColor = System.Drawing.Color.White;
            this.textBoxVal_6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxVal_6.Location = new System.Drawing.Point(219, 2);
            this.textBoxVal_6.Margin = new System.Windows.Forms.Padding(1);
            this.textBoxVal_6.Name = "textBoxVal_6";
            this.textBoxVal_6.Size = new System.Drawing.Size(52, 20);
            this.textBoxVal_6.TabIndex = 2;
            // 
            // tableLayoutPanelChart5
            // 
            this.tableLayoutPanelChart5.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanelChart5.ColumnCount = 1;
            this.tableLayoutPanelChart5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelChart5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelChart5.Controls.Add(this.chart5, 0, 0);
            this.tableLayoutPanelChart5.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanelChart5.Location = new System.Drawing.Point(36, 273);
            this.tableLayoutPanelChart5.Name = "tableLayoutPanelChart5";
            this.tableLayoutPanelChart5.RowCount = 2;
            this.tableLayoutPanelChart5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 77F));
            this.tableLayoutPanelChart5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.tableLayoutPanelChart5.Size = new System.Drawing.Size(279, 121);
            this.tableLayoutPanelChart5.TabIndex = 0;
            // 
            // chart5
            // 
            verticalLineAnnotation4.AxisXName = "ChartArea1\\rX";
            verticalLineAnnotation4.Height = 1D;
            verticalLineAnnotation4.IsSizeAlwaysRelative = false;
            verticalLineAnnotation4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            verticalLineAnnotation4.Name = "VerticalLineAnnotation1";
            verticalLineAnnotation4.X = 2D;
            verticalLineAnnotation4.Y = 16.5D;
            verticalLineAnnotation4.YAxisName = "ChartArea1\\rY";
            this.chart5.Annotations.Add(verticalLineAnnotation4);
            this.chart5.BackColor = System.Drawing.Color.Black;
            chartArea4.AxisX.IsLabelAutoFit = false;
            chartArea4.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea4.AxisX.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea4.AxisX.LineColor = System.Drawing.Color.DimGray;
            chartArea4.AxisX.LineWidth = 2;
            chartArea4.AxisX.MajorGrid.Enabled = false;
            chartArea4.AxisX.Maximum = 15D;
            chartArea4.AxisX.Minimum = 0D;
            chartArea4.AxisY.Interval = 0.5D;
            chartArea4.AxisY.IsLabelAutoFit = false;
            chartArea4.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea4.AxisY.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea4.AxisY.LineColor = System.Drawing.Color.DimGray;
            chartArea4.AxisY.LineWidth = 2;
            chartArea4.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray;
            chartArea4.AxisY.MajorGrid.LineWidth = 2;
            chartArea4.AxisY.Maximum = 17.5D;
            chartArea4.AxisY.Minimum = 16.5D;
            chartArea4.BackColor = System.Drawing.Color.Black;
            chartArea4.Name = "ChartArea1";
            this.chart5.ChartAreas.Add(chartArea4);
            this.chart5.Location = new System.Drawing.Point(3, 3);
            this.chart5.Name = "chart5";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Name = "Series1";
            this.chart5.Series.Add(series4);
            this.chart5.Size = new System.Drawing.Size(273, 87);
            this.chart5.TabIndex = 0;
            this.chart5.Text = "chart1";
            title4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            title4.Name = "Title1";
            title4.Text = "Chart Title";
            this.chart5.Titles.Add(title4);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.21978F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.78022F));
            this.tableLayoutPanel4.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.textBoxVal_5, 1, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 96);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(273, 22);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(4, 1);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(210, 20);
            this.label7.TabIndex = 1;
            this.label7.Text = "Current Value";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVal_5
            // 
            this.textBoxVal_5.BackColor = System.Drawing.Color.White;
            this.textBoxVal_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxVal_5.Location = new System.Drawing.Point(219, 2);
            this.textBoxVal_5.Margin = new System.Windows.Forms.Padding(1);
            this.textBoxVal_5.Name = "textBoxVal_5";
            this.textBoxVal_5.Size = new System.Drawing.Size(52, 20);
            this.textBoxVal_5.TabIndex = 2;
            // 
            // tableLayoutPanelChart4
            // 
            this.tableLayoutPanelChart4.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanelChart4.ColumnCount = 1;
            this.tableLayoutPanelChart4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelChart4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelChart4.Controls.Add(this.chart4, 0, 0);
            this.tableLayoutPanelChart4.Controls.Add(this.tableLayoutPanel7, 0, 1);
            this.tableLayoutPanelChart4.Location = new System.Drawing.Point(389, 145);
            this.tableLayoutPanelChart4.Name = "tableLayoutPanelChart4";
            this.tableLayoutPanelChart4.RowCount = 2;
            this.tableLayoutPanelChart4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 77F));
            this.tableLayoutPanelChart4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.tableLayoutPanelChart4.Size = new System.Drawing.Size(279, 121);
            this.tableLayoutPanelChart4.TabIndex = 0;
            // 
            // chart4
            // 
            verticalLineAnnotation5.AxisXName = "ChartArea1\\rX";
            verticalLineAnnotation5.Height = 1D;
            verticalLineAnnotation5.IsSizeAlwaysRelative = false;
            verticalLineAnnotation5.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            verticalLineAnnotation5.Name = "VerticalLineAnnotation1";
            verticalLineAnnotation5.X = 2D;
            verticalLineAnnotation5.Y = 16.5D;
            verticalLineAnnotation5.YAxisName = "ChartArea1\\rY";
            this.chart4.Annotations.Add(verticalLineAnnotation5);
            this.chart4.BackColor = System.Drawing.Color.Black;
            chartArea5.AxisX.IsLabelAutoFit = false;
            chartArea5.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea5.AxisX.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea5.AxisX.LineColor = System.Drawing.Color.DimGray;
            chartArea5.AxisX.LineWidth = 2;
            chartArea5.AxisX.MajorGrid.Enabled = false;
            chartArea5.AxisX.Maximum = 15D;
            chartArea5.AxisX.Minimum = 0D;
            chartArea5.AxisY.Interval = 0.5D;
            chartArea5.AxisY.IsLabelAutoFit = false;
            chartArea5.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea5.AxisY.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea5.AxisY.LineColor = System.Drawing.Color.DimGray;
            chartArea5.AxisY.LineWidth = 2;
            chartArea5.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray;
            chartArea5.AxisY.MajorGrid.LineWidth = 2;
            chartArea5.AxisY.Maximum = 17.5D;
            chartArea5.AxisY.Minimum = 16.5D;
            chartArea5.BackColor = System.Drawing.Color.Black;
            chartArea5.Name = "ChartArea1";
            this.chart4.ChartAreas.Add(chartArea5);
            this.chart4.Location = new System.Drawing.Point(3, 3);
            this.chart4.Name = "chart4";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Name = "Series1";
            this.chart4.Series.Add(series5);
            this.chart4.Size = new System.Drawing.Size(273, 87);
            this.chart4.TabIndex = 0;
            this.chart4.Text = "chart1";
            title5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            title5.Name = "Title1";
            title5.Text = "Chart Title";
            this.chart4.Titles.Add(title5);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tableLayoutPanel7.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.21978F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.78022F));
            this.tableLayoutPanel7.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.textBoxVal_4, 1, 0);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 96);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(273, 22);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(4, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(210, 20);
            this.label6.TabIndex = 1;
            this.label6.Text = "Current Value";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVal_4
            // 
            this.textBoxVal_4.BackColor = System.Drawing.Color.White;
            this.textBoxVal_4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxVal_4.Location = new System.Drawing.Point(219, 2);
            this.textBoxVal_4.Margin = new System.Windows.Forms.Padding(1);
            this.textBoxVal_4.Name = "textBoxVal_4";
            this.textBoxVal_4.Size = new System.Drawing.Size(52, 20);
            this.textBoxVal_4.TabIndex = 2;
            // 
            // tableLayoutPanelChart3
            // 
            this.tableLayoutPanelChart3.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanelChart3.ColumnCount = 1;
            this.tableLayoutPanelChart3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelChart3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelChart3.Controls.Add(this.chart3, 0, 0);
            this.tableLayoutPanelChart3.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanelChart3.Location = new System.Drawing.Point(36, 146);
            this.tableLayoutPanelChart3.Name = "tableLayoutPanelChart3";
            this.tableLayoutPanelChart3.RowCount = 2;
            this.tableLayoutPanelChart3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 77F));
            this.tableLayoutPanelChart3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.tableLayoutPanelChart3.Size = new System.Drawing.Size(279, 121);
            this.tableLayoutPanelChart3.TabIndex = 0;
            // 
            // chart3
            // 
            verticalLineAnnotation6.AxisXName = "ChartArea1\\rX";
            verticalLineAnnotation6.Height = 1D;
            verticalLineAnnotation6.IsSizeAlwaysRelative = false;
            verticalLineAnnotation6.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            verticalLineAnnotation6.Name = "VerticalLineAnnotation1";
            verticalLineAnnotation6.X = 2D;
            verticalLineAnnotation6.Y = 16.5D;
            verticalLineAnnotation6.YAxisName = "ChartArea1\\rY";
            this.chart3.Annotations.Add(verticalLineAnnotation6);
            this.chart3.BackColor = System.Drawing.Color.Black;
            chartArea6.AxisX.IsLabelAutoFit = false;
            chartArea6.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea6.AxisX.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea6.AxisX.LineColor = System.Drawing.Color.DimGray;
            chartArea6.AxisX.LineWidth = 2;
            chartArea6.AxisX.MajorGrid.Enabled = false;
            chartArea6.AxisX.Maximum = 15D;
            chartArea6.AxisX.Minimum = 0D;
            chartArea6.AxisY.Interval = 0.5D;
            chartArea6.AxisY.IsLabelAutoFit = false;
            chartArea6.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea6.AxisY.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea6.AxisY.LineColor = System.Drawing.Color.DimGray;
            chartArea6.AxisY.LineWidth = 2;
            chartArea6.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray;
            chartArea6.AxisY.MajorGrid.LineWidth = 2;
            chartArea6.AxisY.Maximum = 17.5D;
            chartArea6.AxisY.Minimum = 16.5D;
            chartArea6.BackColor = System.Drawing.Color.Black;
            chartArea6.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea6);
            this.chart3.Location = new System.Drawing.Point(3, 3);
            this.chart3.Name = "chart3";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Name = "Series1";
            this.chart3.Series.Add(series6);
            this.chart3.Size = new System.Drawing.Size(273, 87);
            this.chart3.TabIndex = 0;
            this.chart3.Text = "chart1";
            title6.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            title6.Name = "Title1";
            title6.Text = "Chart Title";
            this.chart3.Titles.Add(title6);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tableLayoutPanel5.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.21978F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.78022F));
            this.tableLayoutPanel5.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.textBoxVal_3, 1, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 96);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(273, 22);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(4, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(210, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = "Current Value";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVal_3
            // 
            this.textBoxVal_3.BackColor = System.Drawing.Color.White;
            this.textBoxVal_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxVal_3.Location = new System.Drawing.Point(219, 2);
            this.textBoxVal_3.Margin = new System.Windows.Forms.Padding(1);
            this.textBoxVal_3.Name = "textBoxVal_3";
            this.textBoxVal_3.Size = new System.Drawing.Size(52, 20);
            this.textBoxVal_3.TabIndex = 2;
            // 
            // tableLayoutPanelChart2
            // 
            this.tableLayoutPanelChart2.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanelChart2.ColumnCount = 1;
            this.tableLayoutPanelChart2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelChart2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelChart2.Controls.Add(this.chart2, 0, 0);
            this.tableLayoutPanelChart2.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanelChart2.Location = new System.Drawing.Point(389, 19);
            this.tableLayoutPanelChart2.Name = "tableLayoutPanelChart2";
            this.tableLayoutPanelChart2.RowCount = 2;
            this.tableLayoutPanelChart2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 77F));
            this.tableLayoutPanelChart2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.tableLayoutPanelChart2.Size = new System.Drawing.Size(279, 121);
            this.tableLayoutPanelChart2.TabIndex = 0;
            // 
            // chart2
            // 
            verticalLineAnnotation7.AxisXName = "ChartArea1\\rX";
            verticalLineAnnotation7.Height = 1D;
            verticalLineAnnotation7.IsSizeAlwaysRelative = false;
            verticalLineAnnotation7.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            verticalLineAnnotation7.Name = "VerticalLineAnnotation1";
            verticalLineAnnotation7.X = 2D;
            verticalLineAnnotation7.Y = 16.5D;
            verticalLineAnnotation7.YAxisName = "ChartArea1\\rY";
            this.chart2.Annotations.Add(verticalLineAnnotation7);
            this.chart2.BackColor = System.Drawing.Color.Black;
            chartArea7.AxisX.IsLabelAutoFit = false;
            chartArea7.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea7.AxisX.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea7.AxisX.LineColor = System.Drawing.Color.DimGray;
            chartArea7.AxisX.LineWidth = 2;
            chartArea7.AxisX.MajorGrid.Enabled = false;
            chartArea7.AxisX.Maximum = 15D;
            chartArea7.AxisX.Minimum = 0D;
            chartArea7.AxisY.Interval = 0.5D;
            chartArea7.AxisY.IsLabelAutoFit = false;
            chartArea7.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea7.AxisY.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea7.AxisY.LineColor = System.Drawing.Color.DimGray;
            chartArea7.AxisY.LineWidth = 2;
            chartArea7.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray;
            chartArea7.AxisY.MajorGrid.LineWidth = 2;
            chartArea7.AxisY.Maximum = 17.5D;
            chartArea7.AxisY.Minimum = 16.5D;
            chartArea7.BackColor = System.Drawing.Color.Black;
            chartArea7.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea7);
            this.chart2.Location = new System.Drawing.Point(3, 3);
            this.chart2.Name = "chart2";
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series7.Name = "Series1";
            this.chart2.Series.Add(series7);
            this.chart2.Size = new System.Drawing.Size(273, 87);
            this.chart2.TabIndex = 0;
            this.chart2.Text = "chart1";
            title7.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            title7.Name = "Title1";
            title7.Text = "Chart Title";
            this.chart2.Titles.Add(title7);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.21978F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.78022F));
            this.tableLayoutPanel3.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.textBoxVal_2, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 96);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(273, 22);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(4, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(210, 20);
            this.label4.TabIndex = 1;
            this.label4.Text = "Current Value";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVal_2
            // 
            this.textBoxVal_2.BackColor = System.Drawing.Color.White;
            this.textBoxVal_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxVal_2.Location = new System.Drawing.Point(219, 2);
            this.textBoxVal_2.Margin = new System.Windows.Forms.Padding(1);
            this.textBoxVal_2.Name = "textBoxVal_2";
            this.textBoxVal_2.Size = new System.Drawing.Size(52, 20);
            this.textBoxVal_2.TabIndex = 2;
            // 
            // panelChart1
            // 
            this.panelChart1.Controls.Add(this.tableLayoutPanelChart1);
            this.panelChart1.Location = new System.Drawing.Point(36, 19);
            this.panelChart1.Name = "panelChart1";
            this.panelChart1.Size = new System.Drawing.Size(279, 125);
            this.panelChart1.TabIndex = 1;
            // 
            // tableLayoutPanelChart1
            // 
            this.tableLayoutPanelChart1.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanelChart1.ColumnCount = 1;
            this.tableLayoutPanelChart1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelChart1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelChart1.Controls.Add(this.chart1, 0, 0);
            this.tableLayoutPanelChart1.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanelChart1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelChart1.Name = "tableLayoutPanelChart1";
            this.tableLayoutPanelChart1.RowCount = 2;
            this.tableLayoutPanelChart1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 77F));
            this.tableLayoutPanelChart1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.tableLayoutPanelChart1.Size = new System.Drawing.Size(279, 121);
            this.tableLayoutPanelChart1.TabIndex = 0;
            // 
            // chart1
            // 
            verticalLineAnnotation8.AxisXName = "ChartArea1\\rX";
            verticalLineAnnotation8.Height = 1D;
            verticalLineAnnotation8.IsSizeAlwaysRelative = false;
            verticalLineAnnotation8.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            verticalLineAnnotation8.Name = "VerticalLineAnnotation1";
            verticalLineAnnotation8.X = 2D;
            verticalLineAnnotation8.Y = 16.5D;
            verticalLineAnnotation8.YAxisName = "ChartArea1\\rY";
            this.chart1.Annotations.Add(verticalLineAnnotation8);
            this.chart1.BackColor = System.Drawing.Color.Black;
            chartArea8.AxisX.IsLabelAutoFit = false;
            chartArea8.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea8.AxisX.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea8.AxisX.LineColor = System.Drawing.Color.DimGray;
            chartArea8.AxisX.LineWidth = 2;
            chartArea8.AxisX.MajorGrid.Enabled = false;
            chartArea8.AxisX.Maximum = 15D;
            chartArea8.AxisX.Minimum = 0D;
            chartArea8.AxisY.Interval = 0.5D;
            chartArea8.AxisY.IsLabelAutoFit = false;
            chartArea8.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea8.AxisY.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea8.AxisY.LineColor = System.Drawing.Color.DimGray;
            chartArea8.AxisY.LineWidth = 2;
            chartArea8.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray;
            chartArea8.AxisY.MajorGrid.LineWidth = 2;
            chartArea8.AxisY.Maximum = 17.5D;
            chartArea8.AxisY.Minimum = 16.5D;
            chartArea8.BackColor = System.Drawing.Color.Black;
            chartArea8.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea8);
            this.chart1.Location = new System.Drawing.Point(3, 3);
            this.chart1.Name = "chart1";
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series8.Name = "Series1";
            this.chart1.Series.Add(series8);
            this.chart1.Size = new System.Drawing.Size(273, 87);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            title8.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            title8.Name = "Title1";
            title8.Text = "Chart Title";
            this.chart1.Titles.Add(title8);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.21978F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.78022F));
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBoxVal_1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 96);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(273, 22);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(4, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "Current Value";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVal_1
            // 
            this.textBoxVal_1.BackColor = System.Drawing.Color.White;
            this.textBoxVal_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxVal_1.Location = new System.Drawing.Point(219, 2);
            this.textBoxVal_1.Margin = new System.Windows.Forms.Padding(1);
            this.textBoxVal_1.Name = "textBoxVal_1";
            this.textBoxVal_1.Size = new System.Drawing.Size(52, 20);
            this.textBoxVal_1.TabIndex = 2;
            // 
            // panelChartParentContainer
            // 
            this.panelChartParentContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelChartParentContainer.Controls.Add(this.panelCharts);
            this.panelChartParentContainer.Location = new System.Drawing.Point(13, 304);
            this.panelChartParentContainer.Name = "panelChartParentContainer";
            this.panelChartParentContainer.Size = new System.Drawing.Size(598, 298);
            this.panelChartParentContainer.TabIndex = 0;
            // 
            // cogToolPropertyProvider0
            // 
            this.cogToolPropertyProvider0.ElectricProvider = null;
            this.cogToolPropertyProvider0.EnableDelegateQueuing = false;
            this.cogToolPropertyProvider0.ErrorProvider = null;
            this.cogToolPropertyProvider0.Subject = null;
            this.cogToolPropertyProvider0.SubjectInUse = false;
            // 
            // cogToolPropertyProvider1
            // 
            this.cogToolPropertyProvider1.ElectricProvider = null;
            this.cogToolPropertyProvider1.EnableDelegateQueuing = false;
            this.cogToolPropertyProvider1.ErrorProvider = null;
            this.cogToolPropertyProvider1.Subject = null;
            this.cogToolPropertyProvider1.SubjectInUse = false;
            // 
            // VisionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelChartParentContainer);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panelCameraDisplay);
            this.Name = "VisionControl";
            this.Size = new System.Drawing.Size(913, 612);
            this.Load += new System.EventHandler(this.VisionControl_Load);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbl_ProgramFile)).EndInit();
            this.tabControl_ProductionParams.ResumeLayout(false);
            this.tabPage_JobN_JobStatistics.ResumeLayout(false);
            this.tabPage_JobN_JobStatistics.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage_Configuration.ResumeLayout(false);
            this.tabReport.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.applicationErrorProvider)).EndInit();
            this.panelCameraDisplay.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panelCharts.ResumeLayout(false);
            this.tableLayoutPanelChart8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart8)).EndInit();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanelChart7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart7)).EndInit();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanelChart6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart6)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanelChart5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanelChart4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).EndInit();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanelChart3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanelChart2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.panelChart1.ResumeLayout(false);
            this.tableLayoutPanelChart1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panelChartParentContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnRunCont;
        private System.Windows.Forms.Label label_Online;
        private System.Windows.Forms.ErrorProvider applicationErrorProvider;
        private System.Windows.Forms.Panel panelCameraDisplay;
        private System.Windows.Forms.Label label_controlErrorMessage;
        private PictureBox lbl_ProgramFile;
        private ComboBox comboBox_Vpp_Files;
        private Panel panel7;
        private FolderBrowserDialog fldrBrowser_ResultsPath;
        private Button btn_loadvppfile;
        private Label label2;
        private Timer timerWorkStation;
        private Panel panelCharts;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private Panel panelChart1;
        private TableLayoutPanel tableLayoutPanelChart1;
        private TableLayoutPanel tableLayoutPanel1;
        private Label label3;
        private TextBox textBoxVal_1;
        private TableLayoutPanel tableLayoutPanelChart2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private TableLayoutPanel tableLayoutPanel3;
        private Label label4;
        private TextBox textBoxVal_2;
        private TableLayoutPanel tableLayoutPanelChart4;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart4;
        private TableLayoutPanel tableLayoutPanel7;
        private Label label6;
        private TextBox textBoxVal_4;
        private TableLayoutPanel tableLayoutPanelChart3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
        private TableLayoutPanel tableLayoutPanel5;
        private Label label5;
        private TextBox textBoxVal_3;
        private TableLayoutPanel tableLayoutPanelChart8;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart8;
        private TableLayoutPanel tableLayoutPanel9;
        private Label label10;
        private TextBox textBoxVal_8;
        private TableLayoutPanel tableLayoutPanelChart7;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart7;
        private TableLayoutPanel tableLayoutPanel8;
        private Label label9;
        private TextBox textBoxVal_7;
        private TableLayoutPanel tableLayoutPanelChart6;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart6;
        private TableLayoutPanel tableLayoutPanel6;
        private Label label8;
        private TextBox textBoxVal_6;
        private TableLayoutPanel tableLayoutPanelChart5;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart5;
        private TableLayoutPanel tableLayoutPanel4;
        private Label label7;
        private TextBox textBoxVal_5;
        private Panel panelChartParentContainer;
        private Panel panel2;
        private TableLayoutPanel tableLayoutPanel2;
        private Cognex.VisionPro.CogRecordsDisplay cogRecordsDisplay1;
        private Label labelCam2Status;
        private Label labelCam1Status;
        private Cognex.VisionPro.CogRecordsDisplay cogRecordsDisplay2;
        private TabControl tabControl_ProductionParams;
        private TabPage tabPage_JobN_JobStatistics;
        private TabPage tabPage_Configuration;
        private Button button_Configuration;
        private ComboBox comboBox_Login;
        private Label label_Login;
        private TableLayoutPanel tableLayoutPanel10;
        private TableLayoutPanel tableLayoutPanel11;
        private Label labelAppVersion;
        private Button buttonSetDimensionParams;
        private Cognex.VisionPro.CogToolPropertyProvider cogToolPropertyProvider0;
        private Cognex.VisionPro.CogToolPropertyProvider cogToolPropertyProvider1;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private Label label17;
        private TextBox textBoxRCRejected;
        private TextBox textBoxRCReset_Rejected;
        private Label label20;
        private Label label18;
        private Label label19;
        private Button buttonRCYieldReset;
        private Label label16;
        private Label label15;
        private TextBox textBoxResetUtilisation;
        private Label label14;
        private TextBox textBoxTotalUtilisation;
        private Button buttonUtilisationDataReset;
        private TextBox textBoxProducedReset;
        private TextBox textBoxProducedTotal;
        private Label label13;
        private Label label12;
        private Label label11;
        private TextBox textBoxIndexedReset;
        private TextBox textBoxIndexedTotal;
        private Label label1;
        private GroupBox groupBox3;
        private Label label22;
        private TextBox textBoxICRejected;
        private TextBox textBoxICResetRejected;
        private Button buttonICReset;
        private Label label25;
        private Label label26;
        private Label label28;
        private Label label27;
        private TextBox textBoxRCYield;
        private TextBox textBoxRCResetYield;
        private Label label30;
        private Label label29;
        private TextBox textBoxICYield;
        private TextBox textBoxICResetYield;
        private Label label21;
        private Label label31;
        private TextBox textBoxTotalYield;
        private Label label23;
        private TextBox textBoxTotalResetYield;
        private Label label24;
        private Label label33;
        private Label label32;
        private Button btnLVDTReset;
        private TabPage tabReport;
        private Button buttonGenerateReport;
        private Panel panel8;
        private Label label34;
        private Button btn_setResultsPath;
        private TextBox txt_results_path;
        private Button buttonProductionParams;
        private Label label37;
    }
}
