﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.CodeDom.Compiler;
using System.Diagnostics;
using Microsoft.CSharp;
using System.IO;
using System.Collections;

namespace Saatvik.VisionPro.VPPEncryptor
{
    public partial class VPPEncryptor : Form
    {

        BindingList<VppFile> data = new BindingList<VppFile>();

        public VPPEncryptor()
        {
            InitializeComponent();
            VppFileGrid.DataSource = data;
        }

        private void btnUploadVppFile_Click(object sender, EventArgs e)
        {
            vpp_fileDialog.CheckFileExists = true;
            vpp_fileDialog.AddExtension = true;
            vpp_fileDialog.Multiselect = true;
            vpp_fileDialog.Filter = "VPP files (*.vpp)|*.vpp";
            if (vpp_fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                foreach (string fileName in vpp_fileDialog.FileNames)
                {
                    if (!((IEnumerable)data).Cast<dynamic>().Any(item => item.FilePath == fileName)) {
                        data.Add(new VppFile { FileName = fileName, FilePath = fileName });
                    }
                    
                }
               
                VppFileGrid.Update();
                VppFileGrid.Invalidate();
                VppFileGrid.Refresh();
            }
        }

        private void GenerateDLL()
        {
            CompilerParameters parameters = new CompilerParameters();
            foreach (var item in data)
            {
                parameters.EmbeddedResources.Add(item.FilePath);
            }
            parameters.GenerateExecutable = false;
            parameters.OutputAssembly = "ProgramLoader.dll";
            parameters.ReferencedAssemblies.Add("System.dll");
            parameters.ReferencedAssemblies.Add("mscorlib.dll");
            CompilerResults result = CodeDomProvider.CreateProvider("CSharp").CompileAssemblyFromSource(parameters, @"
            using System;
            using System.Reflection;
            using System.IO;
            namespace Saathvik
            {
                public static class ProgramLoader
                {
                    public static string[] GetprogramList()
                    {
                        Assembly currentAssembly = Assembly.GetExecutingAssembly();
                        return currentAssembly.GetManifestResourceNames();
                    }

                    public static Stream GetProgram(string programName)
                    {
                        Assembly currentAssembly = Assembly.GetExecutingAssembly();
                        return currentAssembly.GetManifestResourceStream(programName);                       
                    }
                }
            }");

            MessageBox.Show("DLL generated successfully", "Done!");
            data = new BindingList<VppFile>();
            VppFileGrid.DataSource = data;
            VppFileGrid.Refresh();
        }

        private void btn_GenerateDll_Click(object sender, EventArgs e)
        {
            GenerateDLL();
        }
    }

    public class VppFile
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}
