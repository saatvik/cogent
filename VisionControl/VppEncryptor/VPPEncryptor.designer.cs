﻿namespace Saatvik.VisionPro.VPPEncryptor
{
    partial class VPPEncryptor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VPPEncryptor));
            this.pnl_main = new System.Windows.Forms.Panel();
            this.btn_GenerateDll = new System.Windows.Forms.Button();
            this.VppFileGrid = new System.Windows.Forms.DataGridView();
            this.clmFileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmFilePath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnUploadVppFile = new System.Windows.Forms.Button();
            this.vpp_fileDialog = new System.Windows.Forms.OpenFileDialog();
            this.pnl_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VppFileGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // pnl_main
            // 
            this.pnl_main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnl_main.Controls.Add(this.btn_GenerateDll);
            this.pnl_main.Controls.Add(this.VppFileGrid);
            this.pnl_main.Controls.Add(this.btnUploadVppFile);
            this.pnl_main.Location = new System.Drawing.Point(2, 2);
            this.pnl_main.Name = "pnl_main";
            this.pnl_main.Size = new System.Drawing.Size(746, 380);
            this.pnl_main.TabIndex = 0;
            // 
            // btn_GenerateDll
            // 
            this.btn_GenerateDll.Location = new System.Drawing.Point(540, 309);
            this.btn_GenerateDll.Name = "btn_GenerateDll";
            this.btn_GenerateDll.Size = new System.Drawing.Size(196, 41);
            this.btn_GenerateDll.TabIndex = 2;
            this.btn_GenerateDll.Text = "Generate DLL";
            this.btn_GenerateDll.UseVisualStyleBackColor = true;
            this.btn_GenerateDll.Click += new System.EventHandler(this.btn_GenerateDll_Click);
            // 
            // VppFileGrid
            // 
            this.VppFileGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VppFileGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmFileName,
            this.clmFilePath});
            this.VppFileGrid.Location = new System.Drawing.Point(10, 79);
            this.VppFileGrid.Name = "VppFileGrid";
            this.VppFileGrid.Size = new System.Drawing.Size(726, 209);
            this.VppFileGrid.TabIndex = 1;
            // 
            // clmFileName
            // 
            this.clmFileName.DataPropertyName = "FileName";
            this.clmFileName.HeaderText = "File Name";
            this.clmFileName.Name = "clmFileName";
            this.clmFileName.ReadOnly = true;
            this.clmFileName.Width = 282;
            // 
            // clmFilePath
            // 
            this.clmFilePath.DataPropertyName = "FilePath";
            this.clmFilePath.HeaderText = "FilePath";
            this.clmFilePath.Name = "clmFilePath";
            this.clmFilePath.ReadOnly = true;
            this.clmFilePath.Width = 400;
            // 
            // btnUploadVppFile
            // 
            this.btnUploadVppFile.Location = new System.Drawing.Point(540, 21);
            this.btnUploadVppFile.Name = "btnUploadVppFile";
            this.btnUploadVppFile.Size = new System.Drawing.Size(196, 38);
            this.btnUploadVppFile.TabIndex = 0;
            this.btnUploadVppFile.Text = "Upload VPP Files";
            this.btnUploadVppFile.UseVisualStyleBackColor = true;
            this.btnUploadVppFile.Click += new System.EventHandler(this.btnUploadVppFile_Click);
            // 
            // vpp_fileDialog
            // 
            this.vpp_fileDialog.FileName = "VPPFileDialog";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 383);
            this.Controls.Add(this.pnl_main);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VPP Encryptor";
            this.pnl_main.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VppFileGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_main;
        private System.Windows.Forms.OpenFileDialog vpp_fileDialog;
        private System.Windows.Forms.Button btnUploadVppFile;
        private System.Windows.Forms.Button btn_GenerateDll;
        private System.Windows.Forms.DataGridView VppFileGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmFileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmFilePath;
    }
}

