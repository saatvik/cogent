﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Saatvik.VisionPro
{
    public partial class ProgressBar : Form
    {
        public ProgressBar()
        {
            InitializeComponent();
        }

        public void Update(int val)
        {
            resultProgressBar.Value = val;
        }

        public void StatusUpdate(string status)
        {
        }
    }
}
