﻿namespace Saatvik.VisionPro
{
    partial class ProgressBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultProgressBar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // resultProgressBar
            // 
            this.resultProgressBar.Location = new System.Drawing.Point(12, 24);
            this.resultProgressBar.Maximum = 100000;
            this.resultProgressBar.Name = "resultProgressBar";
            this.resultProgressBar.Size = new System.Drawing.Size(391, 23);
            this.resultProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.resultProgressBar.TabIndex = 0;
            // 
            // ProgressBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 72);
            this.ControlBox = false;
            this.Controls.Add(this.resultProgressBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ProgressBar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar resultProgressBar;
    }
}