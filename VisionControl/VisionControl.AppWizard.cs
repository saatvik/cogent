using Cognex.VisionPro.Implementation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;
using SAATVIK.HARDWARE;
using MySQLHelper.Entities;
using System.Threading;
using Saatvik.VisionPro.Shared;
using System.Collections.Specialized;
using System.Configuration;
using MySQLHelper.Global;
using System.Globalization;
using AppOne;

namespace Saatvik
{
    public partial class VisionControl
    {
        ///////////////////////// START WIZARD GENERATED
        // cognex.wizard.globals.begin
        private static string mVppFilename = @"vpp/program.vpp";
        private static string defaultResultsPath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Results");
        private static string templatePath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Template.xlsx");
        public static string mResultsPath = defaultResultsPath;
        private const string mApplicationName = "Vial Inspection";
        private static bool mUsePasswords = true;
        private static bool mQuickBuildAccess = true;
        private const string mDefaultAdministratorPassword = "admin";
        private const string mDefaultSupervisorPassword = "";
        private static DateTime mGenerationDateTime = new DateTime(2015, 7, 26, 8, 22, 27);
        private static string mGeneratedByVersion = "53.2.0.0";
        // cognex.wizard.globals.end
        ///////////////////////// END WIZARD GENERATED

        private void Wizard_FormLoad()
        {

            ///////////////////////// START WIZARD GENERATED
            // cognex.wizard.formloadactions
            ///////////////////////// END WIZARD GENERATED
        }

        private void Wizard_AttachPropertyProviders()
        {
            ///////////////////////// START WIZARD GENERATED
            // cognex.wizard.attachpropertyproviders
            ///////////////////////// END WIZARD GENERATED
            cogToolPropertyProvider1.Subject = mJM.Job(0).VisionTool;
            cogToolPropertyProvider0.Subject = mJM.Job(1).VisionTool;
        }

        private void Wizard_DetachPropertyProviders()
        {
            ///////////////////////// START WIZARD GENERATED
            // cognex.wizard.detachpropertyproviders
            ///////////////////////// END WIZARD GENERATED
            cogToolPropertyProvider1.Subject = null;
            cogToolPropertyProvider0.Subject = null;
        }

        private void Wizard_EnableControls(bool running)
        { 
            ///////////////////////// START WIZARD GENERATED
            // cognex.wizard.enablecontrols
            ///////////////////////// END WIZARD GENERATED
        }

        private void Wizard_AddJobTabs(System.Collections.ArrayList newPagesList)
        {
            ///////////////////////// START WIZARD GENERATED
            // begin cognex.wizard.addjobtabs

            // end cognex.wizard.addjobtabs
            ///////////////////////// END WIZARD GENERATED
        }

        private static int currentWorkStationCount = -1;
        public static event ChartPlotter PlotChartEventHandler;
        public delegate void ChartPlotter();
        private void Wizard_UpdateJobResults(int idx, Cognex.VisionPro.ICogRecord result)
        {
            try
            {
                var date = DateTime.Now;
                currentWorkStationCount = SAATVIK.HARDWARE.Device.ReadHoldingRegisters(3590, 2, 1)[1];
                Log.LogDetails(string.Format("Trigger Time: {0}   Work Station {1}", (date.ToLongTimeString() + date.ToString("s.ffff") + "         " + idx), currentWorkStationCount));
                Queue1.AddQueueItem(() =>
                {
                    new Action<Cognex.VisionPro.ICogRecord>((Cognex.VisionPro.ICogRecord imageData) =>
                    {
                        if (idx == 0)
                        {
                            DumpDataFromDataWylerRotationCamera(imageData);
                        }
                        else if (idx == 1)
                        {
                            DumpDataFromDataWylerIdCamera(imageData);
                        }
                    })(result);

                });
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
            }
        }

        private void DumpDataFromDataWylerRotationCamera(Cognex.VisionPro.ICogRecord result)
        {
            try
            {
                if (result.SubRecords.Count > 0)
                {
                    var job = RunTimeData.Jobs[0];
                    var config = (NameValueCollection)ConfigurationManager.GetSection("AutoMachineInterface/Calibration");
                    var cameraId = 1;
                    for (int i = 0; i < result.SubRecords.Count; i++)
                    {
                        var rec = result.SubRecords[i];
                        if (rec.RecordKey.StartsWith("P_"))
                        {
                            cameraId = Convert.ToInt32(rec.RecordKey.Split('_')[1]);
                            if (rec.RecordKey.StartsWith("P_" + cameraId + "_Status"))
                            {
                                job.TriggerData.Rows[job.TriggerData.Rows.Count - 1]["Status"] = Utility.GetUserResultData(result.SubRecords[i], false);
                            }
                            else if (rec.RecordKey.StartsWith("P_" + cameraId + "_T"))
                            {
                                var postedData = rec.RecordKey.Split('_');
                                var value = Convert.ToDecimal(Utility.GetUserResultData(result.SubRecords[i], false));
                                if (postedData[3] == "High")
                                {
                                    job.TriggerData.Rows[job.TriggerData.Rows.Count - 1]["TolerenceHigh"] = value;
                                }
                                else if (postedData[3] == "Low")
                                {
                                    job.TriggerData.Rows[job.TriggerData.Rows.Count - 1]["TolerenceLow"] = value;
                                }
                            }
                            else if (rec.RecordKey.StartsWith("P_" + cameraId + "_Error"))
                            {
                                var errorStatus = Utility.GetUserResultData(result.SubRecords[i], false);
                                job.TriggerData.Rows[job.TriggerData.Rows.Count - 1]["IsError"] = Job.ResultTypes.Accept.GetHashCode();

                                if (!string.IsNullOrEmpty(errorStatus))
                                {
                                    if (errorStatus.Equals("Error"))
                                    {
                                        job.TriggerData.Rows[job.TriggerData.Rows.Count - 1]["IsError"] = Job.ResultTypes.Error.GetHashCode();
                                    }
                                    else if (errorStatus.Equals("Reject"))
                                    {
                                        job.TriggerData.Rows[job.TriggerData.Rows.Count - 1]["IsError"] = Job.ResultTypes.Reject.GetHashCode();
                                    }
                                }
                            }
                            else if (rec.RecordKey.StartsWith("P_" + cameraId))
                            {
                                var postedData = rec.RecordKey.Split('_');
                                var dimension = postedData[2];
                                job.WorkStationCount = currentWorkStationCount;
                                var dimensionValue = Convert.ToDecimal(Utility.GetUserResultData(result.SubRecords[i], false));
                                job.TriggerData.Rows.Add(mVppFilename, dimension, dimensionValue, string.Empty, mVppFilename, job.TriggerCount, job.WorkStationCount, 0);
                            }
                        }
                    }

                    job.TriggerCount++;
                    if (job.TriggerCount > 2)
                    {
                        if (job.TriggerData.AsEnumerable().Any(dr =>
                            dr.Field<int>("IsError") == Job.ResultTypes.Error.GetHashCode() ||
                            dr.Field<int>("IsError") == Job.ResultTypes.Reject.GetHashCode()))
                        {
                            var cameraRejectSample = common.getRegDeviceInfo(config["Camera1_RejecteSample"]);
                            Device.WriteCoils(cameraRejectSample[0], new bool[] { true }, 1);
                        }

                        UtilisationData.Indexed++;
                        UtilisationData.Reset_Indexed++;

                        var results = job.TriggerData.AsEnumerable().GroupBy(g => g.Field<int>("TriggerNumber"));
                        if (!job.TriggerData.AsEnumerable().Any(dr =>
                            dr.Field<int>("IsError") == Job.ResultTypes.Error.GetHashCode()))
                        {
                            UtilisationData.Produced++;
                            UtilisationData.Reset_Produced++;
                            RotationCameraYield.TotalProduced++;
                            RotationCameraYield.Reset_TotalProduced++;
                        }

                        if (job.TriggerData.AsEnumerable().Any(dr =>
                           dr.Field<int>("IsError") == Job.ResultTypes.Reject.GetHashCode()))
                        {
                            RotationCameraYield.Rejected++;
                            RotationCameraYield.Reset_Rejected++;
                        }

                        job.Data.AddTriggerData(job.TriggerData);
                        job.Data.SetSampleCount();
                        job.TriggerCount = 0;
                        job.TriggerData = job.TriggerData.Clone();
                        PlotChartEventHandler.Invoke();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.HandleError(ex);
            }
        }

        private void DumpDataFromDataWylerIdCamera(Cognex.VisionPro.ICogRecord result)
        {
            try
            {
                if (result.SubRecords.Count > 0)
                {
                    var job = RunTimeData.Jobs[1];
                    var config = (NameValueCollection)ConfigurationManager.GetSection("AutoMachineInterface/Calibration");
                    var cameraId = 1;
                    for (int i = 0; i < result.SubRecords.Count; i++)
                    {
                        var rec = result.SubRecords[i];
                        if (rec.RecordKey.StartsWith("P_"))
                        {
                            cameraId = Convert.ToInt32(rec.RecordKey.Split('_')[1]);
                            if (rec.RecordKey.StartsWith("P_" + cameraId + "_Status"))
                            {
                                job.TriggerData.Rows[job.TriggerData.Rows.Count - 1]["Status"] = Utility.GetUserResultData(result.SubRecords[i], false);
                            }
                            else if (rec.RecordKey.StartsWith("P_" + cameraId + "_T"))
                            {
                                var postedData = rec.RecordKey.Split('_');
                                var value = Convert.ToDecimal(Utility.GetUserResultData(result.SubRecords[i], false));
                                if (postedData[3] == "High")
                                {
                                    job.TriggerData.Rows[job.TriggerData.Rows.Count - 1]["TolerenceHigh"] = value;
                                }
                                else if (postedData[3] == "Low")
                                {
                                    job.TriggerData.Rows[job.TriggerData.Rows.Count - 1]["TolerenceLow"] = value;
                                }
                            }
                            else if (rec.RecordKey.StartsWith("P_" + cameraId + "_Error"))
                            {
                                var errorStatus = Utility.GetUserResultData(result.SubRecords[i], false);
                                job.TriggerData.Rows[job.TriggerData.Rows.Count - 1]["IsError"] = Job.ResultTypes.Accept.GetHashCode();

                                if (!string.IsNullOrEmpty(errorStatus))
                                {
                                    if (errorStatus.Equals("Error"))
                                    {
                                        job.TriggerData.Rows[job.TriggerData.Rows.Count - 1]["IsError"] = Job.ResultTypes.Error.GetHashCode();
                                    }
                                    else if (errorStatus.Equals("Reject"))
                                    {
                                        job.TriggerData.Rows[job.TriggerData.Rows.Count - 1]["IsError"] = Job.ResultTypes.Reject.GetHashCode();
                                    }
                                }
                            }
                            else if (rec.RecordKey.StartsWith("P_" + cameraId))
                            {
                                var postedData = rec.RecordKey.Split('_');
                                var dimension = postedData[2];
                                job.WorkStationCount = currentWorkStationCount;
                                var dimensionValue = Convert.ToDecimal(Utility.GetUserResultData(result.SubRecords[i], false));
                                job.TriggerData.Rows.Add(mVppFilename, dimension, dimensionValue, string.Empty, mVppFilename, job.TriggerCount, job.WorkStationCount, 0);
                            }
                        }
                    }

                    if (job.CameraId == 1)
                    {
                        //Read Vial height data

                        if (job.TriggerData.AsEnumerable().Any(dr =>
                            dr.Field<int>("IsError") == Job.ResultTypes.Error.GetHashCode() ||
                            dr.Field<int>("IsError") == Job.ResultTypes.Reject.GetHashCode()))
                        {
                            var cameraRejectSample = common.getRegDeviceInfo(config["Camera2_RejecteSample"]);
                            Device.WriteCoils(cameraRejectSample[0], new bool[] { true }, 1);
                        }

                        if (!job.TriggerData.AsEnumerable().Any(dr =>
                            dr.Field<int>("IsError") == Job.ResultTypes.Error.GetHashCode()))
                        {
                            IDCameraYield.TotalProduced++;
                            IDCameraYield.Reset_TotalProduced++;
                        }

                        if (job.TriggerData.AsEnumerable().Any(dr =>
                           dr.Field<int>("IsError") == Job.ResultTypes.Reject.GetHashCode()))
                        {
                            IDCameraYield.Rejected++;
                            IDCameraYield.Reset_Rejected++;
                        }

                        config = (NameValueCollection)ConfigurationManager.GetSection("ManMachineInterfaceUI/LVDTChart");
                        var actualValueReg = common.getRegDeviceInfo(config["ActualValueRegister"]);
                        LVDTChart.CurrentValue = Utility.GetDecimalValue(Convert.ToInt32(Device.ReadHoldingRegisters(actualValueReg[0], (ushort)1)));
                        TraceLog.LogTrace(LVDTChart.CurrentValue.ToString());
                        job.TriggerData.Rows.Add(mVppFilename, "Vial Height", LVDTChart.CurrentValue, string.Empty, mVppFilename, job.TriggerCount, job.WorkStationCount, 0, LVDTChart.UpperLimit, LVDTChart.LowerLimit);

                        if ((LVDTChart.CurrentValue >= LVDTChart.LowerLimit && LVDTChart.CurrentValue <= LVDTChart.UpperLimit))
                        {
                            LVDTYield.Produced++;
                            LVDTYield.Reset_Produced++;
                        }

                        /*RunTimeData.IDCameraJobs.Add(job);
                        Queue2.AddQueueItem(() =>
                        {
                            var waitingJob = RunTimeData.IDCameraJobs.FirstOrDefault();
                            if (waitingJob != null)
                            {
                                waitingJob.Data.AddTriggerData(waitingJob.TriggerData);
                                waitingJob.Data.SetSampleCount();
                                RunTimeData.IDCameraJobs.Remove(waitingJob);
                            }
                        });*/
                        job.Data.AddTriggerData(job.TriggerData);
                        job.Data.SetSampleCount();
                        job.TriggerCount = 0;
                        job.TriggerData = job.TriggerData.Clone();
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.HandleError(ex);
            }
        }


        private void GenerateReport()
        {
            var productionParams = new ProductionParam().GetProductionParams(mVppFilename);
            if (productionParams == null)
            {
                MessageBox.Show("Unable to generate report. Please set production parameters and try again.");
                return;
            }

            Report.GenerateReportForRC(productionParams, mVppFilename);
            Report.GenerateReportForIC(productionParams, mVppFilename);

            //var progressBar = new Saatvik.VisionPro.ProgressBar();
            ////progressBar.StatusUpdate("Please wait....");

            //new Utility().GenerateReportForRotationCamera(mVppFilename, progressBar, productionParams);

            ////.Update(1000);
            //// Utility().GenerateReportForIDCamera(mVppFilename, progressBar, productionParams);
            //progressBar.Close();
        }
    }
}
