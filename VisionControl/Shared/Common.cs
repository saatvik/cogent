﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Saatvik.VisionPro.Shared
{
    public class Common
    {
        public ushort[] getRegDeviceInfo(string register)
        {
            var splitValue = register.Split('_');
            return new ushort[] { splitValue[0].ToUShort16(), splitValue[1].ToUShort16() };
        }

        public void initialiseApplication(int availableJobs)
        {
            RunTimeData.Jobs = initAvailableJobs(availableJobs);
            RunTimeData.RotationCameraJobs = new List<Job>();
            RunTimeData.IDCameraJobs = new List<Job>();
        }

        public List<Job> initAvailableJobs(int availableJobs)
        {
            List<Job> jobs = new List<Job>();
            for (var i = 0; i < availableJobs; i++)
            {
                jobs.Add(new Job(0));
                jobs.Add(new Job(1));
            }
            return jobs;
        }
    }

    public static class SchemaOne
    {
        public static ushort ToUShort16(this string s)
        {
            return string.IsNullOrEmpty(s) ? (ushort)0 : Convert.ToUInt16(s);
        }
    }

}
