﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Saatvik.VisionPro.Shared
{
    public static class RunTimeData
    {
        public static Utility.ChartTypes ChartType { get; set; }
        public static List<Job> Jobs { get; set; }
        public static List<Job> RotationCameraJobs { get; set; }
        public static List<Job> IDCameraJobs { get; set; }
    }

    public static class UtilisationData
    {
        public static int Indexed { get; set; }
        public static int Produced { get; set; }
        public static int Utilisation { get; set; }
        public static int Reset_Indexed { get; set; }
        public static int Reset_Produced { get; set; }
        public static int Reset_Utilisation { get; set; }

        public static string GetObjectString()
        {
            return "" + Indexed + "|" + Produced + "|" + Utilisation + "|" + Reset_Indexed + "|" + Reset_Produced + "|" + Reset_Utilisation;
        }

        public static void SerialiseFromString(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                var val = str.Split('|');
                Indexed = Convert.ToInt32(val[0]);
                Produced = Convert.ToInt32(val[1]);
                Utilisation = Convert.ToInt32(val[2]);
                Reset_Indexed = Convert.ToInt32(val[3]);
                Reset_Produced = Convert.ToInt32(val[4]);
                Reset_Utilisation = Convert.ToInt32(val[5]);
            }
        }
    }

    public static class RotationCameraYield
    {
        public static int Rejected { get; set; }
        public static int Reset_Rejected { get; set; }
        public static int TotalProduced { get; set; }
        public static int Reset_TotalProduced { get; set; }
        public static string GetObjectString()
        {
            return "" + Rejected + "|" + Reset_Rejected + "|" + TotalProduced + "|" + Reset_TotalProduced;
        }
        public static void SerialiseFromString(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                var val = str.Split('|');
                Rejected = Convert.ToInt32(val[0]);
                Reset_Rejected = Convert.ToInt32(val[1]);
                TotalProduced = Convert.ToInt32(val[2]);
                Reset_TotalProduced = Convert.ToInt32(val[3]);
            }
        }
    }

    public static class IDCameraYield
    {
        public static int Rejected { get; set; }
        public static int Reset_Rejected { get; set; }
        public static int TotalProduced { get; set; }
        public static int Reset_TotalProduced { get; set; }

        public static string GetObjectString()
        {
            return "" + Rejected + "|" + Reset_Rejected + "|" + TotalProduced + "|" + Reset_TotalProduced;
        }
        public static void SerialiseFromString(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                var val = str.Split('|');
                Rejected = Convert.ToInt32(val[0]);
                Reset_Rejected = Convert.ToInt32(val[1]);
                TotalProduced = Convert.ToInt32(val[2]);
                Reset_TotalProduced = Convert.ToInt32(val[3]);
            }
        }
    }

    public static class LVDTYield
    {
        public static int Produced { get; set; }
        public static int Reset_Produced { get; set; }

        public static string GetObjectString()
        {
            return "" + Produced + "|" + Reset_Produced;
        }
        public static void SerialiseFromString(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                var val = str.Split('|');
                Produced = Convert.ToInt32(val[0]);
                Reset_Produced = Convert.ToInt32(val[1]);
            }
        }
    }

    public static class LVDTChart
    {
        public static double UpperLimit { get; set; }
        public static double LowerLimit { get; set; }
        public static double CurrentValue { get; set; }
    }

    public class ReportCalculatedData
    {
        public int Count { get; set; }
        public decimal Mu { get; set; }
        public decimal Sigma { get; set; }
        public decimal Max { get; set; }
        public decimal min { get; set; }
        public decimal Range { get; set; }
        public decimal Cpu { get; set; }
        public decimal Cpl { get; set; }
    }
}
