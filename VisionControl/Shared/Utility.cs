using System;
using System.Windows.Forms;
using System.Resources;

using Cognex.VisionPro;
using Cognex.VisionPro.Implementation.Internal;
using Cognex.VisionPro.QuickBuild;
using System.Net.NetworkInformation;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using OfficeOpenXml;
using System.IO;
using System.Xml;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using MySQLHelper.Entities;
using Saatvik;
using System.Diagnostics;
using System.Windows.Forms.DataVisualization.Charting;
using System.Collections.Specialized;
using System.Configuration;
using Saatvik.VisionPro.Shared;
using SAATVIK.HARDWARE;
using MySQLHelper.Global;
using Saatvik.Shared;

namespace Saatvik.VisionPro.Shared
{
    public class Utility
    {

        public enum ChartTypes
        {
            Line,
            Spline,
            Stack
        }

        private static int sampleCount = 0;
        static public ICogRecord TraverseSubRecords(ICogRecord r, string[] subs)
        {
            // Utility function to walk down to a specific subrecord
            if (r == null)
                return r;

            foreach (string s in subs)
            {
                if (r.SubRecords.ContainsKey(s))
                    r = r.SubRecords[s];
                else
                    return null;
            }

            return r;
        }

        static public void FlushAllQueues(CogJobManager jm)
        {
            // Flush all queues
            if (jm == null)
                return;

            jm.UserQueueFlush();
            jm.FailureQueueFlush();
            for (int i = 0; i < jm.JobCount; i++)
            {
                jm.Job(i).OwnedIndependent.RealTimeQueueFlush();
                jm.Job(i).ImageQueueFlush();
            }
        }

        static public int GetJobIndexFromName(CogJobManager mgr, string name)
        {
            if (mgr != null)
            {
                for (int i = 0; i < mgr.JobCount; ++i)
                    if (mgr.Job(i).Name == name)
                        return i;
            }
            return -1;
        }

        static public bool AddRecordToDisplay(CogRecordsDisplay disp, ICogRecord r, string[] subs, bool pickBestImage)
        {
            // Utility function to put a specific subrecord into a display
            ICogRecord addrec = Utility.TraverseSubRecords(r, subs);
            if (addrec != null)
            {
                // if this is the first record in, then always select an image
                if (disp.Subject == null)
                    pickBestImage = true;

                disp.Subject = addrec;

                if (pickBestImage)
                {
                    // select first non-empty image record, to workaround the fact that the input image tool
                    // adds an empty subrecord to the LastRun record when it is disabled (when an image file
                    // tool is used, for example)
                    for (int i = 0; i < addrec.SubRecords.Count; ++i)
                    {
                        ICogImage img = addrec.SubRecords[i].Content as ICogImage;
                        if (img != null && img.Height != 0 && img.Width != 0)
                        {
                            disp.SelectedRecordKey = addrec.RecordKey + "." + addrec.SubRecords[i].RecordKey;
                            break;
                        }
                    }
                }

                return true;
            }

            return false;
        }

        private static bool TypeIsNumeric(Type t)
        {
            if (t == null)
                return false;

            if (t == typeof(double) ||
              t == typeof(long) ||
              t == typeof(sbyte) || t == typeof(byte) ||
              t == typeof(short) || t == typeof(ushort) ||
              t == typeof(int) || t == typeof(uint) ||
              t == typeof(ulong))
                return true;

            return false;
        }

        private static Type GetPropertyType(object obj, string path)
        {
            if (obj == null || path == "")
                return null;

            System.Reflection.MemberInfo[] infos = CogToolTerminals.
              ConvertPathToMemberInfos(obj, obj.GetType(), path);

            if (infos.Length == 0)
                return null;

            // Return the type of the last path element.
            return CogToolTerminals.GetReturnType(infos[infos.Length - 1]);
        }

        public static void FillUserResultData(Control ctrl, ICogRecord result, string path)
        {
            FillUserResultData(ctrl, result, path, false);
        }

        public static void FillUserResultData(Control ctrl, ICogRecord result, string path, bool convertRadiansToDegrees)
        {
            // Extract the data identified by the path (if available) from the given result record.
            // Use a format string for doubles.
            string rtn;
            HorizontalAlignment align = HorizontalAlignment.Left;
            if (result == null)
                rtn = ResourceUtility.GetString("RtResultNotAvailable");
            else
            {
                object obj = null;

                try
                {
                    obj = result.SubRecords[path].Content;
                }
                catch
                {
                }

                // check if data is available
                if (obj != null && obj.GetType().FullName != "System.Object")
                {
                    if (obj.GetType() == typeof(double))
                    {
                        double d = (double)obj;
                        if (convertRadiansToDegrees)
                            d = CogMisc.RadToDeg(d);
                        rtn = d.ToString("0.000");
                    }
                    else
                        rtn = obj.ToString();

                    if (TypeIsNumeric(obj.GetType()))
                        align = HorizontalAlignment.Right;
                }
                else
                    rtn = ResourceUtility.GetString("RtResultNotAvailable");
            }

            ctrl.Text = rtn;
            TextBox box = ctrl as TextBox;
            if (box != null)
                box.TextAlign = align;
        }

        public static void SetupPropertyProvider(CogToolPropertyProvider p, Control gui, object tool, string path)
        {
            p.SetPath(gui, path);

            TextBox box = gui as TextBox;
            if (box != null)
            {
                Type t = GetPropertyType(tool, path);
                if (TypeIsNumeric(t))
                    box.TextAlign = HorizontalAlignment.Right;
            }
        }

        public static string GetThisExecutableDirectory()
        {
            string loc = Application.ExecutablePath;
            loc = System.IO.Path.GetDirectoryName(loc) + "\\";
            return loc;
        }

        public static bool AccessAllowed(string stringLevelRequired, AccessLevel currentLogin)
        {
            // return true if the currentLogin is equal to or greater than the given access
            // level (expressed as a string)
            AccessLevel needed = AccessLevel.Administrator;

            try
            {
                object obj = Enum.Parse(typeof(AccessLevel), stringLevelRequired, true);
                needed = (AccessLevel)obj;
            }
            catch (ArgumentException)
            {
            }

            return currentLogin >= needed;
        }

        /// <summary>
        /// Take a filename (generally a relative path) and determine the full path to the file to
        /// use.  First the directory containing the current .vpp file is checked for the given filename,
        /// then the directory containing this code's assembly is checked.
        /// </summary>
        public static string ResolveAssociatedFilename(string vppfname, string fname)
        {
            // check for the given file in the same directory as the developer vpp file path
            string trydev = System.IO.Path.GetDirectoryName(vppfname) + "\\" + fname;
            if (System.IO.File.Exists(trydev))
            {
                fname = trydev;
            }
            else
            {
                // otherwise use same directory as this executable
                fname = GetThisExecutableDirectory() + fname;
            }

            return fname;
        }

        public static string GetUserResultData(ICogRecord result, bool convertRadiansToDegrees)
        {
            string rtn;

            if (result == null)
                rtn = ResourceUtility.GetString("RtResultNotAvailable");
            else
            {
                object obj = null;

                try
                {
                    obj = result.Content;
                }
                catch
                {
                }

                // check if data is available
                if (obj != null && obj.GetType().FullName != "System.Object")
                {
                    if (obj.GetType() == typeof(double))
                    {
                        double d = (double)obj;
                        if (convertRadiansToDegrees)
                            d = CogMisc.RadToDeg(d);
                        rtn = d.ToString("0.000");
                    }
                    else
                        rtn = obj.ToString();
                }
                else
                    rtn = ResourceUtility.GetString("RtResultNotAvailable");
            }

            return rtn;
        }

        public static MemoryStream GetExcelMemoryStream(DataTable data)
        {
            int starting_Row = 8;
            MemoryStream ms = new MemoryStream();
            try
            {
                using (FileStream fs = File.OpenRead(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int serialNumber = 1;
                        excelWorksheet.Cells["B1:C1"].Merge = true;
                        excelWorksheet.Cells[1, 2].Value = string.Format("{0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString());
                        foreach (DataRow row in data.Rows)
                        {
                            excelWorksheet.Cells[starting_Row, 2].Value = serialNumber.ToString();
                            excelWorksheet.Cells[starting_Row, 3].Value = row["ProgramName"].ToString();
                            excelWorksheet.Cells[starting_Row, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            excelWorksheet.Cells[starting_Row, 4].Value = row["Dimension"].ToString();
                            excelWorksheet.Cells[starting_Row, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            excelWorksheet.Cells[starting_Row, 5].Value = row["Limit"].ToString();
                            excelWorksheet.Cells[starting_Row, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            excelWorksheet.Cells[starting_Row, 6].Value = row["Result"].ToString();
                            excelWorksheet.Cells[starting_Row, 7].Value = row["Status"].ToString();
                            excelWorksheet.Cells[starting_Row, 7].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            serialNumber++;
                            starting_Row++;
                        }

                        excelPackage.SaveAs(ms);
                    }
                }
            }
            catch (Exception ex)
            {

                return ms;
            }

            return ms;

        }

        private static void creatXMLFile(string defaultPath)
        {
            XmlDocument doc = new XmlDocument();
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);
            XmlElement element1 = doc.CreateElement("ResultPath", "");
            element1.InnerText = defaultPath;
            doc.AppendChild(element1);
            doc.Save(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "\\settings.xml"));
        }

        public static string GetResultPath()
        {
            var resultPath = "";
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(Path.GetDirectoryName(Application.ExecutablePath) + "\\settings.xml");
                resultPath = doc.SelectSingleNode("ResultPath").InnerText;
            }
            catch (Exception ex)
            {
                var defaultPath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Results");
                creatXMLFile(defaultPath);
                return defaultPath;
            }
            return resultPath;
        }

        public static void SetResultPath(string resultPath)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(Path.GetDirectoryName(Application.ExecutablePath) + "\\settings.xml");
                doc.SelectSingleNode("ResultPath").InnerText = resultPath;
                doc.Save(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "settings.xml"));
            }
            catch (Exception ex)
            {
                var defaultPath = Path.Combine(resultPath);
                creatXMLFile(defaultPath);
            }
        }

        public static int xValue1 = 0;
        public static int xValue2 = 0;
        public static int xValue3 = 0;
        public static int xValue4 = 0;
        public static int xValue5 = 0;
        public static int xValue6 = 0;
        public static int xValue7 = 0;
        public static int xValue8 = 0;

        public int xValue
        {
            get
            {
                int xVal = 0;
                switch (ChartNumber)
                {
                    case 1:
                        xVal = xValue1;
                        break;
                    case 2:
                        xVal = xValue2;
                        break;
                    case 3:
                        xVal = xValue3;
                        break;
                    case 4:
                        xVal = xValue4;
                        break;
                    case 5:
                        xVal = xValue5;
                        break;
                    case 6:
                        xVal = xValue6;
                        break;
                    case 7:
                        xVal = xValue7;
                        break;
                    case 8:
                        xVal = xValue8;
                        break;

                }
                return xVal;
            }
            set
            {
                switch (ChartNumber)
                {
                    case 1:
                        xValue1 = value;
                        break;
                    case 2:
                        xValue2 = value;
                        break;
                    case 3:
                        xValue3 = value;
                        break;
                    case 4:
                        xValue4 = value;
                        break;
                    case 5:
                        xValue5 = value;
                        break;
                    case 6:
                        xValue6 = value;
                        break;
                    case 7:
                        xValue7 = value;
                        break;
                    case 8:
                        xValue8 = value;
                        break;
                }
            }
        }

        public int ChartNumber { get; set; }

        public Sample GetSampleObject(int cameraPosition)
        {
            Sample commonObject = null;
            switch (cameraPosition)
            {
                case 1:
                    commonObject = new RotationCameraData();
                    break;
                case 2:
                    commonObject = new IdCameraData();
                    break;
            }
            return commonObject;
        }

        public void GenerateReportForRotationCamera(string programName, ProgressBar pBar, ProductionParam productionParams)
        {
            List<RotationCameraData> data = new RotationCameraData().GetSamples(productionParams.BatchSetOn.Date, productionParams.BatchSetOn.Date.AddDays(7) + new TimeSpan(23, 59, 59), programName);
            var pNames = data.Select(s => s.ParameterName).Distinct().ToList();
            var visionProParams = new VisionProParams().GetAll();

            List<string> parameterNames = new List<string>();
            pNames.ForEach(p =>
            {
                var pA = visionProParams.Find(pAA => pAA.ParamName.Equals(p));
                if (pA != null && pA.IncludeInReport == 1)
                {
                    parameterNames.Add(p);
                }
            });

            //Headers
            DataTable dtReportData = new DataTable();
            DataTable dtResultData = new DataTable();
            dtReportData.Columns.Add("SampleName", typeof(string));
            dtResultData.Columns.Add("Name", typeof(string));
            parameterNames.ForEach(pName =>
            {
                dtReportData.Columns.Add(pName, typeof(decimal));
                dtResultData.Columns.Add(pName, typeof(decimal));
            });

            var grpData = (from s in data
                           group s by new
                           {
                               s.SampleName,
                               s.ParameterName,
                           } into g
                           select new RCReportData
                           {
                               SampleName = g.Key.SampleName,
                               ParamName = g.Key.ParameterName,
                               ParamValue = (g.Where(i => i.IsError == Job.ResultTypes.Accept.GetHashCode()).Sum(i => i.ParameterValue) / g.Where(i => i.IsError == Job.ResultTypes.Accept.GetHashCode()).Count()).RoundDecimal()
                           }).ToList();

            var sampleNames = data.Select(s => s.SampleName).Distinct().ToList();
            pBar.StatusUpdate("Rotation Camera: generating report...");

            sampleNames.ForEach(s =>
            {
                pBar.Update(2000 + sampleNames.IndexOf(s));
                var sampleDataList = grpData.FindAll(g => g.SampleName.Equals(s)).ToList();
                DataRow row = dtReportData.NewRow();
                row["SampleName"] = sampleDataList.First().SampleName;

                for (var i = 0; i < parameterNames.Count; i++)
                {
                    var sampleData = sampleDataList.Find(e => e.ParamName.Equals(parameterNames[i]));
                    if (sampleData != null)
                    {
                        if (parameterNames.Contains(sampleData.ParamName))
                            row[sampleData.ParamName] = sampleData.ParamValue.GetType() != typeof(DBNull) ? sampleData.ParamValue : 0;
                    }
                    else
                    {
                        row[parameterNames[i]] = 0;
                    }
                }
                dtReportData.Rows.Add(row);
            });

            PdfPTable mTable = new PdfPTable(parameterNames.Count + 1);
            mTable.HorizontalAlignment = 1;
            mTable.WidthPercentage = 130f;
            //Main Header
            foreach (var col in dtReportData.Columns)
            {
                Font font = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 9f, 1);
                var column = col.GetType().GetProperty("ColumnName").GetValue(col, null);
                if (column.Equals("SampleName"))
                {
                    var newCell = new PdfPCell(new Phrase("Identifier", font));
                    newCell.HorizontalAlignment = 1;
                    mTable.AddCell(newCell);
                }
                else if (!column.Equals("TriggerNumber"))
                    mTable.AddCell(new Phrase(column.ToString(), font));
            }

            // Header: Nominal
            //PdfPRow row = new PdfPRow()
            PdfPCell[] cells = new PdfPCell[parameterNames.Count + 1];
            for (var i = 0; i < dtReportData.Columns.Count; i++)
            {
                Font font = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8f, 0);
                var col = dtReportData.Columns[i];
                var column = col.GetType().GetProperty("ColumnName").GetValue(col, null);
                if (column.Equals("SampleName"))
                    cells[i] = new PdfPCell(new Phrase("Nominal", font));
                else
                {
                    var vParam = visionProParams.Where(pA => pA.ParamName.Equals(column.ToString())).FirstOrDefault();
                    decimal nominal = 0;
                    if (vParam != null)
                    {
                        nominal = vParam.Nominal;
                    }
                    cells[i] = new PdfPCell(new Phrase(nominal.ToString(), font));
                }
            }
            mTable.Rows.Add(new PdfPRow(cells));

            // Header: +Tolerance
            cells = new PdfPCell[parameterNames.Count + 1];
            for (var i = 0; i < dtReportData.Columns.Count; i++)
            {
                Font font = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8f, 0);
                var col = dtReportData.Columns[i];
                var column = col.GetType().GetProperty("ColumnName").GetValue(col, null);
                if (column.Equals("SampleName"))
                    cells[i] = new PdfPCell(new Phrase("+ Tolerance", font));
                else
                {
                    var vParam = visionProParams.Where(pA => pA.ParamName.Equals(column.ToString())).FirstOrDefault();
                    decimal pTolerence = 0;
                    if (vParam != null)
                    {
                        pTolerence = vParam.pTolerence;
                        pTolerence = Math.Abs(pTolerence - (vParam.pTolerence + vParam.nTolerence) / 2);
                    }
                    cells[i] = new PdfPCell(new Phrase(pTolerence.ToString(), font));
                }
            }
            mTable.Rows.Add(new PdfPRow(cells));

            // Header: -Tolerance
            cells = new PdfPCell[parameterNames.Count + 1];
            for (var i = 0; i < dtReportData.Columns.Count; i++)
            {
                Font font = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8f, 0);
                var col = dtReportData.Columns[i];
                var column = col.GetType().GetProperty("ColumnName").GetValue(col, null);
                if (column.Equals("SampleName"))
                    cells[i] = new PdfPCell(new Phrase("- Tolerance", font));
                else
                {
                    var vParam = visionProParams.Where(pA => pA.ParamName.Equals(column.ToString())).FirstOrDefault();
                    decimal nTolerence = 0;
                    if (vParam != null)
                    {
                        nTolerence = vParam.nTolerence;
                        nTolerence = Math.Abs(nTolerence - (vParam.pTolerence + vParam.nTolerence) / 2);
                    }
                    cells[i] = new PdfPCell(new Phrase(nTolerence.ToString(), font));
                }
            }
            mTable.Rows.Add(new PdfPRow(cells));

            Font rowFont = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6f, 0);
            Font rowFontRed = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7f, 0, BaseColor.RED);
            for (var k = 0; k < dtReportData.Rows.Count; k++)
            {
                int count = 0;
                var icells = new PdfPCell[parameterNames.Count + 1];
                DataRow dr = dtReportData.Rows[k];
                icells[count++] = new PdfPCell(new Phrase(dr["SampleName"].ToString(), rowFont));
                parameterNames.ForEach(p =>
                {
                    icells[count++] = new PdfPCell(new Phrase(dr[p].ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
            }

            Func<string, DataTable> GetTable = (header) =>
            {
                var dataTable = dtResultData.Clone();
                return dataTable;
            };

            Func<List<double>, double> getStandardDeviation = (doubleList) =>
            {
                double average = doubleList.Average();
                double sumOfDerivation = 0;
                foreach (double value in doubleList)
                {
                    sumOfDerivation += (value) * (value);
                }
                double sumOfDerivationAverage = sumOfDerivation / (doubleList.Count);
                return Math.Sqrt(sumOfDerivationAverage - (average * average));
            };

            Func<DataTable, string, DataTable> GetPreparedTable = (dt, header) =>
            {
                if (dt != null && dt.Rows.Count != 0)
                    return dt;
                else
                {
                    return GetTable(header);
                }
            };

            var dtMu = GetPreparedTable(null, "Mu");


            DataTable dtSigma = new DataTable();
            dtSigma.Columns.Add("Name", typeof(string));
            parameterNames.ForEach(pName =>
            {
                dtSigma.Columns.Add(pName, typeof(string));
            });

            for (var k = 0; k < dtReportData.Rows.Count; k++)
            {
                DataRow dataRow = dtReportData.Rows[k];
                var muRow = dtMu.NewRow();
                var sigmaRow = dtSigma.NewRow();
                parameterNames.ForEach(p =>
                {
                    muRow[p] = dataRow[p];
                    sigmaRow[p] = dataRow[p];
                });
                dtMu.Rows.Add(muRow);
                dtSigma.Rows.Add(sigmaRow);
            }

            #region Count
            var index = 0;
            var countCells = new PdfPCell[parameterNames.Count + 1];
            countCells[index++] = new PdfPCell(new Phrase("Count", rowFont));
            var _results = new Dictionary<string, ReportCalculatedData>();
            parameterNames.ForEach(p =>
            {
                if (!_results.ContainsKey(p))
                {
                    _results.Add(p, new ReportCalculatedData() { Count = sampleNames.Count });
                }
                countCells[index++] = new PdfPCell(new Phrase(_results[p].Count.ToString(), rowFont));
            });
            mTable.Rows.Add(new PdfPRow(countCells));
            #endregion

            #region Mu
            if (dtMu != null)
            {
                int count = 0;
                var icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Mu", rowFont));
                parameterNames.ForEach(p =>
                {
                    var val = dtMu.AsEnumerable().Sum(dr => dr.Field<decimal>(p));
                    _results[p].Mu = (val / _results[p].Count).RoundDecimal();
                    icells[count++] = new PdfPCell(new Phrase(_results[p].Mu.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
            }
            #endregion

            if (dtSigma != null)
            {
                int count = 0;
                var icells = new PdfPCell[parameterNames.Count + 1];

                #region Sigma
                icells[count++] = new PdfPCell(new Phrase("Sigma", rowFont));
                parameterNames.ForEach(p =>
                {
                    var val = string.Join(",", dtSigma.AsEnumerable().Select(dr => dr.Field<string>(p)));
                    List<double> values = val.Split(',').Select(s => Convert.ToDouble(s)).ToList();
                    _results[p].Sigma = Convert.ToDecimal(getStandardDeviation(values)).RoundDecimal();
                    icells[count++] = new PdfPCell(new Phrase(_results[p].Sigma.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region MAXIMUM
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Maximum", rowFont));
                parameterNames.ForEach(p =>
                {
                    var val = string.Join(",", dtSigma.AsEnumerable().Select(dr => dr.Field<string>(p)));
                    List<decimal> values = val.Split(',').Select(s => Convert.ToDecimal(s)).ToList();
                    _results[p].Max = values.Max().RoundDecimal();
                    icells[count++] = new PdfPCell(new Phrase(_results[p].Max.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region MINIMUM
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Minimum", rowFont));
                parameterNames.ForEach(p =>
                {
                    var val = string.Join(",", dtSigma.AsEnumerable().Select(dr => dr.Field<string>(p)));
                    List<decimal> values = val.Split(',').Select(s => Convert.ToDecimal(s)).ToList();
                    _results[p].min = values.Min().RoundDecimal();
                    icells[count++] = new PdfPCell(new Phrase(_results[p].min.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region RANGE
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Range", rowFont));
                parameterNames.ForEach(p =>
                {
                    _results[p].Range = _results[p].Max - _results[p].min;
                    icells[count++] = new PdfPCell(new Phrase(_results[p].Range.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region Mu+3 Sigma
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Mu+3 Sigma", rowFont));
                parameterNames.ForEach(p =>
                {
                    var result = Convert.ToDecimal(_results[p].Mu) + (3 * Convert.ToDecimal(_results[p].Sigma));
                    icells[count++] = new PdfPCell(new Phrase(result.RoundDecimal().ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region Mu-3 Sigma
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Mu-3 Sigma", rowFont));
                parameterNames.ForEach(p =>
                {
                    var result = Convert.ToDecimal(_results[p].Mu) - (3 * Convert.ToDecimal(_results[p].Sigma));
                    icells[count++] = new PdfPCell(new Phrase(result.RoundDecimal().ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region Cpu
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Cpu", rowFont));
                parameterNames.ForEach(p =>
                {
                    var vParam = visionProParams.Where(pA => pA.ParamName.Equals(p)).FirstOrDefault();
                    decimal pTolerence = 0;
                    decimal nTolerence = 0;
                    decimal nominal = 0;
                    if (vParam != null)
                    {
                        var val = Math.Abs(vParam.nTolerence - ((vParam.pTolerence + vParam.nTolerence) / 2));
                        nTolerence = pTolerence = val;
                        nominal = vParam.Nominal;
                    }
                    var result = _results[p].Sigma != 0 ? (nominal + pTolerence - _results[p].Mu) / (3 * Convert.ToDecimal(_results[p].Sigma)) : 0;
                    _results[p].Cpu = result.RoundDecimal();
                    icells[count++] = new PdfPCell(new Phrase(_results[p].Cpu.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region Cpl
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Cpl", rowFont));
                parameterNames.ForEach(p =>
                {
                    var vParam = visionProParams.Where(pA => pA.ParamName.Equals(p)).FirstOrDefault();
                    decimal pTolerence = 0;
                    decimal nTolerence = 0;
                    decimal nominal = 0;
                    if (vParam != null)
                    {
                        var val = Math.Abs(vParam.nTolerence - ((vParam.pTolerence + vParam.nTolerence) / 2));
                        nTolerence = pTolerence = val;
                        nominal = vParam.Nominal;
                    }
                    var result = _results[p].Sigma != 0 ? (_results[p].Mu - (nominal - Math.Abs(nTolerence))) / (3 * Convert.ToDecimal(_results[p].Sigma)) : 0;
                    _results[p].Cpl = result.RoundDecimal();
                    icells[count++] = new PdfPCell(new Phrase(_results[p].Cpl.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region Cp
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Cp", rowFont));
                parameterNames.ForEach(p =>
                {
                    var vParam = visionProParams.Where(pA => pA.ParamName.Equals(p)).FirstOrDefault();
                    decimal pTolerence = 0;
                    decimal nTolerence = 0;
                    decimal nominal = 0;
                    if (vParam != null)
                    {
                        var val = Math.Abs(vParam.nTolerence - ((vParam.pTolerence + vParam.nTolerence) / 2));
                        nTolerence = pTolerence = val;
                        nominal = vParam.Nominal;
                    }
                    var result = _results[p].Sigma != 0 ? (Math.Abs(pTolerence) + Math.Abs(nTolerence)) / (6 * Convert.ToDecimal(_results[p].Sigma)) : 0;
                    icells[count++] = new PdfPCell(new Phrase(Math.Round(result, 3).ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region Cpk
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Cpk", rowFont));
                parameterNames.ForEach(p =>
                {
                    var vParam = visionProParams.Where(pA => pA.ParamName.Equals(p)).FirstOrDefault();
                    decimal pTolerence = 0;
                    decimal nTolerence = 0;
                    decimal nominal = 0;
                    if (vParam != null)
                    {
                        var val = Math.Abs(vParam.pTolerence - ((vParam.pTolerence + vParam.nTolerence) / 2));
                        nTolerence = pTolerence = val;
                        nominal = vParam.Nominal;
                    }
                    var result = (_results[p].Cpu < _results[p].Cpl ? _results[p].Cpu : _results[p].Cpl).RoundDecimal();
                    icells[count++] = new PdfPCell(new Phrase(result.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion
            }

            pBar.StatusUpdate("Rotation Camera: Generating PDF document");
            Document document = new Document(new RectangleReadOnly(595, 842, 90), 88f, 88f, 10f, 10f);
            ReportSetting rp = new ReportSetting().GetReportSetting();
            var filePath = (rp != null ? rp.ReportFilePath.Replace('|', '\\') : Saatvik.VisionControl.mResultsPath) + (@"\" + "rotationCamera_" + productionParams.ProductName + "_" + productionParams.BatchNumber + "_" + string.Format("{0:yyyyMMddHHmmss}", DateTime.Now)) + ".pdf";
            var instance = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.Create));
            document.Open();

            var customer = new PdfPTable(3);
            customer.WidthPercentage = 110f;
            var eCell = new PdfPCell(new Phrase("           "));
            eCell.Border = 0;
            var customerNameCell = new PdfPCell(new Phrase(productionParams.CustomerName, new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10f, 1)));
            customerNameCell.HorizontalAlignment = 2;
            customerNameCell.Border = 0;
            customer.AddCell(eCell);
            customer.AddCell(eCell);
            customer.AddCell(customerNameCell);
            document.Add(customer);

            //Production parameters information
            var productParams = new PdfPTable(1);
            productParams.HorizontalAlignment = 1;
            productParams.WidthPercentage = 130f;
            var timeStampCell = new PdfPCell(new Phrase(String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now), rowFont));
            var pCell = new PdfPCell(new Phrase("Product Name:  " + productionParams.ProductName, rowFont));
            pCell.Border = 0;
            var vCell = new PdfPCell(new Phrase("Variant:  " + productionParams.Variant, rowFont));
            vCell.Border = 0;
            var bCell = new PdfPCell(new Phrase("Batch No:  " + productionParams.BatchNumber, rowFont));
            bCell.Border = 0;
            var lCell = new PdfPCell(new Phrase("Lot No:  " + productionParams.LotNumber, rowFont));
            lCell.Border = 0;

            //Remarks Section
            var remarks = new PdfPTable(1);
            remarks.HorizontalAlignment = 1;
            remarks.WidthPercentage = 130f;
            var emptyCell = new PdfPCell(new Phrase(" "));
            emptyCell.Border = 0;
            var checkedByCell = new PdfPCell(new Phrase("Checked By:  ", rowFont));
            checkedByCell.Border = 0;
            var approvedByCell = new PdfPCell(new Phrase("Approved By:  ", rowFont));
            approvedByCell.Border = 0;
            var remarksCell = new PdfPCell(new Phrase("Remarks:  ", rowFont));
            remarksCell.Border = 0;
            remarks.AddCell(emptyCell);
            remarks.AddCell(checkedByCell);
            remarks.AddCell(approvedByCell);
            remarks.AddCell(remarksCell);

            timeStampCell.Border = 0;
            timeStampCell.HorizontalAlignment = 2;
            productParams.AddCell(pCell);
            productParams.AddCell(vCell);
            productParams.AddCell(bCell);
            productParams.AddCell(lCell);
            productParams.AddCell(timeStampCell);
            document.Add(productParams);
            document.Add(mTable);

            document.Add(remarks);
            document.Close();
        }

        public void GenerateReportForIDCamera(string programName, ProgressBar pBar, ProductionParam productionParams)
        {
            pBar.Update(2000);
            pBar.StatusUpdate("Rotation Camera: Fetching report data...");
            List<IdCameraData> data = new IdCameraData().GetSamples(productionParams.BatchSetOn, DateTime.Now, programName);
            var pNames = data.Select(s => s.ParameterName).Distinct().ToList();
            var visionProParams = new VisionProParams().GetAll();

            List<string> parameterNames = new List<string>();
            pNames.ForEach(p =>
            {
                //var pA = visionProParams.Find(pAA => pAA.ParamName.Equals(p));
                //if (pA != null && pA.IncludeInReport == 1)
                //{
                    parameterNames.Add(p);
               // }
            });

            //Headers
            DataTable dtReportData = new DataTable();
            DataTable dtResultData = new DataTable();
            dtReportData.Columns.Add("SampleName", typeof(string));
            dtResultData.Columns.Add("Name", typeof(string));
            parameterNames.ForEach(pName =>
            {
                dtReportData.Columns.Add(pName, typeof(decimal));
                dtResultData.Columns.Add(pName, typeof(decimal));
            });

            var grpData = (from s in data
                           group s by new
                           {
                               s.SampleName,
                               s.ParameterName,
                           } into g
                           select new ICReportData
                           {
                               SampleName = g.Key.SampleName,
                               ParamName = g.Key.ParameterName,
                               ParamValue = (g.Where(i => i.IsError == Job.ResultTypes.Accept.GetHashCode()).Sum(i => i.ParameterValue) / g.Where(i => i.IsError == Job.ResultTypes.Accept.GetHashCode()).Count()).RoundDecimal()
                           }).ToList();

            var sampleNames = data.Select(s => s.SampleName).Distinct().ToList();
            pBar.StatusUpdate("Rotation Camera: generating report...");

            sampleNames.ForEach(s =>
            {
                pBar.Update(2000 + sampleNames.IndexOf(s));
                var sampleDataList = grpData.FindAll(g => g.SampleName.Equals(s)).ToList();
                DataRow row = dtReportData.NewRow();
                row["SampleName"] = sampleDataList.First().SampleName;

                for (var i = 0; i < parameterNames.Count; i++)
                {
                    var sampleData = sampleDataList.Find(e => e.ParamName.Equals(parameterNames[i]));
                    if (sampleData != null)
                    {
                        if (parameterNames.Contains(sampleData.ParamName))
                            row[sampleData.ParamName] = sampleData.ParamValue.GetType() != typeof(DBNull) ? sampleData.ParamValue : 0;
                    }
                    else
                    {
                        row[parameterNames[i]] = 0;
                    }
                }
                dtReportData.Rows.Add(row);
            });

            PdfPTable mTable = new PdfPTable(parameterNames.Count + 1);
            mTable.HorizontalAlignment = 1;
            mTable.WidthPercentage = 130f;
            //Main Header
            foreach (var col in dtReportData.Columns)
            {
                Font font = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 9f, 1);
                var column = col.GetType().GetProperty("ColumnName").GetValue(col, null);
                if (column.Equals("SampleName"))
                {
                    var newCell = new PdfPCell(new Phrase("Identifier", font));
                    newCell.HorizontalAlignment = 1;
                    mTable.AddCell(newCell);
                }
                else if (!column.Equals("TriggerNumber"))
                    mTable.AddCell(new Phrase(column.ToString(), font));
            }

            // Header: Nominal
            //PdfPRow row = new PdfPRow()
            PdfPCell[] cells = new PdfPCell[parameterNames.Count + 1];
            decimal nominal = 0;
            for (var i = 0; i < dtReportData.Columns.Count; i++)
            {
                Font font = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8f, 0);
                var col = dtReportData.Columns[i];
                var column = col.GetType().GetProperty("ColumnName").GetValue(col, null);
                if (column.Equals("SampleName"))
                    cells[i] = new PdfPCell(new Phrase("Nominal", font));
                else
                {
                    var vParam = visionProParams.Where(pA => pA.ParamName.Equals(column.ToString())).FirstOrDefault();
                    if (vParam != null)
                    {
                        nominal = vParam.Nominal;
                    }

                    if (column.ToString().Equals("Vial Height"))
                    {
                        nominal = Math.Round((Convert.ToDecimal(LVDTChart.UpperLimit) + Convert.ToDecimal(LVDTChart.LowerLimit)) / 2, 2);
                    }
                    cells[i] = new PdfPCell(new Phrase(nominal.ToString(), font));
                }
            }
            mTable.Rows.Add(new PdfPRow(cells));

            // Header: +Tolerance
            cells = new PdfPCell[parameterNames.Count + 1];
            for (var i = 0; i < dtReportData.Columns.Count; i++)
            {
                Font font = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8f, 0);
                var col = dtReportData.Columns[i];
                var column = col.GetType().GetProperty("ColumnName").GetValue(col, null);
                if (column.Equals("SampleName"))
                    cells[i] = new PdfPCell(new Phrase("+ Tolerance", font));
                else
                {
                    var vParam = visionProParams.Where(pA => pA.ParamName.Equals(column.ToString())).FirstOrDefault();
                    decimal pTolerence = 0;
                    if (vParam != null)
                    {
                        pTolerence = vParam.pTolerence;
                        pTolerence = Math.Abs(pTolerence - (vParam.pTolerence + vParam.nTolerence) / 2);
                    }
                    if (column.ToString().Equals("Vial Height"))
                    {
                        pTolerence = Math.Round(Convert.ToDecimal(LVDTChart.UpperLimit), 2);
                        pTolerence = Math.Abs(pTolerence - nominal);
                    }
                    cells[i] = new PdfPCell(new Phrase(pTolerence.ToString(), font));
                }
            }
            mTable.Rows.Add(new PdfPRow(cells));

            // Header: -Tolerance
            cells = new PdfPCell[parameterNames.Count + 1];
            for (var i = 0; i < dtReportData.Columns.Count; i++)
            {
                Font font = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8f, 0);
                var col = dtReportData.Columns[i];
                var column = col.GetType().GetProperty("ColumnName").GetValue(col, null);
                if (column.Equals("SampleName"))
                    cells[i] = new PdfPCell(new Phrase("- Tolerance", font));
                else
                {
                    var vParam = visionProParams.Where(pA => pA.ParamName.Equals(column.ToString())).FirstOrDefault();
                    decimal nTolerence = 0;
                    if (vParam != null)
                    {
                        nTolerence = vParam.nTolerence;
                        nTolerence = Math.Abs(nTolerence - (vParam.pTolerence + vParam.nTolerence) / 2);
                    }
                    if (column.ToString().Equals("Vial Height"))
                    {
                        nTolerence = Math.Round(Convert.ToDecimal(LVDTChart.LowerLimit), 2);
                        nTolerence = Math.Abs(nTolerence - nominal);
                    }
                    cells[i] = new PdfPCell(new Phrase(nTolerence.ToString(), font));
                }
            }
            mTable.Rows.Add(new PdfPRow(cells));

            Font rowFont = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6f, 0);
            Font rowFontRed = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7f, 0, BaseColor.RED);
            for (var k = 0; k < dtReportData.Rows.Count; k++)
            {
                int count = 0;
                var icells = new PdfPCell[parameterNames.Count + 1];
                DataRow dr = dtReportData.Rows[k];
                icells[count++] = new PdfPCell(new Phrase(dr["SampleName"].ToString(), rowFont));
                parameterNames.ForEach(p =>
                {
                    icells[count++] = new PdfPCell(new Phrase(dr[p].ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
            }

            Func<string, DataTable> GetTable = (header) =>
            {
                var dataTable = dtResultData.Clone();
                return dataTable;
            };

            Func<List<double>, double> getStandardDeviation = (doubleList) =>
            {
                double average = doubleList.Average();
                double sumOfDerivation = 0;
                foreach (double value in doubleList)
                {
                    sumOfDerivation += (value) * (value);
                }
                double sumOfDerivationAverage = sumOfDerivation / (doubleList.Count);
                return Math.Sqrt(sumOfDerivationAverage - (average * average));
            };

            Func<DataTable, string, DataTable> GetPreparedTable = (dt, header) =>
            {
                if (dt != null && dt.Rows.Count != 0)
                    return dt;
                else
                {
                    return GetTable(header);
                }
            };

            var dtMu = GetPreparedTable(null, "Mu");


            DataTable dtSigma = new DataTable();
            dtSigma.Columns.Add("Name", typeof(string));
            parameterNames.ForEach(pName =>
            {
                dtSigma.Columns.Add(pName, typeof(string));
            });

            for (var k = 0; k < dtReportData.Rows.Count; k++)
            {
                DataRow dataRow = dtReportData.Rows[k];
                var muRow = dtMu.NewRow();
                var sigmaRow = dtSigma.NewRow();
                parameterNames.ForEach(p =>
                {
                    muRow[p] = dataRow[p];
                    sigmaRow[p] = dataRow[p];
                });
                dtMu.Rows.Add(muRow);
                dtSigma.Rows.Add(sigmaRow);
            }

            #region Count
            var index = 0;
            var countCells = new PdfPCell[parameterNames.Count + 1];
            countCells[index++] = new PdfPCell(new Phrase("Count", rowFont));
            var _results = new Dictionary<string, ReportCalculatedData>();
            parameterNames.ForEach(p =>
            {
                if (!_results.ContainsKey(p))
                {
                    _results.Add(p, new ReportCalculatedData() { Count = sampleNames.Count });
                }
                countCells[index++] = new PdfPCell(new Phrase(_results[p].Count.ToString(), rowFont));
            });
            mTable.Rows.Add(new PdfPRow(countCells));
            #endregion

            #region Mu
            if (dtMu != null)
            {
                int count = 0;
                var icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Mu", rowFont));
                parameterNames.ForEach(p =>
                {
                    var val = dtMu.AsEnumerable().Sum(dr => dr.Field<decimal>(p));
                    _results[p].Mu = (val / _results[p].Count).RoundDecimal();
                    icells[count++] = new PdfPCell(new Phrase(_results[p].Mu.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
            }
            #endregion

            if (dtSigma != null)
            {
                int count = 0;
                var icells = new PdfPCell[parameterNames.Count + 1];

                #region Sigma
                icells[count++] = new PdfPCell(new Phrase("Sigma", rowFont));
                parameterNames.ForEach(p =>
                {
                    var val = string.Join(",", dtSigma.AsEnumerable().Select(dr => dr.Field<string>(p)));
                    List<double> values = val.Split(',').Select(s => Convert.ToDouble(s)).ToList();
                    _results[p].Sigma = Convert.ToDecimal(getStandardDeviation(values)).RoundDecimal();
                    icells[count++] = new PdfPCell(new Phrase(_results[p].Sigma.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region MAXIMUM
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Maximum", rowFont));
                parameterNames.ForEach(p =>
                {
                    var val = string.Join(",", dtSigma.AsEnumerable().Select(dr => dr.Field<string>(p)));
                    List<decimal> values = val.Split(',').Select(s => Convert.ToDecimal(s)).ToList();
                    _results[p].Max = values.Max().RoundDecimal();
                    icells[count++] = new PdfPCell(new Phrase(_results[p].Max.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region MINIMUM
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Minimum", rowFont));
                parameterNames.ForEach(p =>
                {
                    var val = string.Join(",", dtSigma.AsEnumerable().Select(dr => dr.Field<string>(p)));
                    List<decimal> values = val.Split(',').Select(s => Convert.ToDecimal(s)).ToList();
                    _results[p].min = values.Min().RoundDecimal();
                    icells[count++] = new PdfPCell(new Phrase(_results[p].min.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region RANGE
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Range", rowFont));
                parameterNames.ForEach(p =>
                {
                    _results[p].Range = _results[p].Max - _results[p].min;
                    icells[count++] = new PdfPCell(new Phrase(_results[p].Range.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region Mu+3 Sigma
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Mu+3 Sigma", rowFont));
                parameterNames.ForEach(p =>
                {
                    var result = Convert.ToDecimal(_results[p].Mu) + (3 * Convert.ToDecimal(_results[p].Sigma));
                    icells[count++] = new PdfPCell(new Phrase(result.RoundDecimal().ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region Mu-3 Sigma
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Mu-3 Sigma", rowFont));
                parameterNames.ForEach(p =>
                {
                    var result = Convert.ToDecimal(_results[p].Mu) - (3 * Convert.ToDecimal(_results[p].Sigma));
                    icells[count++] = new PdfPCell(new Phrase(result.RoundDecimal().ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region Cpu
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Cpu", rowFont));
                parameterNames.ForEach(p =>
                {
                    var vParam = visionProParams.Where(pA => pA.ParamName.Equals(p)).FirstOrDefault();
                    decimal pTolerence = 0;
                    decimal nTolerence = 0;
                    if (vParam != null)
                    {
                        var val = Math.Abs(vParam.nTolerence - ((vParam.pTolerence + vParam.nTolerence) / 2));
                        nTolerence = pTolerence = val;
                        nominal = vParam.Nominal;
                    }
                    var result = _results[p].Sigma != 0 ? (nominal + pTolerence - _results[p].Mu) / (3 * Convert.ToDecimal(_results[p].Sigma)) : 0;
                    _results[p].Cpu = result.RoundDecimal();
                    icells[count++] = new PdfPCell(new Phrase(_results[p].Cpu.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region Cpl
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Cpl", rowFont));
                parameterNames.ForEach(p =>
                {
                    var vParam = visionProParams.Where(pA => pA.ParamName.Equals(p)).FirstOrDefault();
                    decimal pTolerence = 0;
                    decimal nTolerence = 0;
                    if (vParam != null)
                    {
                        var val = Math.Abs(vParam.nTolerence - ((vParam.pTolerence + vParam.nTolerence) / 2));
                        nTolerence = pTolerence = val;
                        nominal = vParam.Nominal;
                    }
                    var result = _results[p].Sigma != 0 ? (_results[p].Mu - (nominal - Math.Abs(nTolerence))) / (3 * Convert.ToDecimal(_results[p].Sigma)) : 0;
                    _results[p].Cpl = result.RoundDecimal();
                    icells[count++] = new PdfPCell(new Phrase(_results[p].Cpl.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region Cp
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Cp", rowFont));
                parameterNames.ForEach(p =>
                {
                    var vParam = visionProParams.Where(pA => pA.ParamName.Equals(p)).FirstOrDefault();
                    decimal pTolerence = 0;
                    decimal nTolerence = 0;
                    if (vParam != null)
                    {
                        var val = Math.Abs(vParam.nTolerence - ((vParam.pTolerence + vParam.nTolerence) / 2));
                        nTolerence = pTolerence = val;
                        nominal = vParam.Nominal;
                    }
                    var result = _results[p].Sigma != 0 ? (Math.Abs(pTolerence) + Math.Abs(nTolerence)) / (6 * Convert.ToDecimal(_results[p].Sigma)) : 0;
                    icells[count++] = new PdfPCell(new Phrase(Math.Round(result, 3).ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion

                #region Cpk
                count = 0;
                icells = new PdfPCell[parameterNames.Count + 1];
                icells[count++] = new PdfPCell(new Phrase("Cpk", rowFont));
                parameterNames.ForEach(p =>
                {
                    var vParam = visionProParams.Where(pA => pA.ParamName.Equals(p)).FirstOrDefault();
                    decimal pTolerence = 0;
                    decimal nTolerence = 0;
                    if (vParam != null)
                    {
                        var val = Math.Abs(vParam.pTolerence - ((vParam.pTolerence + vParam.nTolerence) / 2));
                        nTolerence = pTolerence = val;
                        nominal = vParam.Nominal;
                    }
                    var result = (_results[p].Cpu < _results[p].Cpl ? _results[p].Cpu : _results[p].Cpl).RoundDecimal();
                    icells[count++] = new PdfPCell(new Phrase(result.ToString(), rowFont));
                });
                mTable.Rows.Add(new PdfPRow(icells));
                #endregion
            }

            pBar.StatusUpdate("Rotation Camera: Generating PDF document");
            Document document = new Document(new RectangleReadOnly(595, 842, 90), 88f, 88f, 10f, 10f);
            ReportSetting rp = new ReportSetting().GetReportSetting();
            var filePath = (rp != null ? rp.ReportFilePath.Replace('|', '\\') : Saatvik.VisionControl.mResultsPath) + (@"\" + "IDCamera_" + productionParams.ProductName + "_" + productionParams.BatchNumber + "_" + string.Format("{0:yyyyMMddHHmmss}", DateTime.Now)) + ".pdf";
            var instance = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.Create));
            document.Open();

            var customer = new PdfPTable(3);
            customer.WidthPercentage = 110f;
            var eCell = new PdfPCell(new Phrase("           "));
            eCell.Border = 0;
            var customerNameCell = new PdfPCell(new Phrase(productionParams.CustomerName, new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10f, 1)));
            customerNameCell.HorizontalAlignment = 2;
            customerNameCell.Border = 0;
            customer.AddCell(eCell);
            customer.AddCell(eCell);
            customer.AddCell(customerNameCell);
            document.Add(customer);

            //Production parameters information
            var productParams = new PdfPTable(1);
            productParams.HorizontalAlignment = 1;
            productParams.WidthPercentage = 130f;
            var timeStampCell = new PdfPCell(new Phrase(String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now), rowFont));
            var pCell = new PdfPCell(new Phrase("Product Name:  " + productionParams.ProductName, rowFont));
            pCell.Border = 0;
            var vCell = new PdfPCell(new Phrase("Variant:  " + productionParams.Variant, rowFont));
            vCell.Border = 0;
            var bCell = new PdfPCell(new Phrase("Batch No:  " + productionParams.BatchNumber, rowFont));
            bCell.Border = 0;
            var lCell = new PdfPCell(new Phrase("Lot No:  " + productionParams.LotNumber, rowFont));
            lCell.Border = 0;

            //Remarks Section
            var remarks = new PdfPTable(1);
            remarks.HorizontalAlignment = 1;
            remarks.WidthPercentage = 130f;
            var emptyCell = new PdfPCell(new Phrase(" "));
            emptyCell.Border = 0;
            var checkedByCell = new PdfPCell(new Phrase("Checked By:  ", rowFont));
            checkedByCell.Border = 0;
            var approvedByCell = new PdfPCell(new Phrase("Approved By:  ", rowFont));
            approvedByCell.Border = 0;
            var remarksCell = new PdfPCell(new Phrase("Remarks:  ", rowFont));
            remarksCell.Border = 0;
            remarks.AddCell(emptyCell);
            remarks.AddCell(checkedByCell);
            remarks.AddCell(approvedByCell);
            remarks.AddCell(remarksCell);

            timeStampCell.Border = 0;
            timeStampCell.HorizontalAlignment = 2;
            productParams.AddCell(pCell);
            productParams.AddCell(vCell);
            productParams.AddCell(bCell);
            productParams.AddCell(lCell);
            productParams.AddCell(timeStampCell);
            document.Add(productParams);
            document.Add(mTable);

            document.Add(remarks);
            document.Close();
        }

        public static double GetDecimalValue(int num)
        {
            var stNum = num.ToString();
            return Convert.ToDouble((stNum.Substring(0, 2) + "." + stNum.Substring(2, stNum.Length - 2)));
        }

    }


    public class ResourceUtility
    {
        // helper class to wrap string resources for this application

        private static ResourceManager mResources;

        static ResourceUtility()
        {
            mResources = new ResourceManager("Saatvik.strings",
              System.Reflection.Assembly.GetExecutingAssembly());
        }

        public static string GetString(string resname)
        {
            string str = mResources.GetString(resname);
            if (str == null)
                str = "ERROR(" + resname + ")";
            return str;
        }

        public static string FormatString(string resname, string arg0)
        {
            try
            {
                return string.Format(GetString(resname), arg0);
            }
            catch (Exception)
            {
            }

            return "ERROR(" + resname + ")";
        }

        public static string FormatString(string resname, string arg0, string arg1)
        {
            try
            {
                return string.Format(GetString(resname), arg0, arg1);
            }
            catch (Exception)
            {
            }

            return "ERROR(" + resname + ")";
        }
    }

    public class RCReportData
    {
        public string SampleName { get; set; }
        public string ParamName { get; set; }
        public decimal ParamValue { get; set; }
    }

    public class ICReportData
    {
        public string SampleName { get; set; }
        public string ParamName { get; set; }
        public decimal ParamValue { get; set; }
    }

    public class Attributes
    {
        public string PName { get; set; }
        public decimal Nominal { get; set; }
        public decimal PTolerence { get; set; }
        public decimal NTolerence { get; set; }
        public bool IncludeInReport { get; set; }
    }

    public static class AppConfig
    {
        public static string Get(string key)
        {
            return System.Configuration.ConfigurationManager.AppSettings[key];
        }
    }

    public class LineChartSample
    {
        public double XValue { get; set; }
        public double YValue { get; set; }
    }
}

