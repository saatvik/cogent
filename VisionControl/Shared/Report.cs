﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Data;
using System.IO;
using System.Linq;
using MySQLHelper.Entities;
using System.Collections.Generic;
using MySQLHelper.Global;

namespace AppOne
{

    public static class Report
    {
        private static PdfPCell GetHeaderCell(string columnName, int alignment = Element.ALIGN_LEFT, bool isBold = false, bool enableBorder = false)
        {
            var cell = new PdfPCell(new Phrase(columnName, new Font(Font.FontFamily.TIMES_ROMAN, 10, isBold ? 1 : 0)));
            if (!enableBorder)
                cell.Border = 0;
            cell.HorizontalAlignment = alignment;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            return cell;
        }

        public static void InsertEmptyLine(Document doc, int number)
        {
            for (var i = 0; i < number; i++)
                doc.Add(new Phrase("\n"));
        }

        public static void GenerateReportForRC(ProductionParam productionParams, string programName)
        {
            try
            {
                var startDate = DateTime.Now.Date.Date.AddDays(-7) + new TimeSpan(23, 59, 59);
                var endDate = DateTime.Now.Date;
                TraceLog.LogTrace("Report generation started for date range: " + startDate.ToShortDateString() + " - " + endDate.ToShortDateString());
                ReportSetting rp = new ReportSetting().GetReportSetting();
                var filePath = (rp != null ? rp.ReportFilePath.Replace('|', '\\') : Saatvik.VisionControl.mResultsPath) + (@"\" + "SideCamera_" + productionParams.ProductName + "_" + productionParams.BatchNumber + "_" + string.Format("{0:yyyyMMddHHmmss}", DateTime.Now)) + ".pdf";
                using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    using (Document document = new Document(PageSize.A4.Rotate(), 20, 20, 20, 20))
                    {
                        using (PdfWriter writer = PdfWriter.GetInstance(document, fs))
                        {

                            Func<List<double>, double> getStandardDeviation = (doubleList) =>
                            {
                                if (!doubleList.Any())
                                {
                                    return 0;
                                }
                                double average = doubleList.Average();
                                double sumOfDerivation = 0;
                                foreach (double value in doubleList)
                                {
                                    sumOfDerivation += (value) * (value);
                                }
                                double sumOfDerivationAverage = sumOfDerivation / (doubleList.Count);
                                return Math.Sqrt(sumOfDerivationAverage - (average * average));
                            };

                            document.Open();

                            //Header
                            var batchInfo = new PdfPTable(1);
                            batchInfo.TotalWidth = 1000;
                            batchInfo.AddCell(GetHeaderCell("Product Name: " + productionParams.ProductName));
                            batchInfo.AddCell(GetHeaderCell("Variant: " + productionParams.Variant));
                            batchInfo.AddCell(GetHeaderCell("Lot No: " + productionParams.LotNumber));
                            batchInfo.AddCell(GetHeaderCell("Batch Number: " + productionParams.BatchNumber));

                            //operator comments
                            var operatorCommments = new PdfPTable(1);
                            operatorCommments.TotalWidth = 1000;
                            operatorCommments.AddCell(GetHeaderCell("Done By:"));
                            operatorCommments.AddCell(GetHeaderCell("Checked By:"));

                            document.Add(batchInfo);
                            InsertEmptyLine(document, 2);
                            InsertEmptyLine(document, 1);

                            var visionProParams = new VisionProParams().GetAll().Where(v => v.IncludeInReport == 1).ToList();
                            List<RotationCameraData> data = new RotationCameraData().GetSamples(startDate, endDate, programName);
                            if (data.Count == 0)
                            {
                                document.Add(new Chunk("No data Found"));
                            }
                            var dates = data.Select(d => d.ModifiedOn.Date).Distinct().ToList();
                            dates.ForEach(date =>
                            {
                                var contentTable = new PdfPTable(visionProParams.Count + 2);
                                contentTable.AddCell(GetHeaderCell("", enableBorder: true));
                                contentTable.AddCell(GetHeaderCell("", enableBorder: true));
                                visionProParams.Select(v => v.ParamName).Distinct().ToList().ForEach(paramName =>
                                {
                                    contentTable.AddCell(GetHeaderCell(paramName, enableBorder: true));
                                });

                                var dataOnDate = data.Where(d => d.ModifiedOn.Date.Equals(date)).ToList();
                                for (var i = 0; i < 6; i++)
                                {
                                    var cells = new PdfPCell[visionProParams.Count + 2];

                                    if (i == 0)
                                    {
                                        cells[0] = GetHeaderCell(string.Format("{0:MM-dd-yyyy}", date), alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        cells[0].Rowspan = 7;

                                        cells[1] = GetHeaderCell("Good", alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        for (var k = 2; k <= visionProParams.Count + 1; k++)
                                        {
                                            var param = visionProParams[k - 2];
                                            var parameters = dataOnDate.Where(s => s.ParameterName.Equals(param.ParamName) && s.Status.Equals("Pass")).ToList();
                                            cells[k] = GetHeaderCell(parameters.Count.ToString(), alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        }
                                        contentTable.Rows.Add(new PdfPRow(cells));
                                    }
                                    else if (i == 1)
                                    {
                                        cells[1] = GetHeaderCell("Bad", alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        for (var k = 2; k <= visionProParams.Count + 1; k++)
                                        {
                                            var param = visionProParams[k - 2];
                                            var parameters = dataOnDate.Where(s => s.ParameterName.Equals(param.ParamName) && s.Status.Equals("Rejected")).ToList();
                                            cells[k] = GetHeaderCell(parameters.Count.ToString(), alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        }
                                        contentTable.Rows.Add(new PdfPRow(cells));
                                    }
                                    else if (i == 2)
                                    {
                                        cells[1] = GetHeaderCell("Total", alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        for (var k = 2; k <= visionProParams.Count + 1; k++)
                                        {
                                            var param = visionProParams[k - 2];
                                            var parameters = dataOnDate.Where(s => s.ParameterName.Equals(param.ParamName)).ToList();
                                            cells[k] = GetHeaderCell(parameters.Count.ToString(), alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        }
                                        contentTable.Rows.Add(new PdfPRow(cells));
                                    }
                                    else if (i == 3)
                                    {
                                        cells[1] = GetHeaderCell((i == 0 ? "Cp" : "Cpk"), alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        for (var k = 2; k <= visionProParams.Count + 1; k++)
                                        {
                                            var param = visionProParams[k - 2];
                                            decimal pTolerence = 0;
                                            decimal nTolerence = 0;
                                            decimal nominal = 0;
                                            var val = Math.Abs(param.nTolerence - ((param.pTolerence + param.nTolerence) / 2));
                                            nTolerence = pTolerence = val;
                                            nominal = param.Nominal;

                                            var parameters = dataOnDate.Where(s => s.ParameterName.Equals(param.ParamName)).ToList();
                                            var paramValues = parameters.Select(s => Convert.ToDouble(s.ParameterValue)).ToList();
                                            var mu = parameters.Count == 0 ? 0 : parameters.Sum(p => p.ParameterValue) / parameters.Count;
                                            var sigma = Math.Round(Convert.ToDecimal(getStandardDeviation(paramValues)), 2);
                                            var cpu = sigma != 0 ? (nominal + pTolerence - mu) / (3 * Convert.ToDecimal(sigma)) : 0;
                                            var cpl = sigma != 0 ? (mu - (nominal - Math.Abs(nTolerence))) / (3 * Convert.ToDecimal(sigma)) : 0;
                                            var cp = sigma != 0 ? (Math.Abs(pTolerence) + Math.Abs(nTolerence)) / (6 * Convert.ToDecimal(sigma)) : 0;
                                            var cpk = cpu < cpl ? cpu : cpl;
                                            cells[k] = GetHeaderCell(Math.Round(i == 0 ? cp : cpk, 2).ToString(), alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        }
                                        contentTable.Rows.Add(new PdfPRow(cells));
                                    }
                                    else if (i == 4)
                                    {
                                        cells[1] = GetHeaderCell("Utilisation", alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        for (var k = 2; k <= visionProParams.Count + 1; k++)
                                        {
                                            var param = visionProParams[k - 2];
                                            var indexedSamples = dataOnDate.Where(s => s.ParameterName.Equals(param.ParamName)).ToList();
                                            var goodSamples = indexedSamples.Where(s => s.Status.Equals("Pass")).ToList();
                                            cells[k] = GetHeaderCell(((indexedSamples.Count == 0 ? 0 : Math.Round(Convert.ToDecimal(goodSamples.Count) / indexedSamples.Count)) * 100).ToString() + "%", alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        }
                                        contentTable.Rows.Add(new PdfPRow(cells));
                                    }
                                    else if (i == 5)
                                    {
                                        cells[1] = GetHeaderCell("Yield Top Camera", alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        for (var k = 2; k <= visionProParams.Count + 1; k++)
                                        {
                                            var param = visionProParams[k - 2];
                                            var indexedSamples = dataOnDate.Where(s => s.ParameterName.Equals(param.ParamName)).ToList();
                                            var badSamples = indexedSamples.Where(s => s.Status.Equals("Rejected")).ToList();
                                            cells[k] = GetHeaderCell((indexedSamples.Count == 0 ? 0 : (100 - Math.Round(Convert.ToDecimal(badSamples.Count) / indexedSamples.Count))).ToString() + "%", alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        }
                                        contentTable.Rows.Add(new PdfPRow(cells));
                                    }

                                }
                                document.Add(contentTable);
                                InsertEmptyLine(document, 2);
                            });

                            InsertEmptyLine(document, 2);
                            document.Add(operatorCommments);
                            document.Close();
                            writer.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TraceLog.LogTrace(ex.Message + "************" + ex.StackTrace.ToString());
            }
        }

        public static void GenerateReportForIC(ProductionParam productionParams, string programName)
        {
            try
            {
                var startDate = DateTime.Now.Date.Date.AddDays(-7) + new TimeSpan(23, 59, 59);
                var endDate = DateTime.Now.Date;
                TraceLog.LogTrace("Report generation started for date range: " + startDate.ToShortDateString() + " - " + endDate.ToShortDateString());
                ReportSetting rp = new ReportSetting().GetReportSetting();
                var filePath = (rp != null ? rp.ReportFilePath.Replace('|', '\\') : Saatvik.VisionControl.mResultsPath) + (@"\" + "TopCamera_" + productionParams.ProductName + "_" + productionParams.BatchNumber + "_" + string.Format("{0:yyyyMMddHHmmss}", DateTime.Now)) + ".pdf";
                using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    using (Document document = new Document(PageSize.A4.Rotate(), 20, 20, 20, 20))
                    {
                        using (PdfWriter writer = PdfWriter.GetInstance(document, fs))
                        {

                            Func<List<double>, double> getStandardDeviation = (doubleList) =>
                            {
                                if (!doubleList.Any())
                                {
                                    return 0;
                                }
                                double average = doubleList.Average();
                                double sumOfDerivation = 0;
                                foreach (double value in doubleList)
                                {
                                    sumOfDerivation += (value) * (value);
                                }
                                double sumOfDerivationAverage = sumOfDerivation / (doubleList.Count);
                                return Math.Sqrt(sumOfDerivationAverage - (average * average));
                            };

                            document.Open();

                            //Header
                            var batchInfo = new PdfPTable(1);
                            batchInfo.TotalWidth = 1000;
                            batchInfo.AddCell(GetHeaderCell("Product Name: " + productionParams.ProductName));
                            batchInfo.AddCell(GetHeaderCell("Variant: " + productionParams.Variant));
                            batchInfo.AddCell(GetHeaderCell("Lot No: " + productionParams.LotNumber));
                            batchInfo.AddCell(GetHeaderCell("Batch Number: " + productionParams.BatchNumber));

                            //operator comments
                            var operatorCommments = new PdfPTable(1);
                            operatorCommments.TotalWidth = 1000;
                            operatorCommments.AddCell(GetHeaderCell("Done By:"));
                            operatorCommments.AddCell(GetHeaderCell("Checked By:"));

                            document.Add(batchInfo);
                            InsertEmptyLine(document, 2);
                            InsertEmptyLine(document, 1);

                            var visionProParams = new VisionProParams().GetAll().Where(v => v.IncludeInReport == 1).ToList();
                            List<IdCameraData> data = new IdCameraData().GetSamples(startDate, endDate, programName);
                            if (data.Count == 0)
                            {
                                document.Add(new Chunk("No data Found"));
                            }
                            var dates = data.Select(d => d.ModifiedOn.Date).Distinct().ToList();
                            dates.ForEach(date =>
                            {
                                var contentTable = new PdfPTable(visionProParams.Count + 2);
                                contentTable.AddCell(GetHeaderCell("", enableBorder: true));
                                contentTable.AddCell(GetHeaderCell("", enableBorder: true));
                                visionProParams.Select(v => v.ParamName).Distinct().ToList().ForEach(paramName =>
                                {
                                    contentTable.AddCell(GetHeaderCell(paramName, enableBorder: true));
                                });

                                var dataOnDate = data.Where(d => d.ModifiedOn.Date.Equals(date)).ToList();
                                for (var i = 0; i < 6; i++)
                                {
                                    var cells = new PdfPCell[visionProParams.Count + 2];

                                    if (i == 0)
                                    {
                                        cells[0] = GetHeaderCell(string.Format("{0:MM-dd-yyyy}", date), alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        cells[0].Rowspan = 7;

                                        cells[1] = GetHeaderCell("Good", alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        for (var k = 2; k <= visionProParams.Count + 1; k++)
                                        {
                                            var param = visionProParams[k - 2];
                                            var parameters = dataOnDate.Where(s => s.ParameterName.Equals(param.ParamName) && s.Status.Equals("Pass")).ToList();
                                            cells[k] = GetHeaderCell(parameters.Count.ToString(), alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        }
                                        contentTable.Rows.Add(new PdfPRow(cells));
                                    }
                                    else if (i == 1)
                                    {
                                        cells[1] = GetHeaderCell("Bad", alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        for (var k = 2; k <= visionProParams.Count + 1; k++)
                                        {
                                            var param = visionProParams[k - 2];
                                            var parameters = dataOnDate.Where(s => s.ParameterName.Equals(param.ParamName) && s.Status.Equals("Rejected")).ToList();
                                            cells[k] = GetHeaderCell(parameters.Count.ToString(), alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        }
                                        contentTable.Rows.Add(new PdfPRow(cells));
                                    }
                                    else if (i == 2)
                                    {
                                        cells[1] = GetHeaderCell("Total", alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        for (var k = 2; k <= visionProParams.Count + 1; k++)
                                        {
                                            var param = visionProParams[k - 2];
                                            var parameters = dataOnDate.Where(s => s.ParameterName.Equals(param.ParamName)).ToList();
                                            cells[k] = GetHeaderCell(parameters.Count.ToString(), alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        }
                                        contentTable.Rows.Add(new PdfPRow(cells));
                                    }
                                    else if (i == 3)
                                    {
                                        cells[1] = GetHeaderCell((i == 0 ? "Cp" : "Cpk"), alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        for (var k = 2; k <= visionProParams.Count + 1; k++)
                                        {
                                            var param = visionProParams[k - 2];
                                            decimal pTolerence = 0;
                                            decimal nTolerence = 0;
                                            decimal nominal = 0;
                                            var val = Math.Abs(param.nTolerence - ((param.pTolerence + param.nTolerence) / 2));
                                            nTolerence = pTolerence = val;
                                            nominal = param.Nominal;

                                            var parameters = dataOnDate.Where(s => s.ParameterName.Equals(param.ParamName)).ToList();
                                            var paramValues = parameters.Select(s => Convert.ToDouble(s.ParameterValue)).ToList();
                                            var mu = parameters.Count == 0 ? 0 : parameters.Sum(p => p.ParameterValue) / parameters.Count;
                                            var sigma = Math.Round(Convert.ToDecimal(getStandardDeviation(paramValues)), 2);
                                            var cpu = sigma != 0 ? (nominal + pTolerence - mu) / (3 * Convert.ToDecimal(sigma)) : 0;
                                            var cpl = sigma != 0 ? (mu - (nominal - Math.Abs(nTolerence))) / (3 * Convert.ToDecimal(sigma)) : 0;
                                            var cp = sigma != 0 ? (Math.Abs(pTolerence) + Math.Abs(nTolerence)) / (6 * Convert.ToDecimal(sigma)) : 0;
                                            var cpk = cpu < cpl ? cpu : cpl;
                                            cells[k] = GetHeaderCell(Math.Round(i == 0 ? cp : cpk, 2).ToString(), alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        }
                                        contentTable.Rows.Add(new PdfPRow(cells));
                                    }
                                    else if (i == 4)
                                    {
                                        cells[1] = GetHeaderCell("Utilisation", alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        for (var k = 2; k <= visionProParams.Count + 1; k++)
                                        {
                                            var param = visionProParams[k - 2];
                                            var indexedSamples = dataOnDate.Where(s => s.ParameterName.Equals(param.ParamName)).ToList();
                                            var goodSamples = indexedSamples.Where(s => s.Status.Equals("Pass")).ToList();
                                            cells[k] = GetHeaderCell(((indexedSamples.Count == 0 ? 0 : Math.Round(Convert.ToDecimal(goodSamples.Count) / indexedSamples.Count)) * 100).ToString() + "%", alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        }
                                        contentTable.Rows.Add(new PdfPRow(cells));
                                    }
                                    else if (i == 5)
                                    {
                                        cells[1] = GetHeaderCell("Yield Top Camera", alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        for (var k = 2; k <= visionProParams.Count + 1; k++)
                                        {
                                            var param = visionProParams[k - 2];
                                            var indexedSamples = dataOnDate.Where(s => s.ParameterName.Equals(param.ParamName)).ToList();
                                            var badSamples = indexedSamples.Where(s => s.Status.Equals("Rejected")).ToList();
                                            cells[k] = GetHeaderCell((indexedSamples.Count == 0 ? 0 : (100 - Math.Round(Convert.ToDecimal(badSamples.Count) / indexedSamples.Count))).ToString() + "%", alignment: Element.ALIGN_CENTER, enableBorder: true);
                                        }
                                        contentTable.Rows.Add(new PdfPRow(cells));
                                    }

                                }
                                document.Add(contentTable);
                                InsertEmptyLine(document, 2);
                            });

                            InsertEmptyLine(document, 2);
                            document.Add(operatorCommments);
                            document.Close();
                            writer.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TraceLog.LogTrace(ex.Message + "************" + ex.StackTrace.ToString());
            }
        }

    }
}

