﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Saatvik.VisionPro.Shared
{
    public class Job
    {
        public enum ResultTypes
        {
            Accept,
            Reject,
            Error
        }
        public bool IsDeleted { get; set; }
        public int TriggerCount { get; set; }
        public int WorkStationCount { get; set; }
        public DataTable TriggerData { get; set; }
        public int CameraId { get; set; }
        public bool IsWorkStationSet { get; set; }
        public MySQLHelper.Entities.Sample Data { get; set; }

        public Job(int camId)
        {
            TriggerCount = 0;
            WorkStationCount = 0;
            CameraId = camId;
            if (camId == 0)
                Data = new MySQLHelper.Entities.RotationCameraData();
            else
                Data = new MySQLHelper.Entities.IdCameraData();
            TriggerData = new DataTable();
            TriggerData.Columns.Add("SampleName", typeof(string));
            TriggerData.Columns.Add("ParameterName", typeof(string));
            TriggerData.Columns.Add("ParameterValue", typeof(decimal));
            TriggerData.Columns.Add("Status", typeof(string));
            TriggerData.Columns.Add("ProgramName", typeof(string));
            TriggerData.Columns.Add("TriggerNumber", typeof(int));
            TriggerData.Columns.Add("WorkStationNo", typeof(int));
            TriggerData.Columns.Add("IsError", typeof(int));
            TriggerData.Columns.Add("TolerenceHigh", typeof(decimal));
            TriggerData.Columns.Add("TolerenceLow", typeof(decimal));
        }
    }
}
