﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Saatvik.Shared
{
    public static class ExtensionMethods
    {
        public static decimal RoundDecimal(this Decimal d)
        {
            return Math.Round(d, 3);
        }
    }
}
