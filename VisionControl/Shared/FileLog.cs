﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Saatvik.Shared
{
    public static class FileLog
    {
        private static string fileUtilisationAndYield= AppDomain.CurrentDomain.BaseDirectory + "/Saatvik.Data.txt";
        private static Object thisLock = new Object();
        public static void WriteToFile(string val)
        {
            lock (thisLock)
            {
                File.AppendAllText(GetFilePath(), val + Environment.NewLine);
            }
        }

        public static IEnumerable<string> GetFromFile()
        {
            IEnumerable<string> val = null;
            lock (thisLock)
            {
                var path = GetFilePath();
                if (File.Exists(path))
                    val = File.ReadLines(path);
            }
            return val;
        }

        public static void DeleteFile()
        {
            try
            {
                File.Delete(GetFilePath());
            }
            catch { }
        }

        private static string GetFilePath()
        {
            return fileUtilisationAndYield;
        }
    }
}
