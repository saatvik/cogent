﻿namespace Saatvik.VisionPro
{
    partial class DimensionHighLowLimits
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelContainer = new System.Windows.Forms.Panel();
            this.labelLimitHigh = new System.Windows.Forms.Label();
            this.labelLimitLow = new System.Windows.Forms.Label();
            this.labelDimension = new System.Windows.Forms.Label();
            this.buttonSaveToVPP = new System.Windows.Forms.Button();
            this.labelIncludeInReport = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // panelContainer
            // 
            this.panelContainer.AutoScroll = true;
            this.panelContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelContainer.Location = new System.Drawing.Point(13, 37);
            this.panelContainer.Name = "panelContainer";
            this.panelContainer.Size = new System.Drawing.Size(644, 215);
            this.panelContainer.TabIndex = 0;
            // 
            // labelLimitHigh
            // 
            this.labelLimitHigh.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLimitHigh.Location = new System.Drawing.Point(361, 9);
            this.labelLimitHigh.Name = "labelLimitHigh";
            this.labelLimitHigh.Size = new System.Drawing.Size(126, 23);
            this.labelLimitHigh.TabIndex = 5;
            this.labelLimitHigh.Text = "High";
            this.labelLimitHigh.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLimitLow
            // 
            this.labelLimitLow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLimitLow.Location = new System.Drawing.Point(236, 9);
            this.labelLimitLow.Name = "labelLimitLow";
            this.labelLimitLow.Size = new System.Drawing.Size(126, 23);
            this.labelLimitLow.TabIndex = 4;
            this.labelLimitLow.Text = "Low";
            this.labelLimitLow.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDimension
            // 
            this.labelDimension.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDimension.Location = new System.Drawing.Point(33, 9);
            this.labelDimension.Name = "labelDimension";
            this.labelDimension.Size = new System.Drawing.Size(203, 23);
            this.labelDimension.TabIndex = 3;
            this.labelDimension.Text = "Dimension";
            this.labelDimension.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonSaveToVPP
            // 
            this.buttonSaveToVPP.Location = new System.Drawing.Point(519, 258);
            this.buttonSaveToVPP.Name = "buttonSaveToVPP";
            this.buttonSaveToVPP.Size = new System.Drawing.Size(121, 33);
            this.buttonSaveToVPP.TabIndex = 6;
            this.buttonSaveToVPP.Text = "Save";
            this.buttonSaveToVPP.UseVisualStyleBackColor = true;
            this.buttonSaveToVPP.Click += new System.EventHandler(this.buttonSaveToVPP_Click);
            // 
            // labelIncludeInReport
            // 
            this.labelIncludeInReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIncludeInReport.Location = new System.Drawing.Point(493, 9);
            this.labelIncludeInReport.Name = "labelIncludeInReport";
            this.labelIncludeInReport.Size = new System.Drawing.Size(111, 23);
            this.labelIncludeInReport.TabIndex = 5;
            this.labelIncludeInReport.Text = "Include Report ";
            this.labelIncludeInReport.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DimensionHighLowLimits
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 303);
            this.Controls.Add(this.buttonSaveToVPP);
            this.Controls.Add(this.labelIncludeInReport);
            this.Controls.Add(this.labelLimitHigh);
            this.Controls.Add(this.labelLimitLow);
            this.Controls.Add(this.labelDimension);
            this.Controls.Add(this.panelContainer);
            this.Name = "DimensionHighLowLimits";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dimension Limit Parameters";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelContainer;
        private System.Windows.Forms.Label labelLimitHigh;
        private System.Windows.Forms.Label labelLimitLow;
        private System.Windows.Forms.Label labelDimension;
        private System.Windows.Forms.Button buttonSaveToVPP;
        private System.Windows.Forms.Label labelIncludeInReport;
    }
}