﻿using MySQLHelper.Entities;
using Saatvik.VisionPro.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Saatvik.VisionPro
{
    public partial class Check : Form
    {
        public Check()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new Utility().GenerateReportForIDCamera("INSPECTION16VIAL20160705-cog.vpp", null, new ProductionParam());
        }
    }
}
