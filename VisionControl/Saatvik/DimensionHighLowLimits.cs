﻿using Cognex.VisionPro;
using Cognex.VisionPro.QuickBuild;
using Cognex.VisionPro.QuickBuild.Implementation.Internal;
using MySQLHelper.Entities;
using Saatvik.VisionPro.Shared;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Saatvik.VisionPro
{
    public partial class DimensionHighLowLimits : Form
    {
        public static CogJobManager _mJM { get; set; }
        public Action SaveChangesToVpp = null;
        public DimensionHighLowLimits(Action saveChangesToVpp)
        {
            SaveChangesToVpp = saveChangesToVpp;
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public void LoadDimensionalParameters(Cognex.VisionPro.CogToolPropertyProvider cogToolPropertyProvider0, Cognex.VisionPro.CogToolPropertyProvider cogToolPropertyProvider1, CogJobManager mJM)
        {
            var visionProParams = new VisionProParams().GetAll();
            _mJM = mJM;
            var config = (NameValueCollection)ConfigurationManager.GetSection("VisionProParams/Dimensions");
            int margin = 10;
            foreach (var key in config.Keys)
            {
                var splitConfig = config[key.ToString()].Split('_');
                if (splitConfig.Length == 2)
                {
                    var dimension = splitConfig[0];
                    var jobId = Convert.ToInt32(splitConfig[1]);
                    var vParam = visionProParams.Where(pA => pA.ParamName.Equals(dimension)).FirstOrDefault();

                    var label = new Label();
                    label.Text = dimension;
                    label.Name = "label_" + dimension;
                    label.Width = 150;
                    label.Location = new Point(labelDimension.Location.X, labelDimension.Location.Y + margin);

                    var textBoxLimitLow = new CogApplicationWizardNumberBox();
                    textBoxLimitLow.Width = 60;
                    textBoxLimitLow.Name = "textBox_nTolerance";
                    textBoxLimitLow.Location = new Point(labelLimitLow.Location.X + 20, labelLimitLow.Location.Y + margin);

                    var textBoxLimitHigh = new CogApplicationWizardNumberBox();
                    textBoxLimitHigh.Width = 60;
                    textBoxLimitHigh.Name = "textBox_pTolerance";
                    textBoxLimitHigh.Location = new Point(labelLimitHigh.Location.X + 20, labelLimitHigh.Location.Y + margin);

                    var chkIncludeInReport = new CheckBox();
                    chkIncludeInReport.Name = "chkBox_IncludeInReport";
                    chkIncludeInReport.Location = new Point(labelIncludeInReport.Location.X + 20, labelIncludeInReport.Location.Y + margin);
                    chkIncludeInReport.Checked = vParam != null ? (vParam.IncludeInReport == 1 ? true : false) : false;
 
                    if (jobId == 1)
                    {
                        Utility.SetupPropertyProvider(cogToolPropertyProvider1, textBoxLimitLow, mJM.Job(0).VisionTool, "Tools.Item[\"Dimensions\"].(Cognex.VisionPro.ToolBlock.CogToolBlock).Tools.Item[\"CogDataAnalysisTool1\"].(Cognex.VisionPro.CogDataAnalysisTool).RunParams.Item[\"" + dimension + "\"].RejectLowLimit");
                        Utility.SetupPropertyProvider(cogToolPropertyProvider1, textBoxLimitHigh, mJM.Job(0).VisionTool, "Tools.Item[\"Dimensions\"].(Cognex.VisionPro.ToolBlock.CogToolBlock).Tools.Item[\"CogDataAnalysisTool1\"].(Cognex.VisionPro.CogDataAnalysisTool).RunParams.Item[\"" + dimension + "\"].RejectHighLimit");
                    }
                    else
                    {
                        Utility.SetupPropertyProvider(cogToolPropertyProvider0, textBoxLimitLow, mJM.Job(1).VisionTool, "Tools.Item[\"Inner Diameter\"].(Cognex.VisionPro.ToolBlock.CogToolBlock).Tools.Item[\"CogDataAnalysisTool1\"].(Cognex.VisionPro.CogDataAnalysisTool).RunParams.Item[\"" + dimension + "\"].RejectLowLimit");
                        Utility.SetupPropertyProvider(cogToolPropertyProvider0, textBoxLimitHigh, mJM.Job(1).VisionTool, "Tools.Item[\"Inner Diameter\"].(Cognex.VisionPro.ToolBlock.CogToolBlock).Tools.Item[\"CogDataAnalysisTool1\"].(Cognex.VisionPro.CogDataAnalysisTool).RunParams.Item[\"" + dimension + "\"].RejectHighLimit");
                    }

                    panelContainer.Controls.Add(label);
                    panelContainer.Controls.Add(textBoxLimitLow);
                    panelContainer.Controls.Add(textBoxLimitHigh);
                    panelContainer.Controls.Add(chkIncludeInReport);
                    margin += 30;
                }
            }
        }

        private void buttonSaveToVPP_Click(object sender, EventArgs e)
        {
            SaveChangesToVpp();

            List<VisionProParams> visionProParams = new List<VisionProParams>();
            VisionProParams pA = null;
            foreach (var c in panelContainer.Controls)
            {
                if (c is Label)
                {
                    var label = (Label)c;
                    pA = new VisionProParams();
                    pA.ParamName = label.Name.Split('_')[1];
                }
                else if (c is TextBox)
                {
                    var textBox = (TextBox)c;
                    var cName = textBox.Name.Split('_')[1];
                    switch (cName)
                    {
                        case "nTolerance":
                            pA.nTolerence = Convert.ToDecimal(textBox.Text);
                            break;
                        case "pTolerance":
                            pA.pTolerence = Convert.ToDecimal(textBox.Text);
                            break;
                    }
                }
                else if (c is CheckBox)
                {
                    var chkBox = (CheckBox)c;
                    var cName = chkBox.Name.Split('_')[1];
                    pA.IncludeInReport = chkBox.Checked ? 1 : 0;
                    visionProParams.Add(pA);
                }
            }

            new VisionProParams().DeleteAll();
            visionProParams.ForEach(vP =>
            {
                vP.Nominal = Math.Round((vP.pTolerence + vP.nTolerence) / 2, 2);
                vP.Add();
            });

            this.Close();
        }
    }
}
