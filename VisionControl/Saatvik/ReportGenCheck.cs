﻿using MySQLHelper.Entities;
using Saatvik.VisionPro.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Saatvik.Test
{
    public partial class ReportGenCheck : Form
    {
        public ReportGenCheck()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var mVppFilename = "";
            var productionParams = new ProductionParam().GetProductionParams(mVppFilename);
            if (productionParams == null)
            {
                MessageBox.Show("Unable to generate report. Please set production parameters and try again.");
                return;
            }

            var progressBar = new Saatvik.VisionPro.ProgressBar();
            progressBar.Show();
            progressBar.StatusUpdate("Please wait....");

            new Utility().GenerateReportForRotationCamera(mVppFilename, progressBar, productionParams);

            progressBar.Update(1000);
            new Utility().GenerateReportForIDCamera(mVppFilename, progressBar, productionParams);
            progressBar.Close();
        }
    }
}
